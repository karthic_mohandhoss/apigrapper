﻿/******************************************************************************************
Filename        : APIGrabberTester.cs
Purpose         : An API Grabber tester UI application. 
                    
                  NOTE : This application was created for api grabber testing purpose.
                    The application is added as part of the deployment package
                    (msi installer) since this application can also be used to 
                    manually trigger the api grabbing, if the data file is needed
                    apart from the normal scheduled time. In addition, this application
                    can be used for manual api grabbing as a backup application, 
                    if the scheduler needs a fix.

Created by      : Raja B
Created date    : 15-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
15-Mar-2013     Raja B                              Initial created
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using APIGrabbers;
using APIGrabberUtility;

using System.Diagnostics;
using System.Configuration;

namespace APIGrabberTester
{
    public partial class APIGrabberTester : Form
    {
        public APIGrabberTester()
        {   
            InitializeComponent();            
        }

        private void APIGrabberTester_Load(object sender, EventArgs e)
        {
            try
            {
                btnKenshooAds.Visible = false;

                bool enableDfaForDart = false;
                bool.TryParse(ConfigurationManager.AppSettings["dfafordartsb_enablebutton"], out enableDfaForDart);
                btnDfaForDARTSB.Enabled = enableDfaForDart;

                bool enableGoogleAnalytics = false;
                bool.TryParse(ConfigurationManager.AppSettings["gaverve_enablebutton"], out enableGoogleAnalytics);
                btnGAVerve.Enabled = enableGoogleAnalytics;

                bool enableFacebookInsights = false;
                bool.TryParse(ConfigurationManager.AppSettings["fbiverve_enablebutton"], out enableFacebookInsights);
                btnFBIVerve.Enabled = enableFacebookInsights;

                bool enableVindicoSB = false;
                bool.TryParse(ConfigurationManager.AppSettings["vindicosb_enablebutton"], out enableVindicoSB);
                btnVindicoSB.Enabled = enableVindicoSB;

                bool enableVizuPD = false;
                bool.TryParse(ConfigurationManager.AppSettings["vizupd_enablebutton"], out enableVindicoSB);
                btnVindicoSB.Enabled = enableVindicoSB;
            }
            catch (Exception ex)
            {
            }
        }    

        private void btnKenshooAds_Click(object sender, EventArgs e)
        {
            //KenshooAds ken = new KenshooAds();
            //ken.ExecuteJob();
        }
        
        private void btnDfaForDARTSB_Click(object sender, EventArgs e)
        {
            DFAforDARTSB dfa = new DFAforDARTSB();
            dfa.ExecuteJob();
        }

        private void btnGAVerve_Click(object sender, EventArgs e)
        {
            GAVerve GA = new GAVerve();
            GA.ExecuteJob();
        }

        private void btnFBIVerve_Click(object sender, EventArgs e)
        {
            FBIVerve fbi = new FBIVerve();
            fbi.ExecuteJob(); 
        }

        private void btnVindicoSB_Click(object sender, EventArgs e)
        {
            VindicoSB vindico = new VindicoSB();
            vindico.ExecuteJob();
        }

        private void btnVizuPD_Click(object sender, EventArgs e)
        {
            VizuPD vizu = new VizuPD();
            vizu.ExecuteJob();
        }                   
    }
}
