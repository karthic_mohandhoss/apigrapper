﻿namespace APIGrabberTester
{
    partial class APIGrabberTester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKenshooAds = new System.Windows.Forms.Button();
            this.btnGAVerve = new System.Windows.Forms.Button();
            this.btnDfaForDARTSB = new System.Windows.Forms.Button();
            this.btnFBIVerve = new System.Windows.Forms.Button();
            this.btnVindicoSB = new System.Windows.Forms.Button();
            this.btnVizuPD = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnKenshooAds
            // 
            this.btnKenshooAds.Location = new System.Drawing.Point(24, 31);
            this.btnKenshooAds.Name = "btnKenshooAds";
            this.btnKenshooAds.Size = new System.Drawing.Size(155, 25);
            this.btnKenshooAds.TabIndex = 0;
            this.btnKenshooAds.Text = "Kenshoo Ads";
            this.btnKenshooAds.UseVisualStyleBackColor = true;
            this.btnKenshooAds.Click += new System.EventHandler(this.btnKenshooAds_Click);
            // 
            // btnGAVerve
            // 
            this.btnGAVerve.Location = new System.Drawing.Point(24, 119);
            this.btnGAVerve.Name = "btnGAVerve";
            this.btnGAVerve.Size = new System.Drawing.Size(155, 25);
            this.btnGAVerve.TabIndex = 1;
            this.btnGAVerve.Text = "Google Analytics (Verve)";
            this.btnGAVerve.UseVisualStyleBackColor = true;
            this.btnGAVerve.Click += new System.EventHandler(this.btnGAVerve_Click);
            // 
            // btnDfaForDARTSB
            // 
            this.btnDfaForDARTSB.Location = new System.Drawing.Point(24, 74);
            this.btnDfaForDARTSB.Name = "btnDfaForDARTSB";
            this.btnDfaForDARTSB.Size = new System.Drawing.Size(155, 23);
            this.btnDfaForDARTSB.TabIndex = 2;
            this.btnDfaForDARTSB.Text = "DFA for DART (Sari Bodner)";
            this.btnDfaForDARTSB.UseVisualStyleBackColor = true;
            this.btnDfaForDARTSB.Click += new System.EventHandler(this.btnDfaForDARTSB_Click);
            // 
            // btnFBIVerve
            // 
            this.btnFBIVerve.Location = new System.Drawing.Point(24, 164);
            this.btnFBIVerve.Name = "btnFBIVerve";
            this.btnFBIVerve.Size = new System.Drawing.Size(155, 25);
            this.btnFBIVerve.TabIndex = 3;
            this.btnFBIVerve.Text = "Facebook Insights (Verve)";
            this.btnFBIVerve.UseVisualStyleBackColor = true;
            this.btnFBIVerve.Click += new System.EventHandler(this.btnFBIVerve_Click);
            // 
            // btnVindicoSB
            // 
            this.btnVindicoSB.Location = new System.Drawing.Point(24, 214);
            this.btnVindicoSB.Name = "btnVindicoSB";
            this.btnVindicoSB.Size = new System.Drawing.Size(155, 25);
            this.btnVindicoSB.TabIndex = 4;
            this.btnVindicoSB.Text = "Vindico (Sari Bodner)";
            this.btnVindicoSB.UseVisualStyleBackColor = true;
            this.btnVindicoSB.Click += new System.EventHandler(this.btnVindicoSB_Click);
            // 
            // btnVizuPD
            // 
            this.btnVizuPD.Location = new System.Drawing.Point(24, 260);
            this.btnVizuPD.Name = "btnVizuPD";
            this.btnVizuPD.Size = new System.Drawing.Size(155, 25);
            this.btnVizuPD.TabIndex = 5;
            this.btnVizuPD.Text = "Vizu (Pramod Dikshith)";
            this.btnVizuPD.UseVisualStyleBackColor = true;
            this.btnVizuPD.Click += new System.EventHandler(this.btnVizuPD_Click);
            // 
            // APIGrabberTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 297);
            this.Controls.Add(this.btnVizuPD);
            this.Controls.Add(this.btnVindicoSB);
            this.Controls.Add(this.btnFBIVerve);
            this.Controls.Add(this.btnDfaForDARTSB);
            this.Controls.Add(this.btnGAVerve);
            this.Controls.Add(this.btnKenshooAds);
            this.Name = "APIGrabberTester";
            this.Text = "API Grabber Tester / Manual Kicker";
            this.Load += new System.EventHandler(this.APIGrabberTester_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnKenshooAds;
        private System.Windows.Forms.Button btnGAVerve;
        private System.Windows.Forms.Button btnDfaForDARTSB;
        private System.Windows.Forms.Button btnFBIVerve;
        private System.Windows.Forms.Button btnVindicoSB;
        private System.Windows.Forms.Button btnVizuPD;
    }
}

