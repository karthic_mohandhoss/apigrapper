﻿/******************************************************************************************
Filename        : Program.cs
Purpose         : An API Grabber tester UI application. 
                  
Created by      : Raja B
Created date    : 15-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
15-Mar-2013     Raja B                              Initial created
  
*******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using APIGrabbers;

namespace APIGrabberTester
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new APIGrabberTester());            
        }
    }
}
