﻿/******************************************************************************************
Filename        : DFAforDART.cs
Purpose         : To connect to the DFA for DART API to grab the data.
                    
Created by      : Sankar
Created date    : 27-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
27-Mar-2013     Sankar                              Initial created
28-Mar-2013     Raja B                              Modified to grab the report by report id.
20-May-2013     Sankar                              Commented the ctl file generation source code
21-June-2013    Raja B                              Fixed the report file downloaded with partial data 
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Dfareporting.v1_1;
using Google.Apis.Dfareporting.v1_1.Data;
using Google.Apis.Samples.Helper;
using Google.Apis.Util;

using System.Net;
using System.Threading;
using System.Configuration;

using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class DFAforDART
    {
        #region DFA for DART - Generic variables for dfa for dart all accounts

        // The client identifier
        private string clientIdentifier = string.Empty;

        // The client secret
        private string clientSecret = string.Empty;

        // The unique storage name for the application
        private string storage = string.Empty;

        // The api key
        private string key = string.Empty;

        // The Dfa for DART api grabber root folder - 
        // dot net end dfa for dart file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The Dfa for DART api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        // The report file name. This is the partial file name. 
        // The datetime stamp will be concatenated with the final file name.
        private string reportFileName = string.Empty;

        // The Dfa for DART api grabber - etl folder path.
        // The path to which the dfa for dart 7z file should be 
        // dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;

        // The report status - processing
        private const string REPORTSTATUS_PROCESSING = "PROCESSING";

        // The report status - available
        private const string REPORTSTATUS_AVAILABLE = "REPORT_AVAILABLE";  
      
        // While the google dart is processing the file to be ready in their 
        // server end for us to download, this is the poll interval on which 
        // we should be hitting the google dart server to let us know whether
        // the report file is ready for us to download.
        private const int SECONDS_BETWEEN_POLLS = 60;

        // The api scope
        private const string DEVSTORAGESCOPE_READONLY = "https://www.googleapis.com/auth/devstorage.read_only";

        // The api additional scope
        private readonly string dfaReportingScope = DfareportingService.Scopes.Dfareporting.GetStringValue();

        #endregion

        #region DFA for DART - Specific to individual dfa for dart account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;
        
        // The datasource general activty code
        private int dsGeneralActivityCode;
        
        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        /// <summary>
        /// The dfa for dart api grabber constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourceActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public DFAforDART(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }

        /// <summary>
        /// Entry point method for the DFA for DART run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool InitiateDataGrabbing()
        {
            bool jobSucceeded = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() started..", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Read the configuration information from the database
                GetConfigInfo();

                // Register the authenticator. - Sari.Bodner@annalect.com
                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);                
                provider.ClientIdentifier = clientIdentifier;
                provider.ClientSecret = clientSecret;

                // Get authenticated
                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);
                var service = new DfareportingService(auth);

                // Get the user profiles list
                UserProfileList userProfiles = service.UserProfiles.List().Fetch();

                #region to get the refresh token through the api grabber ui (APIGrabberTester) - for one time activity
                
                bool onlyToGetRefreshToken = false;
                bool.TryParse(ConfigurationManager.AppSettings["dfafordart_onlytogetrefreshtoken"], out onlyToGetRefreshToken);
                if (onlyToGetRefreshToken)
                {
                    UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing : The API Grabber UI application running in " +
                                    "onlyToGetRefreshToken mode. The dfafordart_onlytogetrefreshtoken configuration flag in " +
                                    "the api grabber ui app.config file is set to true", string.Empty, SeverityLevel.INFORMATION, 
                                    EmailNotification.NOT_NEEDED);
                    
                    jobSucceeded = true;
                    return jobSucceeded;
                }               

                #endregion
                
                string userProfileId = string.Empty;
                foreach (UserProfile userProfile in userProfiles.Items)
                {
                    userProfileId = userProfile.ProfileId;
                    //Retrieve the report data 
                    jobSucceeded = RetrieveReportData(service, userProfileId);
                    if (!jobSucceeded)
                    {
                        throw new Exception(dsActivityLogNameIP + 
                                                "Execute Job : Error occurred during dart report generation");
                    }
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(dsCode);

                jobSucceeded = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() completed", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, 
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(dsCode);
                }                
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            Tbl_DatasourceConfig datasourceConfig = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Retrieve the configuration information for the database (tbl_datasourceconfig table)
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(dsCode);

                if (datasourceConfig != null)
                {
                    // Client identifier
                    clientIdentifier = datasourceConfig.Clientidentifier;
                    if (string.IsNullOrEmpty(clientIdentifier))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Client Identifier, config info not found in DB");
                    }

                    // Client secret
                    clientSecret = datasourceConfig.Clientsecret;
                    if (string.IsNullOrEmpty(clientSecret))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Client Secret, config info not found in DB");
                    }

                    // Storage
                    storage = datasourceConfig.Storage;
                    if (string.IsNullOrEmpty(storage))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Storage, config info not found in DB");
                    }

                    // Api key
                    key = datasourceConfig.Apikey;
                    if (string.IsNullOrEmpty(key))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Key, config info not found in DB");
                    }

                    // The root folder path for dfa for dart
                    rootFolderPath = datasourceConfig.Rootfolderpath;
                    if (string.IsNullOrEmpty(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                    }
                    rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);
                    
                    // The report file name (partial), the datetime stamp will be concatenated with this report file name
                    // to make the full file name.
                    reportFileName = datasourceConfig.Reportfilename;
                    if (string.IsNullOrEmpty(reportFileName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");

                    }

                    // The back up folder name. The back up of the csv file taken in 7z format
                    backupZipFolderName = datasourceConfig.Backupzipfoldername;
                    if (string.IsNullOrEmpty(backupZipFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                    }

                    // The etl folder path - the folder path to which the final report file (7z extension) and the control file
                    // should be file i/o.
                    etlFolderPath = datasourceConfig.Etlfolderpath;
                    if (string.IsNullOrEmpty(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                    }
                    etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Config info not found in DB");
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() completed.",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (datasourceConfig != null)
                {
                    datasourceConfig = null;
                }
            }
        }

        /// <summary>
        /// To get the authorization
        /// </summary>
        /// <param name="client">the native application client object</param>
        /// <returns>the object which implements iauthorization state - the authorization state object</returns>
        private IAuthorizationState GetAuthorization(NativeApplicationClient client)
        {
            IAuthorizationState authorizationState = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetAuthorization() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Check if there is a cached refresh token available.
                authorizationState = GetCachedRefreshToken(storage, key);

                if (authorizationState != null)
                {
                    try
                    {
                        // Refresh the token
                        client.RefreshToken(authorizationState);
                        return authorizationState;
                    }
                    catch (DotNetOpenAuth.Messaging.ProtocolException ex)
                    {
                        throw new Exception(dsActivityLogNameIP + 
                                            "Get Authorization : Using existing refresh token failed: " + ex.Message);
                    }
                }

                // Retrieve the authorization from the user.                
                authorizationState = AuthorizationMgr.RequestNativeAuthorization(client, dfaReportingScope,
                                                                                    DEVSTORAGESCOPE_READONLY);
                SetCachedRefreshToken(storage, key, authorizationState);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAuthorization() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return authorizationState;
        }

        /// <summary>         
        /// Returns a cached refresh token for this application, or null if unavailable.         
        /// </summary>         
        /// <param name="storageName">The file name (without extension) used for storage.</param>         
        /// <param name="key">The key to decrypt the data with.</param>         
        /// <returns>The authorization state containing a Refresh Token, or null if unavailable</returns>         
        private AuthorizationState GetCachedRefreshToken(string storageName, string key)
        {
            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            string contents = DataAccessUtility.GetScopeRefreshToken(dsCode);
            if (string.IsNullOrEmpty(contents))
            {
                return null; // No cached token available.             
            }
            string[] content = contents.Split(new[] { "\r\n" }, StringSplitOptions.None);
            // Create the authorization state.             
            string[] scopes = content[0].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string refreshToken = content[1];

            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            return new AuthorizationState(scopes) { RefreshToken = refreshToken };
        }

        /// <summary>         
        /// Saves a refresh token to the specified storage name, and encrypts it using the specified key.         
        /// </summary>         
        private void SetCachedRefreshToken(string storageName, string key, IAuthorizationState state)
        {
            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            // Create the file content.             
            string scopes = state.Scope.Aggregate("", (left, append) => left + " " + append);
            string content = scopes + "\r\n" + state.RefreshToken;

            DataAccessUtility.SetScopeRefreshToken(dsCode, content);

            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        }         

        /// <summary>
        /// To retrieve the reports list
        /// </summary>
        /// <param name="service">The API service</param>
        /// <param name="userProfileId">The user profile id</param>        
        /// <returns>True, if successful. Otherwise false</returns>
        private bool RetrieveReportData(DfareportingService service, string userProfileId)
        {
            bool result = false;
            List<string> reportIds = null;
            StringBuilder customErrorMessage = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "RetrieveReportData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                reportIds = new List<string>();
                // Get all the report ids to download
                reportIds = DataAccessUtility.GetDartReportIds(dsCode);

                foreach (string reportId in reportIds)
                {
                    // Prepare the report - Get the report ready for download from google server
                    File file = PrepareReport(service, userProfileId, reportId.Trim()); 

                    if (file != null)
                    {
                        //If the report file generation did not fail, display results.
                        DownloadCSV(file, service);
                    }
                    else
                    {
                        // If the file is null then prepare and raise a custom error message
                        customErrorMessage = new StringBuilder();
                        customErrorMessage.Append(dsActivityLogNameIP);
                        customErrorMessage.Append("RetrieveReportData : Report file generation failed. The user profile id : ");
                        customErrorMessage.Append(userProfileId);
                        customErrorMessage.Append(". The report id : ");
                        customErrorMessage.Append(reportId);
                        throw new Exception(customErrorMessage.ToString());
                    }
                }
                result = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "RetrieveReportData() completed.", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (reportIds != null)
                {
                    reportIds = null;
                }
                if (customErrorMessage != null)
                {
                    customErrorMessage = null;
                }
            }
            return result;
        }
        
        /// <summary>
        /// To prepare the report in DFA for DART API end        
        /// </summary>
        /// <param name="service">The API service</param>
        /// <param name="userProfileId">The user profile id</param>
        /// <param name="reportId">The report id</param>
        /// <returns>The report file</returns>
        private File PrepareReport(DfareportingService service, string userProfileId, string reportId)
        {
            File reportFile = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "PrepareReport() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Run report synchronously.
                ReportsResource.RunRequest request = service.Reports.Run(userProfileId, reportId);
                request.Synchronous = true;
                reportFile = request.Fetch();

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "PrepareReport : Report execution initiated. " +
                                    "Checking for completion...", string.Empty, SeverityLevel.INFORMATION, 
                                    EmailNotification.NOT_NEEDED);

                // Poll for the report completion
                reportFile = WaitForReportCompletion(service, userProfileId, reportFile);

                if (!reportFile.Status.Equals(REPORTSTATUS_AVAILABLE))
                {
                    reportFile = null;
                    throw new Exception(dsActivityLogNameIP + "PrepareReport : Report file generation failed to finish. " + 
                                            "Final status is: " + reportFile.Status);                     
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "PrepareReport : Report file with ID " + reportFile.Id + " generated.",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "PrepareReport() completed.", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return reportFile;
        }

        /// <summary>
        /// To iteratively check whether the report is ready from DFA for DART API end for the download
        /// </summary>
        /// <param name="service">The API service</param>
        /// <param name="userProfileId">The user profile id</param>
        /// <param name="file">The report file</param>
        /// <returns>The report file</returns>
        private File WaitForReportCompletion(DfareportingService service, string userProfileId, File file)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "WaitForReportCompletion() started..", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                TimeSpan timeToSleep = TimeSpan.FromSeconds(SECONDS_BETWEEN_POLLS);
                for (int i = 0; i <= 10000; i++)
                {
                    if (!file.Status.Equals(REPORTSTATUS_PROCESSING))
                    {
                        // If the report is processed and ready to be downloaded then break
                        break;
                    }

                    UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                        dsActivityLogNameIP + "Waiting For Report Completion : Polling again in " +
                                        SECONDS_BETWEEN_POLLS + " seconds.", string.Empty, SeverityLevel.INFORMATION, 
                                        EmailNotification.NOT_NEEDED);

                    Thread.Sleep(timeToSleep);
                    file = service.Reports.Files.Get(userProfileId, file.ReportId, file.Id).Fetch();
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "WaitForReportCompletion() completed.", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            } 
            return file;
        }

        /// <summary>
        /// To download the CSV file from DFA for DART environment
        /// </summary>
        /// <param name="reportFile">The report file</param>
        /// <param name="service">The API service</param>
        private void DownloadCSV(File reportFile, DfareportingService service)
        {
            #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
            //int recordCount = 0;
            #endregion

            StringBuilder csvReportFileName = null;
            StringBuilder reportFilePath = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "DownloadCSV() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Verify whether the root folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(rootFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "DownloadCSV : dfa for dart root folder path not found or not accessible. " +
                                            "The root folder path is " + rootFolderPath);
                }

                // Form the csv report full file name with datetime stamp concatenated with csv extension
                csvReportFileName = new StringBuilder();
                csvReportFileName.Append(reportFileName);
                csvReportFileName.Append(UtilityHelper.FormatDateTimeForFileName());
                csvReportFileName.Append(UtilityHelper.CSV_FILEEXTN);

                // Form the csv report full path (with the folder path and file name)
                reportFilePath = new StringBuilder();
                reportFilePath.Append(rootFolderPath);
                reportFilePath.Append(csvReportFileName.ToString());

                // The google server url, from where the report csv file should be downloaded
                string url = reportFile.Urls.ApiUrl;
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                service.Authenticator.ApplyAuthenticationToRequest(webRequest);
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                using (System.IO.Stream stream = webResponse.GetResponseStream())
                {
                    using (System.IO.StreamReader reader = new System.IO.StreamReader(stream)) //(new System.IO.BufferedStream(stream)))
                    {
                        string reportData = string.Empty;
                        using (System.IO.StreamWriter sWriter = new System.IO.StreamWriter(reportFilePath.ToString(), true))
                        {
                            // Modified by Raja on 6/21/2013 to fix the issue of partial report data being downloaded.
                            while ((reportData = reader.ReadLine()) != null)
                            {
                                sWriter.WriteLine(reportData);

                                #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                                //recordCount = recordCount + 1;
                                #endregion
                            }
                        }
                    }
                }

                #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                //recordCount = recordCount - 11; // excluding header and footer
                //if (recordCount < 0)
                //{
                //    recordCount = 0;
                //}
                #endregion

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "DownloadCSV() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                #region Write files. Backup file, control file and etl file

                // Verify whether the backup folder exist. If not, try to create it.
                string backupZipFileSubPath = rootFolderPath + backupZipFolderName;
                if (!UtilityHelper.VerifyFolderExists(backupZipFileSubPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "DownloadCSV : dfa for dart backup folder path not found or not accessible. " +
                                            "The backup folder path is " + backupZipFileSubPath);
                }

                // Verify whether the etl folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(etlFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "DownloadCSV : dfa for dart etl folder path not found  or not accessible. " +
                                            "The etl folder path is " + etlFolderPath);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "Backup file creation, Control file creation and " +
                                    "copying the 7z file to etl folder is started.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                UtilityHelper.CompressAndBackup(csvReportFileName.ToString(), rootFolderPath, backupZipFileSubPath, etlFolderPath);

                #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                //UtilityHelper.WriteControlFileForDART(csvReportFileName.ToString(), rootFolderPath, backupZipFileSubPath,
                //                   reportFilePath.ToString(), etlFolderPath);  //recordCount
                #endregion

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "Backup file creation, Control file creation and " +
                                    "copying the 7z file to etl folder is completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                #endregion

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }               
    }
}

