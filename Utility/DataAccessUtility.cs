﻿/******************************************************************************************
Filename        : DataAccessUtility.cs
Purpose         : For DB data transactions

Created by      : Raja B
Created date    : 22-Apr-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
22-Apr-2013     Raja B                              Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace APIGrabberUtility
{
    public class DataAccessUtility
    {
        /// <summary>
        /// To retrieve the google analytics - accounts config data 
        /// </summary>
        /// <returns>the google analytics accounts data should be fetch, config data</returns>
        public static IEnumerable<Tbl_GAAccounts> GetGAAccounts(int dsCode)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    return dbEntities.Tbl_GAAccounts.ToList().Where(item => item.Dscode == dsCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To retrieve the schedule data
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Tbl_Schedule> GetSchedule()
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    return dbEntities.Tbl_Schedule.ToList().Where(item => item.Inprocess == false && item.Enabled == true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To retrieve the schedule data by dscode
        /// </summary>
        /// <param name="dsCode">the datasource code</param>
        /// <returns>the schedule record</returns>
        private static IEnumerable<Tbl_Schedule> GetSchedule(int dsCode)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    return dbEntities.Tbl_Schedule.ToList().Where(item => item.Dscode == dsCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To retrieve the vizu - web urls config data 
        /// </summary>
        /// <returns>the vizu web urls config data from the db</returns>
        public static IEnumerable<Tbl_VizuUrls> GetVizuUrls(int dsCode)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    return dbEntities.Tbl_VizuUrls.ToList().Where(item => item.Dscode == dsCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To update the schedule to inprocess
        /// </summary>
        /// <param name="newDateTime">the new scheduled datetime</param>
        /// <param name="dsCode">the data source code</param>
        public static void UpdateScheduleToInprocess(int dsCode)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    var tblSchedules = dbEntities.Tbl_Schedule.Where(item => item.Dscode == dsCode);
                    foreach (var tblSchedule in tblSchedules)
                    {
                        tblSchedule.Inprocess = true;
                    }
                    dbEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///<summary>
        ///To update the schedule to inprocess
        ///</summary>        
        ///<param name="dsCode">the data source code</param>
        public static void ScheduleSuccessfullyCompleted(int dsCode)
        {
            try
            {
                // Load the schedule by dsCode
                IEnumerable<Tbl_Schedule> tblSchedules = DataAccessUtility.GetSchedule(dsCode);

                foreach (var tblSchedule in tblSchedules)
                {
                    string dateAndTime = tblSchedule.Dateandtime.ToString();
                    string interval = tblSchedule.Interval.ToString();
                    string repeat = tblSchedule.Repeat == null ? string.Empty : tblSchedule.Repeat.ToString();

                    // Get the next run time                                    
                    string nextRunTime = UtilityHelper.GetNextRunTime(dateAndTime, interval, repeat);

                    using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                    {
                        Tbl_Schedule tblScheduleUpdate = dbEntities.Tbl_Schedule.First(item => item.Dscode == dsCode);
                        tblScheduleUpdate.Inprocess = false;
                        tblScheduleUpdate.Dateandtime = DateTime.Parse(nextRunTime);
                        dbEntities.SaveChanges();

                        Tbl_DatasourceConfig tblDatasourceConfig = dbEntities.Tbl_DatasourceConfig.First(item => item.Dscode == dsCode);
                        tblDatasourceConfig.Dsconfig5 = "0";
                        dbEntities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        /// <summary>
        /// To update the schedule to completed
        /// </summary>
        /// <param name="dsCode">the data source code</param>
        public static void ScheduleUnsuccessfullyCompleted(int dsCode)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    var tblSchedules = dbEntities.Tbl_Schedule.Where(item => item.Dscode == dsCode);
                    foreach (var tblSchedule in tblSchedules)
                    {
                        tblSchedule.Inprocess = false;
                    }
                    dbEntities.SaveChanges();

                    var tblDatasourceConfig = dbEntities.Tbl_DatasourceConfig.ToList().Where(item => item.Dscode == dsCode).Select(col => col.Dsconfig5);
                    string strDsConfig5 = tblDatasourceConfig == null ? string.Empty : tblDatasourceConfig.ToString();

                    int dsConfig5 = 0;
                    int.TryParse(strDsConfig5, out dsConfig5);
                    if (dsConfig5 >= 3)
                    {
                        // threshold has reached, disable the datasource
                        var tblSchedulesDisable = dbEntities.Tbl_Schedule.Where(item => item.Dscode == dsCode);
                        foreach (var tblSchedule in tblSchedulesDisable)
                        {
                            tblSchedule.Enabled = false;
                        }
                        dbEntities.SaveChanges();
                    }
                    else
                    {
                        dsConfig5 = dsConfig5 + 1;
                        // threshold is not yet attained. Increment the unsuccessfull attempt
                        var tblSchedulesIncrement = dbEntities.Tbl_DatasourceConfig.Where(item => item.Dscode == dsCode);
                        foreach (var tblSchedule in tblSchedulesIncrement)
                        {
                            tblSchedule.Dsconfig5 = dsConfig5.ToString();
                        }
                        dbEntities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// APIGrabberScheduler > OnStart, to reset all the inprocess schedules 
        /// </summary>        
        public static void ResetAllInprocessDatasources()
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    var tblSchedules = dbEntities.Tbl_Schedule.Where(item => item.Enabled);
                    foreach (var tblSchedule in tblSchedules)
                    {
                        tblSchedule.Inprocess = false;
                    }
                    dbEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To get the datasource config info
        /// </summary>
        /// <param name="dsCode">the data source code</param>
        /// <returns>the datasource config data</returns>
        public static Tbl_DatasourceConfig GetDatasourceConfig(int dsCode)
        {            
            Tbl_DatasourceConfig tblDatasourceConfig = new Tbl_DatasourceConfig();
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    tblDatasourceConfig = dbEntities.Tbl_DatasourceConfig.ToList().Where(item => item.Dscode == dsCode).FirstOrDefault();                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tblDatasourceConfig;
        }

        /// <summary>
        /// To get the list of dfa for dart api, report ids
        /// </summary>
        /// <returns>the list of report ids, that should be downloaded</returns>
        public static List<string> GetDartReportIds(int dsCode)
        {
            List<string> reportIds = new List<string>();
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    reportIds = dbEntities.Tbl_DatasourceFields.ToList().Where(item => item.Dscode == dsCode).Select(col => col.Dsfields).ToList();
                }

                //if (reportIds != null && reportIds.Count != 0)
                //{
                //    foreach (var id in reportIds)
                //    {
                //        if (!string.IsNullOrEmpty(id != null ? id.ToString() : string.Empty))
                //        {
                //            reportIds.Add(id.ToString());
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportIds;
        }

        /// <summary>
        /// To store the scope and refresh token to the database
        /// </summary>
        /// <param name="dsCode">the datasource code</param>
        /// <param name="ScopeRefreshToken">the scope and the refresh token</param>
        public static void SetScopeRefreshToken(int dsCode, string ScopeRefreshToken)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    Tbl_DatasourceConfig tblDatasourceConfig = dbEntities.Tbl_DatasourceConfig.First(i => i.Dscode == dsCode);
                    tblDatasourceConfig.Dsconfig1 = ScopeRefreshToken;
                    dbEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To retrieve the scope and the refresh token from the database
        /// </summary>
        /// <param name="dsCode">the datasource code</param>
        /// <returns>the scope and the refresh token</returns>
        public static string GetScopeRefreshToken(int dsCode)
        {
            string scopeRefreshToken = string.Empty;
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    var tblDatasourceConfig = dbEntities.Tbl_DatasourceConfig.ToList().Where(item => item.Dscode == dsCode).First();
                    if (tblDatasourceConfig != null)
                    {
                        scopeRefreshToken = tblDatasourceConfig.Dsconfig1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scopeRefreshToken;
        }

        /// <summary>
        /// To log the event log (activity log) to the database
        /// </summary>
        /// <param name="dsCode">the data source code</param>
        /// <param name="errCode">the error code</param>
        /// <param name="evtMessage">the event message</param>
        /// <param name="sevLevel">the severity level</param>
        /// <param name="dateLogged">the date of event logging</param>
        /// <param name="stackTrace">the stack trace</param>
        /// <param name="notify">the integer value to indicate whether the notification email should be sent to
        /// the configured recipients</param>
        /// <param name="dateNotified">the notification email sent datetime</param>
        public static void EventLogToDB(int dsCode, int errCode, string evtMessage, int sevLevel,
                                            DateTime dateLogged, string stackTrace, int notify, DateTime dateNotified)
        {
            try
            {
                using (BIDWH_QAEntities dbEntities = new BIDWH_QAEntities())
                {
                    Tbl_EventLog tblEventLog = new Tbl_EventLog()
                    {
                        Dscode = dsCode,
                        Errcode = errCode,
                        Evtmessage = evtMessage,
                        Severitylevel = sevLevel,
                        Datelogged = dateLogged,
                        Stacktrace = stackTrace,
                        Notify = notify,
                        Datenotified = dateNotified
                    };
                    dbEntities.Tbl_EventLog.AddObject(tblEventLog);
                    dbEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

    }

}
