﻿/******************************************************************************************
Filename        : UtilityHelper.cs
Purpose         : To have the general purpose utility helper methods
                  
Created by      : Raja B
Created date    : 22-Apr-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
22-Apr-2013     Raja B                              Initial created
20-May-2013     Sankar                              Commented the ctl file generation source code
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.IO;
using System.Configuration;
using ICSharpCode.SharpZipLib.Zip;
using System.Net.Sockets;
using System.Net;

namespace APIGrabberUtility
{
    public class UtilityHelper
    {
        // Date format of the scheduled date
        private const string DATE_FORMAT_STRING = "MM/dd/yyyy HH:mm";

        // The csv file extension
        public const string CSV_FILEEXTN = ".csv";

        #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
        // The control file extension
        // public const string CONTROL_FILEEXTN = ".ctl";
        #endregion

        // The zip file extension
        public const string ZIP_FILEEXTN = ".7z"; //".zip";

        // The csv file extension
        public const string XML_FILEEXTN = ".xml";

        // The facebook insights report - periods
        private const string strDailyPeriod = "Daily";
        private const string strWeeklyPeriod = "Weekly";
        private const string strMontlyPeriod = "Monthly";
        private const string strlifeTimePeriod = "Lifetime";
        private const string str28Period = "28days";

        // To store the ip address of the machine
        private static string machineIPAddress = string.Empty;

        // Windows event log object
        private static System.Diagnostics.EventLog winEventLog;

        public static bool EnableWindowsEventLogging = false;
        public static bool EnableDatabaseLogging = false;



        /// <summary>
        /// The method to log the activity log (windows event log and event log to DB)
        /// </summary>
        /// <param name="API">the datasource code</param>
        /// <param name="eventCode">the event code</param>
        /// <param name="eventMessage">the event message</param>
        /// <param name="stackTrace">the stack trace</param>
        /// <param name="severityLevel">the severity level</param>
        /// <param name="notify">the integer value to indicate whether the notification email should be sent to
        /// the configured recipients</param>
        /// <param name="writeToEventLog">if true, the actiivty should be logged to the windows event log</param>
        /// <param name="writeToDB">if true, the activity should be logged to the database</param>
        public static void ActivityLog(int API, int eventCode, string eventMessage, string stackTrace, int severityLevel, int notify)
        {

            //try
            //{
            //    // Look for the "APIGrabbers" entry
            //    if (!System.Diagnostics.EventLog.SourceExists("APIGrabbers"))
            //    {
            //        System.Diagnostics.EventLog.CreateEventSource("APIGrabbers", "Application");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    System.Diagnostics.EventLog.CreateEventSource("APIGrabbers", "Application");
            //}

            //if (winEventLog == null)
            //{
            //    winEventLog = new System.Diagnostics.EventLog("Application", Environment.MachineName, "APIGrabbers");
            //}

            //if (EnableWindowsEventLogging)
            //{
            //    //write the activity to the windows event log
            //    winEventLog.WriteEntry(API.ToString() + "\n" + eventCode.ToString() + "\n" + eventMessage + "\n" +
            //                                    stackTrace + "\n" + severityLevel.ToString() + "\n" + notify.ToString());
            //}
            

            if (EnableDatabaseLogging)
            {
                // write the activity to database
                DataAccessUtility.EventLogToDB(API, eventCode, eventMessage, severityLevel, DateTime.Now, stackTrace, notify, DateTime.Now);
            }
        }

        /// <summary>
        /// To verify whether the folder exists
        /// </summary>
        /// <param name="folderPath">the folder path</param>
        /// <returns>true, if exists/created successfully. Otherwise false</returns>
        public static bool VerifyFolderExists(string folderPath)
        {
            bool result = false;
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// To get the datetime stamp, that should be appended to the filenames
        /// </summary>
        /// <returns>the datatimestamp to append to the file name</returns>
        public static string FormatDateTimeForFileName()
        {
            //return DateTime.Now.ToString().Replace(":", string.Empty).Replace("/", string.Empty).Replace(" ", string.Empty);
            return DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        }

        /// <summary>
        /// To format the folder path as correct folder path
        /// </summary>
        /// <param name="folderPath">the folder path should be formatted</param>
        /// <param name="IsRootFolder">flag to indicate whether the folder need to be 
        /// formatted is a root folder</param>
        /// <returns>the formatted root folder path</returns>
        public static string FormatFolderPath(string folderPath, bool IsRootFolder)
        {
            folderPath = folderPath.Replace("/", @"\");
            folderPath = folderPath.EndsWith(@"\") ? folderPath : folderPath + @"\";
            if (IsRootFolder)
            {
                folderPath = folderPath + UtilityHelper.FormatDateTimeForFileName() + @"\";
            }
            return folderPath;
        }

        /// <summary>
        /// Generate the CSV file, if not already exists. If exist append the data.
        /// </summary>
        /// <param name="dsReports">the dataset having the reports data</param>
        /// <param name="folderPath">the folder to file i/o</param>
        /// <param name="fileName">the filename</param>
        /// <param name="backupZipFileSubPath">the backup zip file path</param>
        /// <param name="index">the index value</param>
        /// <param name="existingFileName">the file (filename) to append the records</param>
        /// <param name="etlFolderPath">the etl folder path</param>
        /// <param name="recordCount">the total record count</param>
        /// <param name="finalChunk">if it is a </param>
        /// <returns>True, if successful. Otherwise false</returns>
        public static string WriteCSV(DataSet dsReports, string folderPath, string fileName,
                                        string backupZipFileSubPath, int index, string existingFileName,
                                        string etlFolderPath, bool finalChunk) //, long recordCount
        {
            string result = string.Empty;

            StreamWriter sWriter = null;
            try
            {
                if (existingFileName != null && existingFileName != string.Empty)
                    fileName = existingFileName;
                else
                    fileName = fileName + FormatDateTimeForFileName();
                string csvFile = folderPath + fileName + CSV_FILEEXTN;

                sWriter = new StreamWriter(csvFile, true);

                int rowsCount = 0;
                int columnsCount = 0;
                if (dsReports.Tables.Count != 0)
                {
                    rowsCount = dsReports.Tables[0].Rows.Count;
                    columnsCount = dsReports.Tables[0].Columns.Count;
                    if (index == 1)
                    {
                        for (int i = 0; i < columnsCount; i++)
                        {
                            sWriter.Write(dsReports.Tables[0].Columns[i]);
                            if (i < columnsCount - 1)
                                sWriter.Write(",");
                        }
                        sWriter.Write(sWriter.NewLine);
                    }

                    foreach (DataRow dr in dsReports.Tables[0].Rows)
                    {
                        for (int i = 0; i < columnsCount; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                                sWriter.Write(dr[i].ToString().Replace(System.Environment.NewLine, string.Empty)
                                                .Replace(",", string.Empty));
                            if (i < columnsCount - 1)
                                sWriter.Write(",");
                        }
                        sWriter.Write(sWriter.NewLine);
                    }
                }
                sWriter.Close();

                // If the set of records are the final chunk of records retrieved
                if (finalChunk)
                {
                    // To compress and backup 
                    CompressAndBackup(fileName + CSV_FILEEXTN, folderPath, backupZipFileSubPath, etlFolderPath);

                    #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                    // To write the control file
                    //WriteControlFile(fileName, recordCount, etlFolderPath);
                    #endregion
                }
                result = fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sWriter != null)
                {
                    sWriter = null;
                }
            }
            return result;
        }

        /// <summary>
        /// To file i/o the csv file
        /// </summary>
        /// <param name="dsReports">the dataset having the reports data</param>
        /// <param name="folderPath">the folder to file i/o</param>
        /// <param name="fileName">the csv file name</param>
        /// <param name="backupZipFileSubPath">the back up folder path</param>
        /// <param name="etlFolderPath">the etl folder path</param>
        /// <param name="period">the period object string</param>
        /// <param name="backup">the flag to indicate whether the backup should be done</param>
        /// <returns>True, if successful. Otherwise false</returns>
        public static bool WriteCSV(DataSet dsReports, string folderPath, string fileName,
                                        string backupZipFileSubPath, string etlFolderPath)
        {
            bool result = false;

            StreamWriter sWriter = null;
            try
            {
                fileName = fileName + FormatDateTimeForFileName();
                string csvFile = folderPath + fileName + CSV_FILEEXTN;

                sWriter = new StreamWriter(csvFile, true);

                #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                //int rowsCount = 0;
                #endregion

                int columnsCount = 0;
                if (dsReports.Tables.Count != 0)
                {
                    #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                    //rowsCount = dsReports.Tables[0].Rows.Count;
                    #endregion

                    columnsCount = dsReports.Tables[0].Columns.Count;

                    for (int i = 0; i < columnsCount; i++)
                    {
                        //Modified by Karthic
                        string colName = dsReports.Tables[0].Columns[i].ToString();

                        sWriter.Write(colName);
                        if (i < columnsCount - 1)
                            sWriter.Write(",");
                    }

                    sWriter.Write(sWriter.NewLine);

                    foreach (DataRow dr in dsReports.Tables[0].Rows)
                    {
                        for (int i = 0; i < columnsCount; i++)
                        {
                            //Modified by Karthic
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                if (i == 0)
                                {
                                    sWriter.Write(dr[i].ToString().Replace(System.Environment.NewLine, string.Empty)
                                                    .Replace(",", string.Empty));
                                }
                                else
                                {
                                    sWriter.Write(dr[i].ToString().Replace(System.Environment.NewLine, string.Empty)
                                                    .Replace(",", string.Empty));
                                }
                            }
                            if (i < columnsCount - 1)
                                sWriter.Write(",");
                        }
                        sWriter.Write(sWriter.NewLine);
                    }
                }

                sWriter.Close();


                // To compress and backup the data
                CompressAndBackup(fileName + CSV_FILEEXTN, folderPath, backupZipFileSubPath, etlFolderPath);

                #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                // To write the control file
                //WriteControlFile(fileName, rowsCount, etlFolderPath);
                #endregion

                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sWriter != null)
                {
                    sWriter = null;
                }
            }
            return result;
        }

        /// <summary>
        /// To unzip
        /// </summary>
        /// <param name="zipFilesDirPath">the folder to the zip files</param>
        /// <param name="reportXmlFileSubPath">the folder to the unzipped xml file</param>
        /// <param name="backupZipFileSubPath">the folder path for the zip file backup</param>
        public static void Decompress(string zipFilesDirPath, string reportXmlFileSubPath, string backupZipFileSubPath)
        {
            try
            {
                DirectoryInfo zipFolder = new DirectoryInfo(zipFilesDirPath);

                // Decompress all *.7z files in the directory.
                foreach (FileInfo zipFile in zipFolder.GetFiles("*.7z"))
                {
                    FastZip fastZip = new FastZip();
                    fastZip.ExtractZip(zipFile.FullName, reportXmlFileSubPath, string.Empty);
                    string str = backupZipFileSubPath + "\\" + zipFile.Name.Replace(ZIP_FILEEXTN, string.Empty) + "_" +
                            FormatDateTimeForFileName() + ZIP_FILEEXTN;

                    zipFile.MoveTo(str);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To compress and backup the files
        /// </summary>
        /// <param name="csvFile">the csv filename</param>
        /// <param name="csvFolderPath">the csv folder path</param>
        /// <param name="backupZipFileSubPath">the backup zip file (7z) sub folder path</param>
        /// <param name="etlFolderPath">the etl folder path</param>
        public static void CompressAndBackup(string csvFile, string csvFolderPath, string backupZipFileSubPath,
                                                    string etlFolderPath)
        {
            try
            {
                string zipFileName = csvFile.Replace(CSV_FILEEXTN, ZIP_FILEEXTN);
                if (File.Exists(csvFolderPath + zipFileName))
                    File.Delete(csvFolderPath + zipFileName);

                //zip it
                FastZip fastZip = new FastZip();
                fastZip.CreateZip(csvFolderPath + zipFileName, csvFolderPath, true, csvFile);

                //backup
                DirectoryInfo zipFolder = new DirectoryInfo(csvFolderPath);
                FileInfo zipFile = zipFolder.GetFiles(zipFileName).FirstOrDefault<FileInfo>();

                UtilityHelper.VerifyFolderExists(etlFolderPath);

                if (!(string.IsNullOrEmpty(etlFolderPath)))
                {
                    zipFile.CopyTo(etlFolderPath + zipFileName);
                }
                zipFile.MoveTo(backupZipFileSubPath + "\\" + zipFileName);

                if (File.Exists(csvFolderPath + csvFile))
                    File.Delete(csvFolderPath + csvFile);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To update the next schedule date time
        /// </summary>
        /// <param name="dateTime">the current scheduled date time</param>
        /// <param name="interval">the schedule interval</param>
        /// <param name="repeat">the schedule repeat</param>
        /// <returns>the next schedule date time</returns>       
        public static string GetNextRunTime(string dateTime, string interval, string repeat)
        {
            DateTime scheduledTime = DateTime.Now;
            try
            {
                // Try parsing the current date time to the datetime format
                DateTime.TryParse(dateTime, out scheduledTime);

                // Try parsing the current schedule interval to the integer format
                int scheduledInterval = 0;
                int.TryParse(interval, out scheduledInterval);

                switch (repeat.ToUpper())
                {
                    // If the api grabber is scheduled to run every hour
                    case RepeatFrequency.HOURLY:
                        scheduledTime = scheduledTime.AddHours(scheduledInterval);
                        if (scheduledTime < DateTime.Now)
                            scheduledTime = DateTime.Now.AddHours(scheduledInterval);
                        break;
                    // If the api grabber is scheduled to run once a day 
                    case RepeatFrequency.DAILY:
                        while (scheduledTime < DateTime.Now)
                        {
                            scheduledTime = scheduledTime.AddDays(scheduledInterval);
                        }
                        break;
                    // If the api grabber is scheduled to run once a week 
                    case RepeatFrequency.WEEKLY:
                        while (scheduledTime < DateTime.Now)
                        {
                            scheduledTime = scheduledTime.AddDays(7 * scheduledInterval);
                        }
                        break;
                    // If the api grabber is scheduled to run once a month 
                    case RepeatFrequency.MONTHLY:
                        while (scheduledTime < DateTime.Now)
                        {
                            scheduledTime = scheduledTime.AddMonths(scheduledInterval);
                        }
                        break;
                    default:
                        repeat = string.Empty;
                        break;
                }
                if (string.IsNullOrEmpty(repeat))
                {
                    throw new Exception("Get Next Run Time method : The scheduler repeat " +
                                            "(tbl_schedule db table -> Repeat column) value is invalid");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (scheduledTime.ToString(DATE_FORMAT_STRING));
        }

        /// <summary>
        /// To format and return the activity log name and the ip address the local system
        /// </summary>
        /// <param name="activityName">the activity log name for a datasource</param>
        /// <returns>the formatted activity event log name and ip address</returns>
        public static string GetActivityLogNameIP(string activityName)
        {
            try
            {
                return activityName + "[" + UtilityHelper.GetIP() + "] : ";
            }
            catch (Exception ex)
            {

            }
            return activityName;
        }

        /// <summary>
        /// To retrieve the local ip address
        /// </summary>
        /// <returns>the ip address</returns>
        private static string GetIP()
        {
            try
            {
                if (string.IsNullOrEmpty(machineIPAddress))
                {

                    IPAddress ip = Dns.GetHostAddresses(Dns.GetHostName()).Where(
                                        address => address.AddressFamily == AddressFamily.InterNetwork
                                        ).FirstOrDefault();

                    machineIPAddress = ip.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return machineIPAddress;
        }

        /// <summary>
        /// To get the DFA for DART datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int DFAforDARTSBDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["dfafordartsb_dscode"], out dsCode);
                return dsCode;
            }
        }

        /// <summary>
        /// To get the Kenshoo datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int KenshooAdsDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["kenshooads_dscode"], out dsCode);
                return dsCode;
            }
        }

        /// <summary>
        /// To get the google analytics datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int GAVerveDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["gaverve_dscode"], out dsCode);
                return dsCode;
            }
        }

        /// <summary>
        /// To get the facebook insights datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int FBIVerveDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["fbiverve_dscode"], out dsCode);
                return dsCode;
            }
        }

        /// <summary>
        /// To get the Vindico datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int VindicoSBDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["vindicosb_dscode"], out dsCode);
                return dsCode;
            }
        }

        /// <summary>
        /// To get the vizu datasource code from the app.config.
        /// This code should match the dscode autogenerated in tbl_datasourcemaster table
        /// </summary>
        public static int VizuPDDSCode
        {
            get
            {
                int dsCode = 0;
                int.TryParse(ConfigurationManager.AppSettings["vizupd_dscode"], out dsCode);
                return dsCode;
            }
        }

        #region kenshoo ads related methods - commented

        ///// <summary>
        ///// To File I/O the csv file.
        ///// </summary>
        ///// <param name="dsReports">the dataset having the reports data</param>
        ///// <param name="folderPath">the folder to file i/o</param>
        ///// <param name="fileName">the filename</param>
        ///// <param name="backupZipFileSubPath">the backup zip file path</param>
        ///// <param name="etlFolderPath">the etl folder path</param>
        ///// <returns>True, if successful. Otherwise false</returns>
        //public static bool WriteCSV(DataSet dsReports, string folderPath, string fileName
        //                                , string backupZipFileSubPath, string etlFolderPath)
        //{
        //    bool result = false;

        //    StreamWriter sWriter = null;
        //    try
        //    {
        //        // The file name without the extension
        //        fileName = fileName + FormatDateTimeForFileName();
        //        // The csv file name with full path, filename and the extension
        //        string csvFile = folderPath + fileName + CSV_FILEEXTN;
        //        // The control file name with the full path, filename and the extension
        //        string controlFile = folderPath + fileName + CONTROL_FILEEXTN;


        //        sWriter = new StreamWriter(csvFile, true);

        //        int rowsCount = 0;
        //        int columnsCount = 0;
        //        if (dsReports.Tables.Count != 0)
        //        {
        //            rowsCount = dsReports.Tables[0].Rows.Count;
        //            columnsCount = dsReports.Tables[0].Columns.Count;
        //            for (int i = 0; i < columnsCount; i++)
        //            {
        //                sWriter.Write(dsReports.Tables[0].Columns[i]);
        //                if (i < columnsCount - 1)
        //                    sWriter.Write(",");
        //            }

        //            sWriter.Write(sWriter.NewLine);

        //            foreach (DataRow dr in dsReports.Tables[0].Rows)
        //            {
        //                for (int i = 0; i < columnsCount; i++)
        //                {
        //                    if (!Convert.IsDBNull(dr[i]))
        //                        sWriter.Write(dr[i].ToString().Replace(System.Environment.NewLine, string.Empty)
        //                                        .Replace(",", string.Empty));
        //                    if (i < columnsCount - 1)
        //                        sWriter.Write(",");
        //                }
        //                sWriter.Write(sWriter.NewLine);
        //            }
        //        }
        //        sWriter.Close();

        //        // Compress and backup the file
        //        CompressAndBackup(fileName + CSV_FILEEXTN, folderPath, backupZipFileSubPath, etlFolderPath);
        //        // Write the control file
        //        WriteControlFile(fileName, rowsCount, etlFolderPath);

        //        result = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (sWriter != null)
        //        {
        //            sWriter = null;
        //        }
        //    }
        //    return result;
        //}


        ///// <summary>
        ///// To clean up (delete) the xml files
        ///// </summary>
        ///// <param name="xmlFilePath">the path to the xml report files</param>
        //public static void CleanUpXMLFiles(string xmlFilePath)
        //{
        //    try
        //    {
        //        DirectoryInfo xmlFolder = new DirectoryInfo(xmlFilePath);
        //        foreach (FileInfo xmlFile in xmlFolder.GetFiles("*.xml"))
        //        {
        //            xmlFile.Delete();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion

        #region control file generation code - commented, since etl team does not need the control file any more

        ///// <summary>
        ///// To write the control file
        ///// </summary>
        ///// <param name="fileName">the file name</param>
        ///// <param name="rowsCount">the rows count</param>
        ///// <param name="controlFile">the control filename</param>
        ///// <param name="etlFolderPath">the etl folder path</param>
        //private static void WriteControlFile(string fileName, long rowsCount, string etlFolderPath)
        //{
        //    StringBuilder ctlFileInfo = null;
        //    StreamWriter sWriter = null;
        //    try
        //    {
        //        ctlFileInfo = new StringBuilder();
        //        ctlFileInfo.Append(fileName);
        //        ctlFileInfo.Append(CSV_FILEEXTN);
        //        ctlFileInfo.Append(",");
        //        ctlFileInfo.Append(rowsCount);
        //        ctlFileInfo.Append(",");
        //        ctlFileInfo.Append(DateTime.Now);

        //        #region to write the control file to the report folder
        //        //sWriter = new StreamWriter(controlFile);
        //        //sWriter.WriteLine(ctlFileInfo.ToString());
        //        //sWriter.Close();
        //        #endregion

        //        sWriter = new StreamWriter(etlFolderPath + fileName + CONTROL_FILEEXTN);
        //        sWriter.WriteLine(ctlFileInfo.ToString());
        //        sWriter.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (ctlFileInfo != null)
        //        {
        //            ctlFileInfo = null;
        //        }

        //        if (sWriter != null)
        //        {
        //            sWriter = null;
        //        }
        //    }
        //}


        ///// <summary>
        ///// To write the control file for DART
        ///// </summary>
        ///// <param name="csvFileName">the csv file name</param>
        ///// <param name="folderPath">the folder path</param>
        ///// <param name="backupZipFileSubPath">the backup zip file subpath</param>
        ///// <param name="reportFilePath">the report file path</param>
        ///// <param name="etlFolderPath">the etl folder path</param>
        ///// <param name="recordCount">the total record count</param>
        //public static void WriteControlFileForDART(string csvFileName, string folderPath,
        //                                    string backupZipFileSubPath, string reportFilePath,
        //                                    string etlFolderPath) // int recordCount
        //{
        //    try
        //    {
        //        // To compress and backup the data
        //        CompressAndBackup(csvFileName, folderPath, backupZipFileSubPath, etlFolderPath);
        //        // To write the control file
        //        WriteControlFile(csvFileName.Replace(CSV_FILEEXTN, string.Empty), recordCount, etlFolderPath);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion

        public static DataSet GetWebRequest(string formattedUrl)
        {
            const string WEBMETHOD = "POST";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(formattedUrl);
            request.Method = WEBMETHOD;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            DataSet dsReportData = new DataSet();
            dsReportData.ReadXml(reader);
            return dsReportData;
        }
    }

    /// <summary>
    /// The severity level of the activity
    /// </summary>
    public struct SeverityLevel
    {
        public const int INFORMATION = 0;
        public const int WARNING = 1;
        public const int ERROR_LEVEL1 = 2;
        public const int ERROR_LEVEL2 = 3;
    }

    /// <summary>
    /// The flag to indicate whether the email notification should be sent to the 
    /// configured recipients on the error message / activity
    /// </summary>
    public struct EmailNotification
    {
        public const int NOT_NEEDED = 0;
        public const int SEND = 1;
    }

    /// <summary>
    /// The repeat frequency of the schedule
    /// </summary>
    public struct RepeatFrequency
    {
        public const string HOURLY = "H";
        public const string DAILY = "D";
        public const string WEEKLY = "W";
        public const string MONTHLY = "M";
        public const string YEARLY = "Y";
    }

    /// <summary>
    /// The facebook insights period
    /// </summary>
    public struct FBIPeriod
    {
        public const string DAILY = "D";
        public const string WEEKLY = "W";
        public const string MONTHLY = "M";
        public const string LIFETIME = "L";
        public const string TWENTY_EIGHT_DAYS = "T";
    }
}
