﻿namespace APIGrabberScheduler
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.APIGrabberProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.APIGrabberServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // APIGrabberProcessInstaller
            // 
            this.APIGrabberProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.APIGrabberProcessInstaller.Password = null;
            this.APIGrabberProcessInstaller.Username = null;
            // 
            // APIGrabberServiceInstaller
            // 
            this.APIGrabberServiceInstaller.Description = "API Grabber Scheduler Service";
            this.APIGrabberServiceInstaller.ServiceName = "API Grabber Scheduler";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.APIGrabberProcessInstaller,
            this.APIGrabberServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller APIGrabberProcessInstaller;
        private System.ServiceProcess.ServiceInstaller APIGrabberServiceInstaller;
    }
}