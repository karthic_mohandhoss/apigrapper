﻿/******************************************************************************************
Filename        : Program.cs
Purpose         : A windows service scheduler that can be executed as a console application,
                    based on the command line argument passed to api grab the data
Created by      : Raja B
Created date    : 15-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
15-Mar-2013     Raja B                              Initial created
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Configuration;

using APIGrabbers;
using APIGrabberUtility;

namespace APIGrabberScheduler
{
    static class Program
    {
        // Error code (for general activities) for api grabber console application. 
        // Used while activity logging
        private const int APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE = 20;

        // Error code (for error activities) for api grabber console application. 
        // Used while activity logging
        private const int APIGRABBER_SCHEDULER_CONSOLE_MODE_ERROR_ACTIVITY_CODE = -20;

        // The activity event log name and the local ip
        private static string activityLogNameIP = string.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // Name for api grabber console application. Used while activity logging
            const string APIGRABBER_SCHEDULER_CONSOLE_MODE_ACTIVITY_NAME = "API Grabber Console Mode : ";

            // To get the activity event log name and the local ip
            activityLogNameIP = UtilityHelper.GetActivityLogNameIP(APIGRABBER_SCHEDULER_CONSOLE_MODE_ACTIVITY_NAME);

            // Command line argument for dfa for dart sari bodner account
            if (args.Contains("-dfafordartsb"))
            {
                StartDFAforDARTSB();
            }// Command line argument for ga verve account
            else if (args.Contains("-gaverve"))
            {
                StartGAVerve();
            }// Command line argument for fbi verve account
            else if (args.Contains("-fbiverve"))
            {
                StartFBIVerve();
            }
            else if (args.Contains("-vindicosb"))
            {
                StartVindicoSB();
            }
            else // otherwise, execute the api grabber scheduler as a windows service
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new APIGrabberService() 
			    };
                ServiceBase.Run(ServicesToRun);
            }
        }

        /// <summary>
        /// To trigger dfa for dart sari bodner account api grabber
        /// </summary>
        private static void StartDFAforDARTSB()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                "DFA for DART (Sari Bodner Account) - API Grabber console application started..",
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DFAforDARTSB dfa = new DFAforDARTSB();
                dfa.ExecuteJob();

                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                            "DFA for DART (Sari Bodner Account) - API Grabber console application completed",
                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_ERROR_ACTIVITY_CODE, activityLogNameIP +
                                    "Error on StartDFAforDARTSB - \n Message : " + ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
        }

        /// <summary>
        /// To trigger GA verve account api grabber
        /// </summary>
        private static void StartGAVerve()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                "Google Analytics (Verve Account) - API Grabber console application started..",
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                GAVerve ga = new GAVerve();
                ga.ExecuteJob();

                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                            "Google Analytics (Verve Account) - API Grabber console application completed",
                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_ERROR_ACTIVITY_CODE, activityLogNameIP +
                                    "Error on StartGAVerve - \n Message : " + ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }

        }

        /// <summary>
        /// To trigger fbi verve account api grabber
        /// </summary>
        private static void StartFBIVerve()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                "Facebook Insights (Verve Account) - API Grabber console application started..",
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                FBIVerve fbi = new FBIVerve();
                fbi.ExecuteJob();

                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                            "Facebook Insights (Verve Account) - API Grabber console application completed",
                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_ERROR_ACTIVITY_CODE, activityLogNameIP +
                                    "Error on StartFBIVerve - \n Message : " + ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
        }

        /// <summary>
        /// To trigger vindico sari bodner account api grabber
        /// </summary>
        private static void StartVindicoSB()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                "Vindico (Sari Bodner Account) - API Grabber console application started..",
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                VindicoSB vindico = new VindicoSB();
                vindico.ExecuteJob();

                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                            "Vindico (Sari Bodner Account) - API Grabber console application completed",
                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_ERROR_ACTIVITY_CODE, activityLogNameIP +
                                    "Error on StartVindicoSB - \n Message : " + ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
        }

        /// <summary>
        /// To log the activities
        /// </summary>
        /// <param name="eventCode">the event code</param>
        /// <param name="message">the message</param>
        /// <param name="stackTrace">the stack trace of the error, if any</param>
        /// <param name="severityLevel">the severity level</param>
        /// <param name="notify">notify value, whether the email should be triggered</param>
        private static void ActivityLog(int eventCode, string message,
                                    string stackTrace, int severityLevel, int notify)
        {
            try
            {
                // ID for api grabber console application. Used while activity logging
                const int APIGRABBER_SCHEDULER_CONSOLE_MODE_SERVICE_ID = 20;

                // Flag, whether to enable the windows event - activity logging
                bool enableWindowsEventLogging = false;

                // Flag, whether to enable the data base - activity logging
                bool enableDatabaseLogging = false;

                // Try parsing the enable windows event logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enablewindowseventlogging"], out enableWindowsEventLogging);

                // Try parsing the enable database logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enabledatabaselogging"], out enableDatabaseLogging);

                UtilityHelper.EnableWindowsEventLogging = enableWindowsEventLogging;
                UtilityHelper.EnableDatabaseLogging = enableDatabaseLogging;

                UtilityHelper.ActivityLog(APIGRABBER_SCHEDULER_CONSOLE_MODE_SERVICE_ID, eventCode, message, stackTrace,
                                        severityLevel, notify);
            }
            catch (Exception) { }
        }
    }
}
