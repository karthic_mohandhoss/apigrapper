﻿/******************************************************************************************
Filename        : APIGrabberService.cs
Purpose         : A windows service scheduler for executing the API grabbers                     
Created by      : Raja B
Created date    : 15-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
15-Mar-2013     Raja B                              Initial created
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Configuration;
using System.Reflection;
using System.Threading;
using System.Timers;
using System.Xml;
using System.IO;

using APIGrabbers;
using APIGrabberUtility;

namespace APIGrabberScheduler
{
    public partial class APIGrabberService : ServiceBase
    {   
        // Namespace of the api grabbers, where the googleanalytics.cs,
        // facebookinsights.cs, etc exist
        private const string APIGRABBER_NAMESPACE = "APIGrabbers."; 

        // Date format of the scheduled date
        private const double TIMER_INTERVAL = 60000;

        // The timer for loading the schedule data (tbl_schedule) in a definite interval.
        // the interval is mentioned in the const - TIMER_INTERVAL
        readonly System.Timers.Timer timer = new System.Timers.Timer();

        // To have assembly reference to api grabbers
        private Assembly apiAssembly = null;

        // The dataset for schedule (tbl_schedules data)
        IEnumerable<Tbl_Schedule> tblSchedules = null;

        // Flag to indicate the set of data source are in progress
        private bool workInProgress = false;

        // Error code (for general activities) for api grabber scheduler. 
        // Used while activity logging
        private const int APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE = 10;

        // Error code (for error activities) for api grabber scheduler. 
        // Used while activity logging
        private const int APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE = -10;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        public APIGrabberService()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// On Start method
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            try
            {
                // Name for api grabber scheduler. Used while activity logging
                const string APIGRABBER_SCHEDULER_SERVICE_ACTIVITY_NAME = "API Grabber Scheduler : ";

                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(APIGRABBER_SCHEDULER_SERVICE_ACTIVITY_NAME);

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP +  "Service Started..", 
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);


                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                "All inprocess datasources reset started...", 
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Upon OnStart - reset all the inprocess datasources
                DataAccessUtility.ResetAllInprocessDatasources();

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                "All inprocess datasources reset completed", 
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Load the api assemblies
                LoadAPIAssembly();

                timer.Interval = TIMER_INTERVAL;
                timer.Elapsed += ExecuteAPIGrabbers;
                timer.Start();
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                    "Error OnStart - \n Message : " + ex.Message, ex.StackTrace, 
                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);  
            } 
        }

        /// <summary>
        /// To load the api using reflection as per the scheduled time
        /// </summary>
        private void LoadAPIAssembly()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                "API Assembly loading started..", string.Empty, 
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Load the api grabber assembly
                if (apiAssembly == null)
                    apiAssembly = Assembly.GetAssembly(typeof(APIGrabbers.IAPIGrabber));

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "API Assembly loading completed. Assembly name is : " + apiAssembly.FullName,
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                    "Error loading the assembly - \n Message : " + ex.Message, ex.StackTrace, 
                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
                throw ex;
            }
        }

        /// <summary>
        /// To execute the respective API grabbers as per the schedule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void ExecuteAPIGrabbers(object sender, ElapsedEventArgs args)
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method started..", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                        "Execute API Grabbers method : Work Inprogress status is " + workInProgress.ToString(),
                                        string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // If the previous schedule is still running then return
                if (workInProgress) return;

                // Otherwise, load the schedule data from tbl_schedule
                LoadSchedule();

                if (tblSchedules != null)
                {   
                    //Set the work in progress status to true
                    workInProgress = true;
                        
                    foreach (var tblSchedule in tblSchedules)
                    {
                        string dateAndTime = tblSchedule.Dateandtime.ToString();
                        string dsCode = tblSchedule.Dscode.ToString();
                        string dsClassName = tblSchedule.Dsclassname == null ? string.Empty : tblSchedule.Dsclassname;

                        // Get the api ( the datasource class name ) scheduled to run
                        string apiClassName = GetAPIGrabberToRun(dateAndTime,dsCode,dsClassName);
                                                                    
                        // Get the data source code (dsCode) for the api scheduled to run
                        int apiDSCode = 0;
                        int.TryParse(dsCode, out apiDSCode);

                        ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method : api code = " + apiDSCode.ToString(), 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                        if (!(string.IsNullOrEmpty(apiClassName)))
                        {
                            ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method :The api scheduled to be run is " + apiClassName, 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                            // Kick-off the respective api grabber to run as per the schedule time elapsed
                            IAPIGrabber apiGrabber = CreateAPIGrabberInstance(apiClassName);
                            Thread thread = new Thread(() => DataGrab(apiGrabber));
                            thread.Start();

                            ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method : The api grabber is started : " + apiClassName, 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                            // Update the schedule with the next run time and make the schedule to inprocess
                            DataAccessUtility.UpdateScheduleToInprocess(apiDSCode);

                            ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method : The schedule is updated to inprocess for : " + apiClassName, 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                            ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Execute API Grabbers method completed", 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                        }
                    }  
                }               
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                "Error on ExecuteAPIGrabbers method" + "- \n Message : " + ex.Message, ex.StackTrace,
                                SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                // Reset the work in progress to false
                workInProgress = false;
            }
        }

        /// <summary>
        /// To trigger the execute job method of the respective apis
        /// </summary>
        /// <param name="objApiGrabber">the api grabber object</param>
        private void DataGrab(object objApiGrabber)
        {
            string scheduleName = string.Empty;
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "DataGrab method : Start", 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                IAPIGrabber apiGrabber = objApiGrabber as IAPIGrabber;

                // Check whether the api grabber object that is scheduled to run is evaluated to null.
                // if so, return
                if (apiGrabber == null) return;

                // Othewise, get the scheduled api grabber
                scheduleName = apiGrabber.GetType().ToString();

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "DataGrab method : Scheduled data source name is " + scheduleName, 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "DataGrab method : " + scheduleName + " grabber execute job method to be called..", 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // The respective apiGrabber starts....  
                apiGrabber.ExecuteJob();
                // The apiGrabber run completed successfully 

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "DataGrab method : " + 
                                    scheduleName + " grabber execute job method call is completed", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                    "Error executing " + scheduleName + " - \n Message : " + ex.Message, ex.StackTrace,
                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }            
        }

        /// <summary>
        /// Load the schedule data (tbl_schedule) to the dataset
        /// </summary>
        private void LoadSchedule()
        {
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "Schedule loading started..", 
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Load the schedules
                tblSchedules = DataAccessUtility.GetSchedule();

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "Schedule loading completed.", 
                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                    "Error loading the schedule - \n Message : " + ex.Message, ex.StackTrace,
                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
                throw ex;
            }
        }

        /// <summary>
        /// To get the api grabbers to be run as per the schedule from 
        /// the schedule (tbl_schedule) dataset
        /// </summary>
        /// <param name="dateAndTime">the scheduled date time</param>
        /// <param name="dsCode">the data source code</param>
        /// <param name="dsClassName">the data source class name</param>
        /// <returns>the api name (the api scheduled to run)</returns>
        private string GetAPIGrabberToRun(string dateAndTime, string dsCode, string dsClassName)
        {
            string apiName = string.Empty;
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP +
                                    "Get API Grabber To Run method started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // The scheduled date time
                DateTime scheduledTime;
                DateTime.TryParse(dateAndTime, out scheduledTime);

                // The data source code
                int apiDSCode = 0;
                int.TryParse(dsCode, out apiDSCode);

                if (!(DateTime.Now < scheduledTime))
                {
                    // Assign the api name
                    apiName = dsClassName;

                    // If the apiName is empty, raise the error
                    if (string.IsNullOrEmpty(apiName))
                    {
                        ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                        "GetAPIGrabberToRun method : API class name is empty", string.Empty,
                                        SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                    }
                    else
                    {
                        ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                        "GetAPIGrabberToRun method : API Datasource code = " + dsCode + 
                                        " API class name = " + apiName, string.Empty, SeverityLevel.INFORMATION, 
                                        EmailNotification.NOT_NEEDED);
                    }                   
                }

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                        "Get API Grabber To Run method completed",
                                        string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP + 
                                    "Error getting the scheduled api grabber to run - \n Message : " + ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return apiName;
        }
       
        /// <summary>
        /// To create the respective api grabber instance using the reflection
        /// </summary>
        /// <param name="apiClassName">the class file name of the api instance be created</param>
        /// <returns>the api grabber instance</returns>
        private IAPIGrabber CreateAPIGrabberInstance(string apiClassName)
        {
            IAPIGrabber apiGrabber = null;
            try
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "Create API Grabber Instance method started", 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Concatenate the api grabbers namespace and the respective class file name to 
                // make the fully qualified api grabbers assembly name.
                string apiGrabberAssemblyFullName = APIGRABBER_NAMESPACE + apiClassName;

                if (apiAssembly == null)
                {
                    throw new Exception("Create API Grabber Instance method : " +
                                            "The API Grabber assembly is null, cannot proceed further..");
                }

                //Create an instance of the API Grabber
                apiGrabber = (IAPIGrabber)apiAssembly.CreateInstance(apiGrabberAssemblyFullName, true);

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                    "CreateAPIGrabberInstance method : API Grabber is : " + apiGrabber.ToString(),
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + 
                                "Create API Grabber Instance method completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ERROR_ACTIVITY_CODE, activityLogNameIP +  
                                    "Error on API Grabber instance creation " + "- \n Message : " + ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return apiGrabber;
        }

        /// <summary>
        /// To log the activities
        /// </summary>
        /// <param name="eventCode">the event code</param>
        /// <param name="message">the message</param>
        /// <param name="stackTrace">the stack trace of the error, if any</param>
        /// <param name="severityLevel">the severity level</param>
        /// <param name="notify">notify value, whether the email should be triggered</param>
        private void ActivityLog(int eventCode, string message, string stackTrace, int severityLevel, int notify)
        {
            try
            {
                // ID for api grabber scheduler. Used while activity logging
                const int APIGRABBER_SCHEDULER_SERVICE_ID = 10;

                // Flag, whether to enable the windows event - activity logging
                bool enableWindowsEventLogging = false;

                // Flag, whether to enable the data base - activity logging
                bool enableDatabaseLogging = false;

                // Try parsing the enable windows event logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enablewindowseventlogging"], out enableWindowsEventLogging);

                // Try parsing the enable database logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enabledatabaselogging"], out enableDatabaseLogging);

                UtilityHelper.EnableWindowsEventLogging = enableWindowsEventLogging;
                UtilityHelper.EnableDatabaseLogging = enableDatabaseLogging;

                UtilityHelper.ActivityLog(APIGRABBER_SCHEDULER_SERVICE_ID, eventCode, message, stackTrace,
                                       severityLevel, notify);

            }
            catch (Exception) { }
        }

        /// <summary>
        /// On stop method
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                // Activity log that the api scheduler service is stopped
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "Service Stopped",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// On pause method
        /// </summary>
        protected override void OnPause()
        {
            base.OnPause();
            try
            {
                // Activity log that the api scheduler service is stopped
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "Service Paused",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// On continue method
        /// </summary>
        protected override void OnContinue()
        {
            base.OnContinue();
            try
            {
                // Activity log that the api scheduler service is stopped
                ActivityLog(APIGRABBER_SCHEDULER_SERVICE_GENERAL_ACTIVITY_CODE, activityLogNameIP + "Service Continue",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception) { }
        }        
    }
}
