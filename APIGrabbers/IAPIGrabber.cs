﻿/******************************************************************************************
Filename        : IAPIGrabber.cs
Purpose         : Interface for the API grabbers                    
Created by      : Raja B
Created date    : 14-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
14-Mar-2013     Raja B                              Initial created
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APIGrabbers
{
    public interface IAPIGrabber
    {
        // Entry method for any datasource - API grabbers
        bool ExecuteJob();
    }
}
