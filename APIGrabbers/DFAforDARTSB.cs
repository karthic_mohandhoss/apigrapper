﻿/******************************************************************************************
Filename        : DFAforDARTSB.cs
Purpose         : API Grabber for DFA for DART - Sari Bodner account
                    
Created by      : Raja
Created date    : 22-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
22-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using APIGrabberUtility;
using APIGrabberDataSources;

namespace APIGrabbers
{
    public class DFAforDARTSB : APIGrabber, IAPIGrabber
    {
        // Error code (for general activities) for dfa for dart api. 
        // Used while activity logging
        private const int DFA_FOR_DART_GENERAL_ACTIVITY_CODE = 100;

        // Error code (for error activities) for dfa for dart api. 
        // Used while activity logging
        private const int DFA_FOR_DART_ERROR_ACTIVITY_CODE = -100;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the DFA for DART run for Sari Bodner Account
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To retrieve the event log config from the app.config file
                base.GetEventLogConfig();

                // Name for dfa for dart api. Used while activity logging
                const string DFA_FOR_DART_ACTIVITY_NAME = "DFA for DART (Sari Bodner Account) : ";
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(DFA_FOR_DART_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.DFAforDARTSBDSCode, DFA_FOR_DART_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Initiate the dfa for dart object and variables
                DFAforDART dfaForDART = new DFAforDART(UtilityHelper.DFAforDARTSBDSCode,
                                                       activityLogNameIP,
                                                       DFA_FOR_DART_GENERAL_ACTIVITY_CODE,
                                                       DFA_FOR_DART_ERROR_ACTIVITY_CODE);

                // Initiate the data grabbing
                jobSucceeded = dfaForDART.InitiateDataGrabbing();

                UtilityHelper.ActivityLog(UtilityHelper.DFAforDARTSBDSCode, DFA_FOR_DART_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.DFAforDARTSBDSCode, DFA_FOR_DART_ERROR_ACTIVITY_CODE,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            return jobSucceeded;
        }
    }
}