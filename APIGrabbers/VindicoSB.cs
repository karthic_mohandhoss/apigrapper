﻿/******************************************************************************************
Filename        : VindicoSB.cs
Purpose         : API Grabber for Vindico - Sari Bodner account
                    
Created by      : Raja
Created date    : 08-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
08-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using APIGrabberUtility;
using APIGrabberDataSources;

namespace APIGrabbers
{
    public class VindicoSB : APIGrabber, IAPIGrabber
    {
        // Error code (for general activities) for vindico api. 
        // Used while activity logging
        private const int VINDICO_GENERAL_ACTIVITY_CODE = 500;

        // Error code (for error activities) for vindico api. 
        // Used while activity logging
        private const int VINDICO_ERROR_ACTIVITY_CODE = -500;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the Vindico run for Sari Bodner Account
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To retrieve the event log config from the app.config file
                base.GetEventLogConfig();

                // Name for vindico api. Used while activity logging
                const string VINDICO_ACTIVITY_NAME = "Vindico (Sari Bodner Account) : ";
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(VINDICO_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.VindicoSBDSCode, VINDICO_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Initiate the vindico object and variables
                Vindico vindico = new Vindico(UtilityHelper.VindicoSBDSCode,
                                                       activityLogNameIP,
                                                       VINDICO_GENERAL_ACTIVITY_CODE,
                                                       VINDICO_ERROR_ACTIVITY_CODE);

                // Initiate the data grabbing
                jobSucceeded = vindico.InitiateDataGrabbing();

                UtilityHelper.ActivityLog(UtilityHelper.VindicoSBDSCode, VINDICO_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.VindicoSBDSCode, VINDICO_ERROR_ACTIVITY_CODE,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            return jobSucceeded;
        }
    }
}