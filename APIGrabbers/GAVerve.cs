﻿/******************************************************************************************
Filename        : GAVerve.cs
Purpose         : API Grabber for GA - Verve account
                    
Created by      : Raja
Created date    : 23-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
23-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using APIGrabberUtility;
using APIGrabberDataSources;

namespace APIGrabbers
{
    public class GAVerve : APIGrabber, IAPIGrabber
    {
        // Error code (for general activities) for google analytics api. 
        // Used while activity logging
        private const int GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE = 300;

        // Error code (for error activities) for google analytisc api. 
        // Used while activity logging
        private const int GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE = -300;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the GA run for Verve Account
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To retrieve the event log config from the app.config file
                base.GetEventLogConfig();

                // Name for google analytics api. Used while activity logging
                const string GOOGLE_ANALYTICS_ACTIVITY_NAME = "Google Analytics (Verve Account) : ";
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(GOOGLE_ANALYTICS_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.GAVerveDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Initiate the dfa for dart object and variables
                GoogleAnalytics googleAnalytics = new GoogleAnalytics(UtilityHelper.GAVerveDSCode,
                                                       activityLogNameIP,
                                                       GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                                       GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE);

                // Initiate the data grabbing
                jobSucceeded = googleAnalytics.InitiateDataGrabbing();

                UtilityHelper.ActivityLog(UtilityHelper.GAVerveDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.GAVerveDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            return jobSucceeded;
        }
    }
}