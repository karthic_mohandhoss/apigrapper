﻿/******************************************************************************************
Filename        : VizuPD.cs
Purpose         : API Grabber for Vizu - Pramod Dikshith account
                    
Created by      : Raja
Created date    : 27-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
27-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using APIGrabberUtility;
using APIGrabberDataSources;

namespace APIGrabbers
{
    public class VizuPD : APIGrabber, IAPIGrabber
    {
        // Error code (for general activities) for vizu api. 
        // Used while activity logging
        private const int VIZU_GENERAL_ACTIVITY_CODE = 1000;

        // Error code (for error activities) for vizu api. 
        // Used while activity logging
        private const int VIZU_ERROR_ACTIVITY_CODE = -1000;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the vizu run for Pramod Dikshith Account
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To retrieve the event log config from the app.config file
                base.GetEventLogConfig();

                // Name for vizu api. Used while activity logging
                const string VIZU_ACTIVITY_NAME = "Vizu (Pramod Dikshith Account) : ";
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(VIZU_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.VizuPDDSCode, VIZU_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Initiate the vizu object and variables
                Vizu vizu = new Vizu(UtilityHelper.VizuPDDSCode,
                                                       activityLogNameIP,
                                                       VIZU_GENERAL_ACTIVITY_CODE,
                                                       VIZU_ERROR_ACTIVITY_CODE);

                // Initiate the data grabbing
                jobSucceeded = vizu.InitiateDataGrabbing();

                UtilityHelper.ActivityLog(UtilityHelper.VizuPDDSCode, VIZU_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.VizuPDDSCode, VIZU_ERROR_ACTIVITY_CODE,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            return jobSucceeded;
        }
    }
}