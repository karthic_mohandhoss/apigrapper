﻿/******************************************************************************************
Filename        : FBIVerve.cs
Purpose         : API Grabber for FBI - Verve account
                    
Created by      : Raja
Created date    : 22-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
22-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using APIGrabberUtility;
using APIGrabberDataSources;

namespace APIGrabbers
{
    public class FBIVerve : APIGrabber, IAPIGrabber
    {
        // Error code (for general activities) for facebook insights api. 
        // Used while activity logging
        private const int FACEBOOK_INSIGHTS_GENERAL_ACTIVITY_CODE = 400;

        // Error code (for error activities) for facebook insights api. 
        // Used while activity logging
        private const int FACEBOOK_INSIGHTS_ERROR_ACTIVITY_CODE = -400;

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the FBI run for Verve Account
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To retrieve the event log config from the app.config file
                base.GetEventLogConfig();

                // Name for facebook insights api. Used while activity logging
                const string FACEBOOK_INSIGHTS_ACTIVITY_NAME = "Facebook Insights (Verve Account) : ";
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(FACEBOOK_INSIGHTS_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.FBIVerveDSCode, FACEBOOK_INSIGHTS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Initiate the fbi object and variables
                FacebookInsights fbi = new FacebookInsights(UtilityHelper.FBIVerveDSCode,
                                                       activityLogNameIP,
                                                       FACEBOOK_INSIGHTS_GENERAL_ACTIVITY_CODE,
                                                       FACEBOOK_INSIGHTS_ERROR_ACTIVITY_CODE);

                // Initiate the data grabbing
                jobSucceeded = fbi.InitiateDataGrabbing();

                UtilityHelper.ActivityLog(UtilityHelper.FBIVerveDSCode, FACEBOOK_INSIGHTS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.FBIVerveDSCode, FACEBOOK_INSIGHTS_ERROR_ACTIVITY_CODE,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            return jobSucceeded;
        }
    }
}
