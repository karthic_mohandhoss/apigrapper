﻿///******************************************************************************************
//Filename        : KenshooAds.cs
//Purpose         : To connect to the Kenshoo API to grab the data.
//                    https://ks123.kenshoo.com/services/KenshooReportsRepositoryV1?wsdl
//Created by      : Raja B
//Created date    : 14-Mar-2013

//Revision history :
//-------------------------------------------------------------------------------------------
//Date            Modified by         Request Id      Change details
//-------------------------------------------------------------------------------------------
//14-Mar-2013     Raja B                              Initial created
  
//*******************************************************************************************/

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Configuration;
//using System.Threading;

//using System.IO;
//using System.Xml;
//using System.Data;

//using APIGrabberUtility;
//using APIGrabbers.KenshooAdsAPI;
//using System.Net;

//namespace APIGrabbers
//{
//    public class KenshooAds : IAPIGrabber
//    {
//        private string userName = string.Empty;
//        private string password = string.Empty;

//        private string rootFolderPath = string.Empty;
//        private string profilesZipFileName = string.Empty;
//        private string xmlReportFolderName = string.Empty;
//        private string backupZipFolderName = string.Empty;
//        private string reportFileName = string.Empty;
//        private string etlFolderPath = string.Empty;

//        private DateTime startDate = DateTime.Now;
//        private DateTime endDate = DateTime.Now;
//        private string repeatFrequency = string.Empty;

//        private const string DAILYADS_REPORTNAME = "DailyAds_";
//        private const string DATEFORMAT = "yyyy-MM-dd";
//        private const string XMLFILE_EXTN = "*.xml";
//        private const int ITERATIVEREPORTSTATUSCHECK_INSECONDS = 5000; // 5 seconds

//        /// <summary>
//        /// Entry point method for the Kenshoo report run
//        /// </summary>
//        /// <returns>True, if successful. Otherwise false</returns>
//        public bool ExecuteJob()
//        {
//            bool jobSucceeded = false;

//            KenshooReportAPIV1Service kenshooReportService = null;
//            string apiToken = string.Empty;
//            try
//            {
//                GetConfigInfo();

//                int dsCode = UtilityHelper.DFAforDARTDSCode;

//                kenshooReportService = new KenshooReportAPIV1Service();
//                apiToken = Authenticate(kenshooReportService, userName, password);

//                if (!string.IsNullOrEmpty(apiToken))
//                {
//                    jobSucceeded = RetrieveZipFileData(kenshooReportService, userName, password, apiToken);
//                    if (!jobSucceeded)
//                    {
//                        throw new Exception("Kenshoo Ads : Execute Job : Error occurred during kenshoo report generation");
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                            SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            finally
//            {
//                if (kenshooReportService != null)
//                {
//                    kenshooReportService = null;
//                }
//                //DataAccessUtility.UpdateScheduleCompleted(UtilityHelper.KenshooAdsDSCode.ToString());
//            }
//            return jobSucceeded;
//        }

//        #region getconfiginfo() - config from app.config - old implementation
//        /// <summary>
//        /// To retrieve the configuration information
//        /// </summary>
//        //private void GetConfigInfo()
//        //{
//        //    try
//        //    {
//        //        userName = ConfigurationManager.AppSettings["kenshooads_username"];
//        //        password = ConfigurationManager.AppSettings["kenshooads_password"];
//        //        rootFolderPath = ConfigurationManager.AppSettings["kenshooads_rootfolderpath"];
//        //        profilesZipFileName = ConfigurationManager.AppSettings["kenshooads_profileszipfilename"];
//        //        xmlReportFolderName = ConfigurationManager.AppSettings["kenshooads_xmlreportfoldername"];
//        //        backupZipFolderName = ConfigurationManager.AppSettings["kenshooads_backupzipfoldername"];
//        //        reportFileName = ConfigurationManager.AppSettings["kenshooads_reportfilename"];
//        //        etlFolderPath = ConfigurationManager.AppSettings["kenshooads_etlfolderpath"];

//        //        DateTime.TryParse(ConfigurationManager.AppSettings["kenshooads_startdate"], out startDate);
//        //        DateTime.TryParse(ConfigurationManager.AppSettings["kenshooads_enddate"], out endDate);  
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//        //                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//        //    }
//        //}
//        #endregion

//        private void GetConfigInfo()
//        {
//            try
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 0, "Kenshoo Ads : GetConfigInfo() started..",
//                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

//                DatasourceConfig datasourceConfig = new DatasourceConfig();
//                datasourceConfig = DataAccessUtility.GetDatasourceConfig(UtilityHelper.KenshooAdsDSCode);

//                if (datasourceConfig != null)
//                {
//                    userName = datasourceConfig.Username;
//                    if (string.IsNullOrEmpty(userName))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Username, config info not found in DB");
//                    }

//                    password = datasourceConfig.Password;
//                    if (string.IsNullOrEmpty(password))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Password, config info not found in DB");
//                    }

//                    rootFolderPath = datasourceConfig.RootFolderPath;
//                    if (string.IsNullOrEmpty(rootFolderPath))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
//                    }

//                    xmlReportFolderName = datasourceConfig.ReportFolderName; // xml report folder name
//                    if (string.IsNullOrEmpty(xmlReportFolderName))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - xml report folder name, " +
//                                                "config info not found in DB");
//                    }

//                    reportFileName = datasourceConfig.ReportFileName;
//                    if (string.IsNullOrEmpty(reportFileName))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Report Filename, config info not found in DB");
//                    }

//                    backupZipFolderName = datasourceConfig.BackupZipFolderName;
//                    if (string.IsNullOrEmpty(backupZipFolderName))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
//                    }

//                    etlFolderPath = datasourceConfig.EtlFolderPath;
//                    if (string.IsNullOrEmpty(etlFolderPath))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
//                    }

//                    // start date ; end date
//                    if (repeatFrequency.ToUpper() == "D") //daily
//                    {
//                        startDate = DateTime.Now.AddDays(-1);
//                        endDate = DateTime.Now.AddDays(-1);
//                    }
//                    else if (repeatFrequency.ToUpper() == "W") //weekly
//                    {
//                        startDate = DateTime.Now.AddDays(-7);
//                        endDate = DateTime.Now.AddDays(-1);
//                    }
//                    else if (repeatFrequency.ToUpper() == "M") //monthly
//                    {
//                        startDate = DateTime.Now.AddMonths(-1);
//                        endDate = DateTime.Now.AddDays(-1);
//                    }
//                    else if (repeatFrequency.ToUpper() == "Y") //yearly
//                    {
//                        startDate = DateTime.Now.AddYears(-1);
//                        endDate = DateTime.Now.AddDays(-1);
//                    }
//                    else
//                    {
//                        startDate = datasourceConfig.StartDate;
//                        endDate = datasourceConfig.EndDate;
//                    }

//                    profilesZipFileName = datasourceConfig.Dsconfig1; // profiles zip file name
//                    if (string.IsNullOrEmpty(profilesZipFileName))
//                    {
//                        throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - profiles zip filename (Dsconfig1), " +
//                                                "config info not found in DB");
//                    }
//                }
//                else
//                {
//                    throw new Exception("Kenshoo Ads : GetConfigInfo : Fatal error - Config info not found in DB");
//                }

//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 0, "Kenshoo Ads : GetConfigInfo() completed.",
//                                   string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                            SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//        }

//        /// <summary>
//        /// To authenticate with kenshoo and get the api token
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo service object</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <returns>API token</returns>
//        private string Authenticate(KenshooReportAPIV1Service kenshooReportService, string userName, string password)
//        {
//            string apiToken = string.Empty;
//            try
//            {
//                apiToken = kenshooReportService.getKenshooAPIToken(userName, password);

//                // Empty if User or Password are not valid
//                if (string.IsNullOrEmpty(apiToken))
//                {
//                    throw new Exception("Kenshoo API : Authenticate : Invalid username or password. Authentication failed.");
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return apiToken;
//        }

//        /// <summary>
//        /// To retrieve and File I/O the kenshoo data
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo service object</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <param name="apiToken">API token</param>
//        /// <returns>True, if successful. Otherwise false</returns>
//        private bool RetrieveZipFileData(KenshooReportAPIV1Service kenshooReportService, string userName,
//                                                                        string password, string apiToken)
//        {
//            bool result = false;

//            string profileZipFilePath = rootFolderPath + profilesZipFileName;
//            string reportXmlFileSubPath = rootFolderPath + xmlReportFolderName;
//            string backupZipFileSubPath = rootFolderPath + backupZipFolderName;

//            try
//            {
//                if (startDate > DateTime.Now)
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : Report start date is later to today");
//                }
//                else if (startDate > endDate)
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : Report start date is later to end date");
//                }
//                Dictionary<string, string> dateRange = GetDateRange(startDate, endDate);

//                if (!(UtilityHelper.VerifyFolderExists(rootFolderPath) &&
//                    UtilityHelper.VerifyFolderExists(reportXmlFileSubPath) &&
//                    UtilityHelper.VerifyFolderExists(backupZipFileSubPath)))
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : Kenshoo folder path does not exist");
//                }

//                int apiStatus = GetProfileStatus(kenshooReportService, userName, password, apiToken);
//                if (apiStatus == 0)
//                {
//                    byte[] file = GetZippedData(kenshooReportService, userName, password, apiToken);
//                    WriteToZipFile(profileZipFilePath, file);
//                }
//                else // > 0
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : Error occurred during profile list preparation");
//                }

//                UtilityHelper.CleanUpXMLFiles(reportXmlFileSubPath);
//                UtilityHelper.Decompress(rootFolderPath, reportXmlFileSubPath, backupZipFileSubPath);

//                DataSet dsProfile = LoadXmlDataFile(reportXmlFileSubPath);
//                if (dsProfile.Tables.Count == 0)
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : Error in loading the profiles data");
//                }

//                if (dsProfile.Tables.Count == 0)
//                {
//                    throw new Exception("Kenshoo API : RetrieveZipFileData : No profile is configured for API grabbing");
//                }

//                DataSet dsProfilesReports = null;
//                DataSet dsReport = null;
//                bool firstReportData = true;

//                foreach (DataRow drProfile in dsProfile.Tables[0].Rows)
//                {
//                    string profileToken = drProfile["Token"].ToString();

//                    foreach (KeyValuePair<string, string> date in dateRange)
//                    {
//                        apiToken = Authenticate(kenshooReportService, userName, password);
//                        apiStatus = GetReportData(kenshooReportService, date.Key, date.Value, userName, password, profileToken, apiToken);
//                        if (apiStatus == 0)
//                        {
//                            byte[] file = GetZippedData(kenshooReportService, userName, password, apiToken);

//                            WriteToZipFile(rootFolderPath + UtilityHelper.FormatDateTimeForFileName() +
//                                UtilityHelper.ZIP_FILEEXTN, file);
//                            UtilityHelper.CleanUpXMLFiles(reportXmlFileSubPath);
//                            UtilityHelper.Decompress(rootFolderPath, reportXmlFileSubPath, backupZipFileSubPath);

//                            dsReport = new DataSet();
//                            dsReport = LoadXmlDataFile(reportXmlFileSubPath);

//                            if (dsReport.Tables.Count != 0)
//                            {
//                                if (firstReportData)
//                                {
//                                    dsProfilesReports = new DataSet();
//                                    dsProfilesReports = GetProfilesReportsSchemaCreated(dsProfile, dsReport, DAILYADS_REPORTNAME);
//                                    firstReportData = false;
//                                }
//                                dsProfilesReports = GetProfilesReportsMerged(dsProfilesReports, drProfile, dsReport.Tables[0]);
//                            }
//                        }
//                        else // > 0
//                        {
//                            throw new Exception("Kenshoo API : RetrieveZipFileData : Error occurred during report data preparation");
//                        }
//                    }
//                }

//                if (dsProfilesReports == null)
//                {
//                    dsProfilesReports = new DataSet();
//                }

//                UtilityHelper.CleanUpXMLFiles(reportXmlFileSubPath);
//                result = UtilityHelper.WriteCSV(dsProfilesReports, rootFolderPath, reportFileName, backupZipFileSubPath, etlFolderPath);
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return result;
//        }

//        /// <summary>
//        /// To created the dataset with the datatable and the column names inline with 
//        /// Profiles and other reports (eg., Daily Ads report) for datacolumns merge.
//        /// </summary>
//        /// <param name="dsProfiles">The profiles dataset</param>
//        /// <param name="dsReport">The reports dataset</param>
//        /// <param name="tableName">Name of the merged new table</param>
//        /// <returns>The newly created empty dataset with the datatable and the merged datacolumns</returns>
//        private DataSet GetProfilesReportsSchemaCreated(DataSet dsProfiles, DataSet dsReport, string tableName)
//        {
//            DataSet dsProfilesReports = new DataSet();

//            DataTable dtProfileReport = new DataTable(tableName);
//            var totalColumns = dsProfiles.Tables[0].Columns.Cast<DataColumn>()
//                  .Concat(dsReport.Tables[0].Columns.Cast<DataColumn>());

//            foreach (var column in totalColumns)
//            {
//                try
//                {
//                    dtProfileReport.Columns.Add(column.ColumnName, column.DataType);
//                }
//                catch (Exception ex)
//                {
//                    dtProfileReport.Columns.Add(tableName + column.ColumnName, column.DataType);
//                }
//            }

//            dsProfilesReports.Tables.Add(dtProfileReport);
//            return dsProfilesReports;
//        }

//        /// <summary>
//        /// To merge the profiles and reports data (eg., daily ads report)
//        /// </summary>
//        /// <param name="dsProfilesReports">The merge dataset</param>
//        /// <param name="drProfile">The profile data</param>
//        /// <param name="dtReport">The report data</param>
//        /// <returns>The merged dataset</returns>
//        private DataSet GetProfilesReportsMerged(DataSet dsProfilesReports, DataRow drProfile, DataTable dtReport)
//        {
//            try
//            {
//                foreach (DataRow drReport in dtReport.Rows)
//                {
//                    var drProfileReport = dsProfilesReports.Tables[0].NewRow();
//                    drProfileReport.ItemArray = drProfile.ItemArray.Concat(drReport.ItemArray).ToArray();
//                    dsProfilesReports.Tables[0].Rows.Add(drProfileReport);
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return dsProfilesReports;
//        }

//        /// <summary>
//        /// To split up the start and end date to collection of weeks date range
//        /// </summary>
//        /// <param name="startDate">The start date</param>
//        /// <param name="endDate">The end date</param>
//        /// <returns>The split up start and end date collection</returns>
//        private Dictionary<string, string> GetDateRange(DateTime startDateRange, DateTime endDateRange)
//        {
//            Dictionary<string, string> dateRange = new Dictionary<string, string>();

//            while (startDateRange <= endDateRange)
//            {
//                DateTime tmpEndDate = startDateRange.AddDays(6);
//                if (tmpEndDate <= endDateRange)
//                {
//                    dateRange.Add(startDateRange.ToString(DATEFORMAT), tmpEndDate.ToString(DATEFORMAT));
//                    startDateRange = tmpEndDate.AddDays(1);
//                }
//                else
//                {
//                    dateRange.Add(startDateRange.ToString(DATEFORMAT), endDateRange.ToString(DATEFORMAT));
//                    startDateRange = tmpEndDate;
//                }
//            }
//            return dateRange;
//        }

//        /// <summary>
//        /// To retrieve the profile report status
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo reporting service object</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <param name="apiToken">API token</param>
//        /// <returns>API status</returns>
//        private int GetProfileStatus(KenshooReportAPIV1Service kenshooReportService, string userName,
//                                                            string password, string apiToken)
//        {
//            /* reportId - Report type. The supported values are -
//                    0 – Daily Campaign report
//                    1 – Campaign Summary report
//                    2 – Daily Keyword report
//                    3 – Keyword Summary report
//                    4 – Profile Listing
//                    10 – Ad Daily
//                    11 – Ad Summary
//             */

//            /* reportOutputType - The requested delivery options. The supported values are - 
//                    0 – Directly downloaded through the API (only this option is currently supported).
//                    1 – The report is mailed to the user’s email address. It may also be downloaded.
//            */

//            /* reportFileType - The report format. The supported values are -              
//                    0 – Tab Separated Values (TSV)
//                    1 – XML format                           
//            */

//            /* startDate - The start date of the report. The date format should be: YYYY-MM-DD             
//                            The start date must be earlier than the current date.
//                            IGNORED FOR PROFILE LISTING REPORT (ReportId=4)
//            */

//            /* endDate - The end date of the report. The date format should be: YYYY-MM-DD             
//                            The end date must be later than ReportStartDate.
//                            IGNORED FOR PROFILE LISTING REPORT (ReportId=4)
//            */

//            // userName
//            // password

//            /* profileToken - To request a report for a specific profile, specify the profile token identifier. 
//                                This is obtained from the Profile Listing report.
//            */

//            // apiToken - Identification token returned by the getKenshooAPIToken method

//            int apiStatus = 0;
//            try
//            {
//                apiStatus = kenshooReportService.generateReport(4, 0, 1, string.Empty, string.Empty,
//                                           userName, password, string.Empty, apiToken);

//                if (apiStatus != 0)
//                {
//                    throw new Exception("Kenshoo API : GetProfileStatus : Error occurred while downloading profile list");
//                }
//                else
//                {
//                    apiStatus = GetReportStatus(kenshooReportService, userName, password, apiToken);
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return apiStatus;
//        }

//        /// <summary>
//        /// To retrieve daily ads data
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo service object</param>
//        /// <param name="startDate">Start date</param>
//        /// <param name="endDate">End date</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <param name="profileToken">Profile token</param>
//        /// <param name="apiToken">API token</param>
//        /// <returns>API status</returns>
//        private int GetReportData(KenshooReportAPIV1Service kenshooReportService, string startDateRange, string endDateRange,
//                                    string userName, string password, string profileToken, string apiToken)
//        {
//            int apiStatus = 0;
//            try
//            {
//                apiStatus = kenshooReportService.generateReport(10, 0, 1, startDateRange, endDateRange,
//                                           userName, password, profileToken, apiToken);

//                if (apiStatus != 0)
//                {
//                    throw new Exception("Kenshoo API : GetReportData : Error occurred while downloading report data. " +
//                                            "Kenshoo error code = " + apiStatus.ToString());
//                }
//                else
//                {
//                    apiStatus = GetReportStatus(kenshooReportService, userName, password, apiToken);
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return apiStatus;
//        }

//        /// <summary>
//        /// To retrieve the report status - whether in progress or ready for download
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo service object</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <param name="apiToken">API token</param>
//        /// <returns>API status</returns>
//        private int GetReportStatus(KenshooReportAPIV1Service kenshooReportService, string userName,
//                                                                        string password, string apiToken)
//        {
//            int apiStatus = 0;
//            try
//            {
//                do
//                {
//                    Thread.Sleep(ITERATIVEREPORTSTATUSCHECK_INSECONDS);
//                    //check the report generate status
//                    apiStatus = kenshooReportService.getReportStatus(userName, password, apiToken);
//                } while (apiStatus < 0);
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                      SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return apiStatus;
//        }

//        /// <summary>
//        /// To retrieve the report date in zip format
//        /// </summary>
//        /// <param name="kenshooReportService">Kenshoo reporting service object</param>
//        /// <param name="userName">User name</param>
//        /// <param name="password">Password</param>
//        /// <param name="apiToken">API token</param>
//        /// <returns>The report data in byte array</returns>
//        private byte[] GetZippedData(KenshooReportAPIV1Service kenshooReportService, string userName,
//                                                                        string password, string apiToken)
//        {
//            byte[] reportData = null;
//            try
//            {
//                //download the data
//                reportData = kenshooReportService.getZippedReport(userName, password, apiToken);
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return reportData;
//        }

//        /// <summary>
//        /// To File I/O the byte array report content
//        /// </summary>
//        /// <param name="filename">Name of the zip file</param>
//        /// <param name="byteArray">The report content in byte array</param>
//        /// <returns>True, if successful. Otherwise false</returns>
//        private bool WriteToZipFile(string filename, byte[] byteArray)
//        {
//            bool result = false;
//            FileStream fileStream = null;
//            try
//            {
//                fileStream = new System.IO.FileStream(filename, System.IO.FileMode.Create);
//                fileStream.Write(byteArray, 0, byteArray.Length);
//                fileStream.Close();
//                result = true;
//            }
//            catch (Exception ex)
//            {
//                if (fileStream != null)
//                {
//                    fileStream.Close();
//                }
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return result;
//        }

//        /// <summary>
//        /// To load the uncompressed xml report data to the dataset
//        /// </summary>
//        /// <param name="reportFileSubFolderPath">The xml file path</param>
//        /// <returns>The report xml file data loaded dataset</returns>
//        private DataSet LoadXmlDataFile(string reportFileSubFolderPath)
//        {
//            DataSet dsReports = null;
//            try
//            {
//                dsReports = new DataSet();
//                DataSet dsReport = null;

//                DirectoryInfo reportFolder = new DirectoryInfo(reportFileSubFolderPath);
//                foreach (FileInfo xmlReports in reportFolder.GetFiles(XMLFILE_EXTN))
//                {
//                    long lng = xmlReports.Length;

//                    dsReport = new DataSet();
//                    dsReport.ReadXml(xmlReports.FullName);
//                    dsReports.Merge(dsReport);
//                    xmlReports.Delete();
//                }
//            }
//            catch (Exception ex)
//            {
//                UtilityHelper.ActivityLog(UtilityHelper.KenshooAdsDSCode, 100, ex.Message, ex.StackTrace,
//                                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
//            }
//            return dsReports;
//        }
//    }
//}
