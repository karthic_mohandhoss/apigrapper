﻿/******************************************************************************************
Filename        : GoogleAnalytics.cs
Purpose         : To connect to google analytics API to grab the data.
                    
Created by      : Raja B
Created date    : 29-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
29-Mar-2013     Raja B                              Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Samples.Helper;
using Google.Apis.Util;

using Google.Apis.Samples.Helper.NativeAuthorizationFlows;

using System.Security.Authentication;
using System.Security.Cryptography;
using System.Configuration;
using System.Data;

using APIGrabberUtility;

namespace APIGrabbers
{
    public class GoogleAnalytics : IAPIGrabber
    {
        // The client identifier
        private string clientIdentifier = string.Empty;

        // The client secret
        private string clientSecret = string.Empty;

        // The storage - unique to the application
        private string storage = string.Empty;

        // The api key
        private string key = string.Empty;

        // The google analytics api grabber root folder - 
        // dot net end google analytics file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The google analytics api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        // The report file name. This is the partial file name. 
        // The datetime stamp will be concatenated with the final file name.
        private string reportFileName = string.Empty;

        // The google analytics api grabber - etl folder path.
        // The path to which the dfa for dart 7z file should be 
        // dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;

        // The google analytics report repeat frequency. Eg. Yesterday data (D), Past one week data (W),
        // Past one month data (M), Past one year data (Y)
        private string repeatFrequency = string.Empty;

        // Name for google analytics api. Used while activity logging
        private const string GOOGLE_ANALYTICS_ACTIVITY_NAME = "Google Analytics : ";

        // Error code (for general activities) for google analytics api. 
        // Used while activity logging
        private const int GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE = 300;

        // Error code (for error activities) for google analytisc api. 
        // Used while activity logging
        private const int GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE = -300;

        // The api scope
        private const string DEVSTORAGESCOPE_READONLY = "https://www.googleapis.com/auth/devstorage.read_only";

        // The api additional scope
        private readonly string googleAnalyticsScope = AnalyticsService.Scopes.Analytics.GetStringValue();

        // The activity event log name and the local ip
        private string activityLogNameIP = string.Empty;

        /// <summary>
        /// Entry point method for the Kenshoo Search run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool ExecuteJob()
        {
            bool jobSucceeded = false;
            try
            {
                // To get the activity event log name and the local ip
                activityLogNameIP = UtilityHelper.GetActivityLogNameIP(GOOGLE_ANALYTICS_ACTIVITY_NAME);

                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                    activityLogNameIP + "ExecuteJob() started..", string.Empty,
                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get the configuration information from the DB (tbl_DatasourceConfig)
                GetConfigInfo();

                // Get the accounts details from the DB (tbl_GAAccounts)
                DataSet dsAccounts = DataAccessUtility.GetGAAccounts();

                // Register the authenticator.
                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
                provider.ClientIdentifier = clientIdentifier;
                provider.ClientSecret = clientSecret;

                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);                
                var service = new AnalyticsService(auth);
                
                // Account Ids - 11401921   12305179    32102700    33685389
                // Get the accounts               
                Accounts accounts = service.Management.Accounts.List().Fetch();

                #region to get the refresh token through the api grabber ui (APIGrabberTester) - for one time activity

                bool onlyToGetRefreshToken = false;
                bool.TryParse(ConfigurationManager.AppSettings["googleanalytics_onlytogetrefreshtoken"], out onlyToGetRefreshToken);
                if (onlyToGetRefreshToken)
                {
                    UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob : The API Grabber UI application running in " +
                                    "onlyToGetRefreshToken mode. The googleanalytics_onlytogetrefreshtoken configuration flag in " +
                                    "the api grabber ui app.config file is set to true", string.Empty, SeverityLevel.INFORMATION,
                                    EmailNotification.NOT_NEEDED);

                    jobSucceeded = true;
                    return jobSucceeded;
                }

                #endregion

                if (accounts == null)
                {
                    throw new Exception(activityLogNameIP + "Execute Job : Error in accounts retrieval");
                }
                else if (accounts.Items.Count == 0)
                {
                    throw new Exception(activityLogNameIP + "Execute Job : No account found");
                }

                string accountName = string.Empty;
                string accountId = string.Empty;
                foreach (DataRow drAccount in dsAccounts.Tables[0].Rows)
                {
                    accountId = drAccount["Accountid"] != null ? drAccount["Accountid"].ToString() : string.Empty;
                    if (string.IsNullOrEmpty(accountId))
                    {
                        throw new Exception(activityLogNameIP + "Execute Job : Account id is empty.");
                    }

                    Account account = accounts.Items.Where(x => x.Id == accountId).FirstOrDefault();
                    if (account == null)
                    {
                        accountName = string.Empty;
                    }
                    else
                    {
                        accountName = account.Name;
                    }

                    string startDate = string.Empty;
                    string endDate = string.Empty;

                    if (repeatFrequency.ToUpper() == RepeatFrequency.DAILY) //daily
                    {
                        startDate = DateTime.Now.AddDays(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.WEEKLY) //weekly
                    {
                        startDate = DateTime.Now.AddDays(-7).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.MONTHLY) //monthly
                    {
                        startDate = DateTime.Now.AddMonths(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.YEARLY) //yearly
                    {
                        startDate = DateTime.Now.AddYears(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else
                    {
                        startDate = drAccount["Startdate"] != null ? drAccount["Startdate"].ToString() : DateTime.Now.ToString();
                        endDate = drAccount["Enddate"] != null ? drAccount["Enddate"].ToString() : DateTime.Now.ToString();
                    }

                    string dimension = drAccount["Dimension"] != null ? drAccount["Dimension"].ToString() : string.Empty;
                    string metrics = drAccount["Metrics"] != null ? drAccount["Metrics"].ToString() : string.Empty;
                    string outputFileName = drAccount["Outputfilename"] != null ? drAccount["Outputfilename"].ToString() : string.Empty;

                    // Get the webproperies for the account
                    Webproperties webProperties = service.Management.Webproperties.List(accountId).Fetch();

                    if (webProperties == null)
                    {
                        throw new Exception(activityLogNameIP + "Execute Job : Error in web properties retrieval " +
                                                    "for account id : " + accountId);
                    }
                    else if (webProperties.Items.Count == 0)
                    {
                        throw new Exception(activityLogNameIP + "Execute Job : No web properties found for account id : " +
                                                        accountId);
                    }

                    string webPropertyId = string.Empty;
                    foreach (Webproperty webProperty in webProperties.Items)
                    {
                        webPropertyId = webProperty.Id;
                        Profiles profiles = service.Management.Profiles.List(accountId, webPropertyId).Fetch();

                        if (profiles == null)
                        {
                            throw new Exception(activityLogNameIP + "Execute Job : Error in profiles retrieval " +
                                                        "for account id : " + accountId + " and web property id : " +
                                                        webPropertyId);
                        }
                        else if (profiles.Items.Count == 0)
                        {
                            throw new Exception(activityLogNameIP + "Execute Job : No profiles found for account id : " +
                                                            accountId + " and web property id : " + webPropertyId);
                        }

                        string profileId = string.Empty;
                        foreach (Profile profile in profiles.Items)
                        {
                            profileId = profile.Id;
                            bool dataRetrievalSucceeded = RetrieveReportData(service, accountId, accountName,
                                                                                profileId, profile.Name,
                                                                                startDate, endDate,
                                                                                dimension, metrics, outputFileName);
                            if (!dataRetrievalSucceeded)
                            {
                                throw new Exception(activityLogNameIP + "Execute Job : Error occurred during " +
                                                        "google analytics report generation");
                            }
                        }
                    }
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(UtilityHelper.GoogleAnalyticsDSCode.ToString());

                jobSucceeded = true;

                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "ExecuteJob() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)                
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(UtilityHelper.GoogleAnalyticsDSCode.ToString());
                }                  
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            try
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DatasourceConfig datasourceConfig = new DatasourceConfig();
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(UtilityHelper.GoogleAnalyticsDSCode);

                if (datasourceConfig != null)
                {
                    clientIdentifier = datasourceConfig.ClientIdentifier;
                    if (string.IsNullOrEmpty(clientIdentifier))
                    {
                        throw new Exception(activityLogNameIP +
                                            "GetConfigInfo : Fatal error - Client Identifier, config info not found in DB");
                    }
                }

                clientSecret = datasourceConfig.ClientSecret;
                if (string.IsNullOrEmpty(clientSecret))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Client Secret, config info not found in DB");
                }

                storage = datasourceConfig.Storage;
                if (string.IsNullOrEmpty(storage))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Storage, config info not found in DB");
                }

                key = datasourceConfig.ApiKey;
                if (string.IsNullOrEmpty(key))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Key, config info not found in DB");
                }

                rootFolderPath = datasourceConfig.RootFolderPath;
                if (string.IsNullOrEmpty(rootFolderPath))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                }
                rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);

                backupZipFolderName = datasourceConfig.BackupZipFolderName;
                if (string.IsNullOrEmpty(backupZipFolderName))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                }

                reportFileName = datasourceConfig.ReportFileName;
                if (string.IsNullOrEmpty(reportFileName))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");
                }
                
                etlFolderPath = datasourceConfig.EtlFolderPath;
                if (string.IsNullOrEmpty(etlFolderPath))
                {
                    throw new Exception(activityLogNameIP +
                                        "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                }
                etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);

                repeatFrequency = datasourceConfig.Repeat;

                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    "Google Analytics : GetConfigInfo() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message, ex.StackTrace,
                            SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        #region GetAuthorization - old implementation
        /// <summary>
        /// To get the authorization
        /// </summary>
        /// <param name="client">the native application client object</param>
        /// <returns>the object which implements iauthorization state - the authorization state object</returns>
        //private static IAuthorizationState GetAuthorization(NativeApplicationClient client)
        //{
        //    IAuthorizationState authorizationState = null;
        //    try
        //    {
        //        UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
        //                            activityLogNameIP + "GetAuthorization() started..", string.Empty,
        //                            SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

        //        // Check if there is a cached refresh token available.
        //        authorizationState = AuthorizationMgr.GetCachedRefreshToken(storage, key);

        //        if (authorizationState != null)
        //        {
        //            try
        //            {
        //                // Refresh the token
        //                client.RefreshToken(authorizationState);
        //                return authorizationState;
        //            }
        //            catch (DotNetOpenAuth.Messaging.ProtocolException ex)
        //            {
        //                throw new Exception("Google Analytics : Get Authorization : Using existing refresh token failed: " +
        //                                            ex.Message);
        //            }
        //        }

        //        // Retrieve the authorization from the user.                
        //        authorizationState = AuthorizationMgr.RequestNativeAuthorization(client, googleAnalyticsScope,
        //                                                                            DEVSTORAGESCOPE_READONLY);
        //        AuthorizationMgr.SetCachedRefreshToken(storage, key, authorizationState);

        //        UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
        //                        activityLogNameIP + "GetAuthorization() completed", string.Empty,
        //                        SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message,
        //                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

        //        throw ex;
        //    }
        //    return authorizationState;
        //}
        #endregion

        /// <summary>
        /// To get the authorization
        /// </summary>
        /// <param name="client">the native application client object</param>
        /// <returns>the object which implements iauthorization state - the authorization state object</returns>
        private IAuthorizationState GetAuthorization(NativeApplicationClient client)
        {
            IAuthorizationState authorizationState = null;
            try
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "GetAuthorization() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Check if there is a cached refresh token available.
                authorizationState = GetCachedRefreshToken(storage, key);

                if (authorizationState != null)
                {
                    try
                    {
                        // Refresh the token
                        client.RefreshToken(authorizationState);
                        return authorizationState;
                    }
                    catch (DotNetOpenAuth.Messaging.ProtocolException ex)
                    {
                        throw new Exception("Google Analytics : Get Authorization : Using existing refresh token failed: " +
                                                    ex.Message);
                    }
                }

                // Retrieve the authorization from the user.                
                authorizationState = AuthorizationMgr.RequestNativeAuthorization(client, googleAnalyticsScope,
                                                                                    DEVSTORAGESCOPE_READONLY);
                SetCachedRefreshToken(storage, key, authorizationState);

                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                activityLogNameIP + "GetAuthorization() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return authorizationState;
        }

        /// <summary>         
        /// Returns a cached refresh token for this application, or null if unavailable.         
        /// </summary>         
        /// <param name="storageName">The file name (without extension) used for storage.</param>         
        /// <param name="key">The key to decrypt the data with.</param>         
        /// <returns>The authorization state containing a Refresh Token, or null if unavailable</returns>         
        private AuthorizationState GetCachedRefreshToken(string storageName, string key)         
        {
            UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "GetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            string contents = DataAccessUtility.GetScopeRefreshToken(UtilityHelper.GoogleAnalyticsDSCode.ToString());
            if (string.IsNullOrEmpty(contents))             
            {                 
                return null; // No cached token available.             
            }                          
            string[] content = contents.Split(new[] { "\r\n" }, StringSplitOptions.None);                       
            // Create the authorization state.             
            string[] scopes = content[0].Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);             
            string refreshToken = content[1];

            UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "GetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            return new AuthorizationState(scopes) { RefreshToken = refreshToken };         
        }                  
        
        /// <summary>         
        /// Saves a refresh token to the specified storage name, and encrypts it using the specified key.         
        /// </summary>         
        private void SetCachedRefreshToken(string storageName, string key, IAuthorizationState state)         
        {
            UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "SetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            // Create the file content.             
            string scopes = state.Scope.Aggregate("", (left, append) => left + " " + append);             
            string content = scopes + "\r\n" + state.RefreshToken;

            DataAccessUtility.SetScopeRefreshToken(UtilityHelper.GoogleAnalyticsDSCode.ToString(), content);

            UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "SetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        }         
        
        #region getconfiginfo() - config from app.config - old implementation
        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        //private void GetConfigInfo()
        //{
        //    try
        //    {
        //        clientIdentifier = ConfigurationManager.AppSettings["googleanalytics_clientidentifier"];
        //        clientSecret = ConfigurationManager.AppSettings["googleanalytics_clientsecret"];
        //        //refreshToken = ConfigurationManager.AppSettings["googleanalytics_refreshtoken"];

        //        storage = ConfigurationManager.AppSettings["googleanalytics_storage"];
        //        key = ConfigurationManager.AppSettings["googleanalytics_key"];

        //        rootFolderPath = ConfigurationManager.AppSettings["googleanalytics_rootfolderpath"];
        //        backupZipFolderName = ConfigurationManager.AppSettings["googleanalytics_backupzipfoldername"];
        //        reportFileName = ConfigurationManager.AppSettings["googleanalytics_reportfilename"];
        //        etlFolderPath = ConfigurationManager.AppSettings["googleanalytics_etlfolderpath"];
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, 30GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message, ex.StackTrace,
        //                    SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
        //    }
        //}
        #endregion

        /// <summary>
        /// The retrieve the report data
        /// </summary>
        /// <param name="service">the google analytics api service</param>
        /// <param name="accountId">the account id</param>
        /// <param name="accountName">the account name</param>
        /// <param name="profileId">the user profile id</param>
        /// <param name="profileName">the profile name</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="dimension">the dimension</param>
        /// <param name="metrics">the metrics</param>
        /// <param name="outputFileName">the output file name</param>
        /// <returns>True, if successful. Otherwise false</returns>
        private bool RetrieveReportData(AnalyticsService service, string accountId, string accountName,
                                                    string profileId, string profileName,
                                                    string startDate, string endDate,
                                                    string dimension, string metrics, string outputFileName)
        {
            UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "RetrieveReportData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            bool reportRetrievalSucceeded = false;
            GADimensionsMetrics gaDimMetrics = null;
            try
            {
                // To retrieve the google analytics report data
                gaDimMetrics = new GADimensionsMetrics(GOOGLE_ANALYTICS_ACTIVITY_NAME,
                                                        GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                                        GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE);

                reportRetrievalSucceeded = gaDimMetrics.GetReportData(service, accountId, accountName,
                                                                    profileId, profileName, startDate, endDate,
                                                                    dimension, metrics, outputFileName,
                                                                    rootFolderPath, backupZipFolderName, etlFolderPath);

                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_GENERAL_ACTIVITY_CODE,
                                    activityLogNameIP + "RetrieveReportData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(UtilityHelper.GoogleAnalyticsDSCode, GOOGLE_ANALYTICS_ERROR_ACTIVITY_CODE, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (gaDimMetrics != null)
                {
                    gaDimMetrics = null;
                }
            }
            return reportRetrievalSucceeded;
        }
    }
}
