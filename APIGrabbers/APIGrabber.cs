﻿/******************************************************************************************
Filename        : APIGrabber.cs
Purpose         : Base class for all the api grabbers
                    
Created by      : Raja
Created date    : 23-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
23-May-2013     Raja                                Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using APIGrabberUtility;

namespace APIGrabbers
{
    public class APIGrabber
    {
        /// <summary>
        /// To retrieve the event log config from the app.config file
        /// </summary>
        protected void GetEventLogConfig()
        {
            try
            {
                // Flag, whether to enable the windows event - activity logging
                bool enableWindowsEventLogging = false;

                // Flag, whether to enable the data base - activity logging
                bool enableDatabaseLogging = false;

                // Try parsing the enable windows event logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enablewindowseventlogging"], out enableWindowsEventLogging);

                // Try parsing the enable database logging - application configuration setting to bool type
                bool.TryParse(ConfigurationManager.AppSettings["enabledatabaselogging"], out enableDatabaseLogging);

                // Set the enable windows event logging config to utility helper
                UtilityHelper.EnableWindowsEventLogging = enableWindowsEventLogging;
                // Set the enable database logging config to utility helper
                UtilityHelper.EnableDatabaseLogging = enableDatabaseLogging;
            }
            catch (Exception ex)
            { }
        }        
    }
}
