﻿/******************************************************************************************
Filename        : Vindico.cs
Purpose         : To connect to the Vindico API to grab the data.
                    
Created by      : Raja
Created date    : 08-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
08-May-2013     Raja                              Initial created
  
*******************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using System.Data;
using System.Configuration;

using APIGrabberUtility;
using Vindico.AdvancedReportService;

namespace APIGrabbers
{
    public class Vindico
    {
        #region Vindico - Generic variables for vindico all accounts

        // The api key
        private string apiKey = string.Empty;

        // The report guid
        private string reportGuid = string.Empty;

        // The vindico api grabber root folder - 
        // dot net end vindico file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The vindico api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        // The report file name. This is the partial file name. 
        // The datetime stamp will be concatenated with the final file name.
        private string reportFileName = string.Empty;

        // The vindico api grabber - etl folder path.
        // The path to which the vindico 7z and ctl file should be 
        // dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;

        // The WCF binding name - basic http
        private const string WCFSERVICE_BINDING = "BasicHttpBinding_IAdvancedReportService";

        // The initial starting index for the pagination of the data fetch
        private const int INITIAL_PAGENUMBER = 1;

        // The # of records should be fetched per request
        private int maxPageSize = 50000;

        #endregion

        #region Vindico - Specific to individual Vindico account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion
        
        /// <summary>
        /// The vindico api grabber constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourcedsActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public Vindico(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }
                
        /// <summary>
        /// Entry point method for the vindico api run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool InitiateDataGrabbing()
        {
            bool jobSucceeded = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() started..", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Read the configuration information from the database
                GetConfigInfo();
                
                //Retrieve the report data 
                jobSucceeded = RetrieveReportData();
                if (!jobSucceeded)
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "InitiateDataGrabbing : Error occurred during vindico report generation");
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(dsCode);

                jobSucceeded = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() completed", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, 
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(dsCode);
                }
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            Tbl_DatasourceConfig datasourceConfig = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Retrieve the configuration information for the database (tbl_datasourceconfig table)
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(dsCode);

                if (datasourceConfig != null)
                {                    
                    // Api key
                    apiKey = datasourceConfig.Apikey;
                    if (string.IsNullOrEmpty(apiKey))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Key, config info not found in DB");
                    }

                    // The root folder path for vindico
                    rootFolderPath = datasourceConfig.Rootfolderpath;
                    if (string.IsNullOrEmpty(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                    }
                    rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);

                    // The report file name (partial), the datetime stamp will be concatenated with this report file name
                    // to make the full file name.
                    reportFileName = datasourceConfig.Reportfilename;
                    if (string.IsNullOrEmpty(reportFileName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");

                    }                   

                    // The back up folder name. The back up of the csv file taken in 7z format
                    backupZipFolderName = datasourceConfig.Backupzipfoldername;
                    if (string.IsNullOrEmpty(backupZipFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                    }

                    // The etl folder path - the folder path to which the final report file (7z extension) and the control file
                    // should be file i/o.
                    etlFolderPath = datasourceConfig.Etlfolderpath;
                    if (string.IsNullOrEmpty(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                    }
                    etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);

                    // Report GUID
                    reportGuid = datasourceConfig.Dsconfig1;
                    if (string.IsNullOrEmpty(reportGuid))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Dsconfig1(report guid), config info not found in DB");
                    }
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Config info not found in DB");
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() completed.",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (datasourceConfig != null)
                {
                    datasourceConfig = null;
                }
            }
        }
        
        /// <summary>
        /// To retrieve the campaign report data using vindico advanced report service client
        /// </summary>        
        /// <returns>True, if successful. Otherwise false</returns>
        private bool RetrieveReportData()
        {
            bool result = false;            
                               
            AdvancedReportServiceClient vindicoReportServiceClient = null;                
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveReportData() started..", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // create a new object of advance report service client
                vindicoReportServiceClient = new AdvancedReportServiceClient(WCFSERVICE_BINDING);
                vindicoReportServiceClient.Open();

                // Retrieve the report data by calling the GetData() method
                // The initial starting index for the pagination is 1 and the initial existing
                // csv filename is empty
                result = GetData(vindicoReportServiceClient, INITIAL_PAGENUMBER, string.Empty);               
                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveReportData() completed.", string.Empty, 
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (vindicoReportServiceClient != null)
                {
                    vindicoReportServiceClient.Close();
                    vindicoReportServiceClient = null;
                }
            }
            return result;
        }

        /// <summary>
        /// To retrieve the advance report data by calling the vindico advanced report service client
        /// </summary>
        /// <param name="vindicoReportServiceClient">the vindico report service client object</param>
        /// <param name="startIndex">the starting index for the pagination. The initial value is 1</param>
        /// <param name="existingFileName">the csv file name, if one already exist and the data 
        /// retrieved should be appended to it. 
        /// So, initially the filename will be empty. During the pagination loop, in the first loop 
        /// the csv file should be created and the file name should be returned for further pagination
        /// data append to the same file during rest of the looping</param>
        /// <returns>True, if the data is successfully retrieved. Otherwise false</returns>
        private bool GetData(AdvancedReportServiceClient vindicoReportServiceClient, int pageNumber, string existingFileName)
        {
            bool dataRetrieved = false;

            // To store the total record count found in the vindico data source
            int totalPageCount = 0;

            // The existing csv file, to which the further pagination looping data should be appended
            string existingFileNameValue = string.Empty;

            // The advanced report data row collection
            AdvancedReportRowDataContract[] campaignReportDetails = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // If suppose vindico report service client is null, initialize it
                if (vindicoReportServiceClient == null)
                {
                    vindicoReportServiceClient = new AdvancedReportServiceClient(WCFSERVICE_BINDING);
                    vindicoReportServiceClient.Open();
                }

                // Retrieve the advanced report data as page-by-page of 5000 records per page call. The values 5000 is 
                // configurable and it is as configured in the variable maxPageSize.
                AdvancedReportResponse advancedReportResponse = vindicoReportServiceClient.GetPagedAdvancedReport(
                                                                            reportGuid, apiKey, pageNumber, maxPageSize);
                
                // Get the total results ( the total # of records found for the call )
                totalPageCount = advancedReportResponse.PageInfo.TotalPageCount;

                // Get the advanced report (campaign report) data 
                campaignReportDetails = advancedReportResponse.AdvancedReport;
                              
                // For pagination.
                if (totalPageCount > pageNumber)
                {
                    // Method to write first 5,000 records to the csv file
                    existingFileNameValue = GenerateCSVFile(campaignReportDetails, pageNumber, existingFileName, totalPageCount, false);

                    // Set the start index for the pagination
                    pageNumber += 1;

                    // Get the data
                    GetData(vindicoReportServiceClient, pageNumber, existingFileNameValue);                    
                }
                else
                {
                    // The file name is returned - since the records are appended for each 5,000 records
                    existingFileNameValue = GenerateCSVFile(campaignReportDetails, pageNumber, existingFileName, totalPageCount, true);
                }
                dataRetrieved = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dataRetrieved;     
        }

        /// <summary>
        /// To generate CSV file based on vindico campaign data
        /// </summary>
        /// <param name="campaignReportDetails">campaign report details - collection of advanced
        /// report data rows</param>
        /// <param name="startIndex">the starting index for report data retrieval</param>
        /// <param name="existingFileName">the existing file to which the report data should be appended</param>
        /// <param name="lastRecordsSet">whether it is the last set of records to be grabbed</param>
        /// <returns>the newly created (first time) / existing file name</returns>
        private string GenerateCSVFile( AdvancedReportRowDataContract[] campaignReportDetails, int startIndex, 
                                                string existingFileName, long recordCount, bool lastRecordsSet)
        {
            // the filename should be returned
            string fileName = string.Empty;

            // the dataset to hold the vindico compaign report data
            DataSet dsVindicoCampaignReport = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GenerateCSVFile() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Verify whether the root folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(rootFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP + "GenerateCSVFile : Vindico root folder does not exist." +
                                                    "The root folder path - " + rootFolderPath);
                }

                // Verify whether the backup folder exist. If not, try to create it.
                string backupZipFileSubPath = rootFolderPath + backupZipFolderName;
                if (!UtilityHelper.VerifyFolderExists(backupZipFileSubPath))
                {
                    throw new Exception(dsActivityLogNameIP + "GenerateCSVFile : Vindico backup folder path does not exist." +
                                                    "The vindico backup folder path - " + backupZipFileSubPath);
                }

                // Verify whether the etl folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(etlFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP + "GenerateCSVFile : Vindico etl folder path does not exist." +
                                                    "The vindico etl folder path - " + etlFolderPath);
                }

                // Get the Vindico report data table schema created
                DataTable dtVindicoCampaignReportData = CreateVindicoReportTableSchema();

                if (campaignReportDetails != null)
                {
                    dsVindicoCampaignReport = new DataSet();

                    // Loop through the campaign report data row-by-row and add the records to the data table
                    foreach (AdvancedReportRowDataContract campaignDetail in campaignReportDetails)
                    {
                        dtVindicoCampaignReportData.Rows.Add(
                                    campaignDetail.Campaign, campaignDetail.Site, campaignDetail.Rotation,
                                    campaignDetail.StartDate, campaignDetail.EndDate, campaignDetail.DayOfWeek,
                                    campaignDetail.DayOfYear, campaignDetail.WeekOfYear, campaignDetail.AdLength,
                                    campaignDetail.PlacementType, campaignDetail.PlacementName, campaignDetail.AdModule,
                                    campaignDetail.VideoImpressionGoal, campaignDetail.BannerImpressionGoal,
                                    campaignDetail.RotationId, campaignDetail.Cpm, campaignDetail.CompanionCpm,
                                    campaignDetail.CampaignId, campaignDetail.PlacementId, campaignDetail.AdModuleId,
                                    campaignDetail.Month, campaignDetail.Day, campaignDetail.Year,
                                    campaignDetail.CreativeName, campaignDetail.ImpressionDate, campaignDetail.Metrics,
                                    campaignDetail.Total, campaignDetail.Unique, campaignDetail.OutletTypeCode,
                                    campaignDetail.ServiceFee);
                    }

                    // Add the data table to a dataset
                    dsVindicoCampaignReport.Tables.Add(dtVindicoCampaignReportData);
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP + "GenerateCSVFile : Campaign data not found");
                }

                // Write the CSV file and return the filename
                fileName = UtilityHelper.WriteCSV(dsVindicoCampaignReport, rootFolderPath, reportFileName,
                                                    backupZipFileSubPath, startIndex, existingFileName, 
                                                    etlFolderPath, lastRecordsSet);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GenerateCSVFile() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return fileName;
        }

        /// <summary>
        /// To create the vindico report table - schema only
        /// </summary>
        /// <returns>the vindico report table structure</returns>
        private DataTable CreateVindicoReportTableSchema()
        {
            DataTable dtVindicoReport = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "CreateVindicoReportTableSchema() started..", 
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Have the possible types
                Type stringType = typeof(string);
                Type decimalType = typeof(decimal);
                Type longType = typeof(long);
                Type intType = typeof(int);

                // create a new table instance
                dtVindicoReport = new DataTable();

                // Add columns to the data table
                dtVindicoReport.Columns.Add("Campaign", stringType);
                dtVindicoReport.Columns.Add("Site", stringType);
                dtVindicoReport.Columns.Add("Rotation", stringType);
                dtVindicoReport.Columns.Add("StartDate", stringType);               //datetime
                dtVindicoReport.Columns.Add("EndDate", stringType);                 //datetime
                dtVindicoReport.Columns.Add("DayOfWeek", stringType);
                dtVindicoReport.Columns.Add("DayOfYear", stringType);
                dtVindicoReport.Columns.Add("WeekOfYear", stringType);
                dtVindicoReport.Columns.Add("AdLength", decimalType);               //decimal
                dtVindicoReport.Columns.Add("PlacementType", stringType);
                dtVindicoReport.Columns.Add("PlacementName", stringType);
                dtVindicoReport.Columns.Add("AdModule", stringType);
                dtVindicoReport.Columns.Add("VideoImpressionGoal", longType);       //long
                dtVindicoReport.Columns.Add("BannerImpressionGoal", longType);      //long
                dtVindicoReport.Columns.Add("RotationId", intType);                 //int
                dtVindicoReport.Columns.Add("Cpm", decimalType);                    //decimal
                dtVindicoReport.Columns.Add("CompanionCpm", decimalType);           //decimal
                dtVindicoReport.Columns.Add("CampaignId", intType);                 //int
                dtVindicoReport.Columns.Add("PlacementId", intType);                //int
                dtVindicoReport.Columns.Add("AdModuleId", intType);                 //int
                dtVindicoReport.Columns.Add("Month", typeof(byte));                 //byte
                dtVindicoReport.Columns.Add("Day", intType);                        //int
                dtVindicoReport.Columns.Add("Year", intType);                       //int
                dtVindicoReport.Columns.Add("CreativeName", stringType);
                dtVindicoReport.Columns.Add("ImpressionDate", stringType);          //datetime
                dtVindicoReport.Columns.Add("Metrics", stringType);
                dtVindicoReport.Columns.Add("Total", longType);                     //long
                dtVindicoReport.Columns.Add("Unique", decimalType);                 //decimal
                dtVindicoReport.Columns.Add("OutletTypeCode", stringType);
                dtVindicoReport.Columns.Add("ServiceFee", decimalType);             //decimal

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "CreateVindicoReportTableSchema() completed.", 
                                   string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                   ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            // return the data table schema
            return dtVindicoReport;
        }     
    }
}

