﻿/******************************************************************************************
Filename        : FacebookInsights.cs
Purpose         : To connect to the Facebook Insights API to grab the data.
                    
Created by      : Raja B
Created date    : 09-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
09-Mar-2013     Raja B                              Initial created
30-Apr-2013     Karthic M                           Modified to get 5 types of report.  
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Data;

using Newtonsoft.Json;
using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class FacebookInsights
    {
        #region Facebook Insights - Generic variables for FBI all accounts

        // The facebook insights app id (refers to the tbl_datasourceconfig > clientIdentifier column value
        private string appId = string.Empty;

        // The facebook insights app secret (refers to the tbl_datasourceconfig > clientSecret column value
        private string appSecret = string.Empty;

        // The facebook insights api key
        private string key = string.Empty;

        // The facebook insights api grabber root folder - 
        // dot net end facebook insights file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The facebook insights api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        private string reportFileName = string.Empty;

        // The facebook insights api grabber - etl folder path.
        // The path to which the facebook insights 7z file 
        // should be dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;

        private int lengthSTrim = 0;
        private int lengthLTrim = 0;

        // The unix start date
        private double unixStartDate = 0;

        // The unix end date
        private double unixEndDate = 0;

        private const int PERIOD_DAILY_VALUE = 86400;
        private const int PERIOD_WEEKLY_VALUE = 604800;
        private const int PERIOD_MONTHLY_VALUE = 2592000;
        private const int PERIOD_LIFETIME_VALUE = 0;
        private const int PERIOD_28DAYS_VALUE = 2419200;

        private const string PERIOD_DAILY = "Daily";
        private const string PERIOD_WEEKLY = "Weekly";
        private const string PERIOD_MONTHLY = "Monthly";
        private const string PERIOD_LIFETIME = "LifeTime";
        private const string PERIOD_28DAYS = "28Days";

        #endregion

        #region Facebook Insights - Specific to individual FBI account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        #region scope

        private string strRptDailyFileName = ConfigurationManager.AppSettings["DayPeriodFileName"];
        private string strRptWeeklyFileName = ConfigurationManager.AppSettings["WeeklyPeriodFileName"];
        private string strRptMonthlyFileName = ConfigurationManager.AppSettings["MontlyPeriodFileName"];
        private string strRptLifeTimeFileName = ConfigurationManager.AppSettings["lifeTimePeriodFileName"];
        private string strRpt28FileName = ConfigurationManager.AppSettings["28PeriodFileName"];

        //<add key="FBScope1" value="friends_about_me,friends_actions.music,friends_actions.news,friends_actions.video,friends_activities,friends_birthday,friends_education_history,friends_events,friends_games_activity,friends_groups,"/>
        //<add key ="FBScope2" value="friends_hometown,friends_interests,friends_likes,friends_location,friends_notes,friends_photos,friends_questions,friends_relationship_details,friends_relationships,friends_religion_politics,friends_status,"/>
        //<add key="FBScope3" value="friends_subscriptions,friends_videos,friends_website,friends_work_history,email,publish_actions,user_about_me,user_actions.music,user_actions.news,user_actions.video,user_activities,user_birthday,"/>
        //<add key="FBScope4" value ="user_education_history,user_events,user_games_activity,user_groups,user_hometown,user_interests,user_likes,user_location,user_notes,user_photos,user_questions,user_relationship_details,user_relationships,"/>
        //<add key="FBScope5" value="user_religion_politics,user_status,user_subscriptions,user_videos,user_website,user_work_history,ads_management,create_event,create_note,export_stream,friends_online_presence,manage_friendlists,"/>
        //<add key ="FBScope6" value="manage_notifications,manage_pages,offline_access,photo_upload,publish_stream,read_friendlists,read_insights,read_mailbox,read_page_mailboxes,read_requests,read_stream,rsvp_event,share_item,sms,"/>
        //<add key="FBScope7" value ="status_update,user_online_presence,video_upload,xmpp_login"/>

        #endregion

        /// <summary>
        /// The facebook insights api grabber constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourceActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public FacebookInsights(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }

        /// <summary>
        /// Entry point method for the Facebook Insights run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool InitiateDataGrabbing()
        {
            bool jobSucceeded = false;

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Read the configuration information from the database
                GetConfigInfo();

                // Get the access token
                string accountAccessToken = GetAccessToken();

                // Get the account detail
                DataSet dsAccount = GetAccountDetails(accountAccessToken);

                string accountId = string.Empty;
                string accountName = string.Empty;
                if (dsAccount != null && dsAccount.Tables.Count > 0 && dsAccount.Tables[0].Rows.Count > 0)
                {
                    // Get the account id
                    accountId = (dsAccount.Tables[0].Rows[0]["id"] == null) ? string.Empty :
                                                            dsAccount.Tables[0].Rows[0]["id"].ToString();

                    // Modified by Karthic as the account Id is 15 digit So truncating the first five numbers and sending 
                    // last digit number ex now truncating 10000
                    if (lengthSTrim != 0)
                    {
                        int Tlength = accountId.Length;
                        accountId = accountId.Substring(lengthSTrim, Tlength - lengthSTrim);
                    }

                    //Modified by Karthic as the account Id is 15 digit So truncating the first five numbers and sending 
                    //last digit number ex now truncating 10000
                    if (lengthLTrim != 0)
                    {
                        int Tlength = accountId.Length;
                        accountId = accountId.Substring(0, Tlength - lengthLTrim);
                    }

                    //Get the account name
                    accountName = (dsAccount.Tables[0].Rows[0]["name"] == null) ? string.Empty :
                                                            dsAccount.Tables[0].Rows[0]["name"].ToString();
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : No account data found.");
                }

                // Get the page details
                DataSet dsPages = GetPages(accountAccessToken);

                if (dsPages == null || dsPages.Tables.Count == 0 || dsPages.Tables[0].Rows.Count == 0)
                {
                    throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : No pages data found.");
                }

                //Modified by Karthic to create seperate extract
                Dictionary<int, string> fbiPeriod = new Dictionary<int, string>();
                //a.  Extract1 – Daily extract period=86400 
                fbiPeriod.Add(PERIOD_DAILY_VALUE, PERIOD_DAILY);
                //b.  Extract2 – Weekly extract period=604800
                fbiPeriod.Add(PERIOD_WEEKLY_VALUE, PERIOD_WEEKLY);
                //c.  Extract3 – Monthly extract period=2592000
                fbiPeriod.Add(PERIOD_MONTHLY_VALUE, PERIOD_MONTHLY);
                //d.  Extract4 – Lifetime extract period=0
                fbiPeriod.Add(PERIOD_LIFETIME_VALUE, PERIOD_LIFETIME);
                //e.  Extract5 – 28 days  extract period=2419200
                fbiPeriod.Add(PERIOD_28DAYS_VALUE, PERIOD_28DAYS);

                DataSet dsReportDetails = null;
                foreach (var fbiPeriodPair in fbiPeriod)
                {
                    string fbiPeriodValue = fbiPeriodPair.Value;

                    UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "Started getting excel value: " + fbiPeriodValue,
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                    dsReportDetails = new DataSet();
                    foreach (DataRow drPage in dsPages.Tables[0].Rows)
                    {
                        string pageId = (drPage["id"] == null) ? string.Empty : drPage["id"].ToString();
                        string pageName = (drPage["name"] == null) ? string.Empty : drPage["name"].ToString();
                        string pageAccessToken = (drPage["access_token"] == null) ? string.Empty : drPage["access_token"].ToString();

                        DataSet dsAccountPages = RetrieveInsightsData(accountId, pageId, pageAccessToken, fbiPeriodPair.Key);

                        dsReportDetails.Merge(FormatToReport(dsAccountPages, accountId, accountName, pageName));
                    }

                    #region Create empty report with columns for FBI single column value issue.

                    DataSet dsReport = new DataSet();
                    DataTable dtReportField = new DataTable();
                    List<string> columns = new List<string>();

                    columns = ReportHeader(fbiPeriodPair.Value);
                    foreach (var column in columns)
                    {
                        if (!dtReportField.Columns.Contains(column.Trim()))
                            dtReportField.Columns.Add(column.Trim());
                    }

                    string periodCol = FBIPeriod.DAILY;
                    if (fbiPeriodPair.Value.Equals(PERIOD_WEEKLY))
                        periodCol = FBIPeriod.WEEKLY;
                    else if (fbiPeriodPair.Value.Equals(PERIOD_MONTHLY))
                        periodCol = FBIPeriod.MONTHLY;
                    else if (fbiPeriodPair.Value.Equals(PERIOD_LIFETIME))
                        periodCol = FBIPeriod.LIFETIME;
                    else if (fbiPeriodPair.Value.Equals(PERIOD_28DAYS))
                        periodCol = FBIPeriod.TWENTY_EIGHT_DAYS;

                    dtReportField.Columns[0].DefaultValue = periodCol;

                    if (dsReportDetails.Tables.Count > 0)
                        dtReportField.Merge(dsReportDetails.Tables["Report"]);

                    #region Remove empty row
                    //if (dtReportField.Rows.Count == 0)
                    //{
                    //    DataRow drReportField = dtReportField.NewRow();

                    //    foreach (DataColumn dcReportField in dtReportField.Columns)
                    //    {
                    //        drReportField[dcReportField] = " ";
                    //    }
                    //    dtReportField.Rows.Add(drReportField);
                    //}
                    #endregion

                    dsReport.Tables.Add(dtReportField);

                    # endregion

                    // Verify whether the root folder exist. If not, try to create it.
                    if (!UtilityHelper.VerifyFolderExists(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                                "InitiateDataGrabbing : Facebook insights root folder path not found or not accessible." +
                                                "The root folder path is " + rootFolderPath);
                    }

                    // Verify whether the backup folder exist. If not, try to create it.
                    string backupZipFileSubPath = rootFolderPath + backupZipFolderName;
                    if (!UtilityHelper.VerifyFolderExists(backupZipFileSubPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                                "InitiateDataGrabbing : Facebook insights backup folder path not found or not accessible. " +
                                                "The backup folder path is " + backupZipFileSubPath);
                    }

                    reportFileName = strRptDailyFileName;
                    if (fbiPeriodValue == PERIOD_WEEKLY)
                        reportFileName = strRptWeeklyFileName;
                    else if (fbiPeriodValue == PERIOD_28DAYS)
                        reportFileName = strRpt28FileName;
                    else if (fbiPeriodValue == PERIOD_MONTHLY)
                        reportFileName = strRptMonthlyFileName;
                    else if (fbiPeriodValue == PERIOD_LIFETIME)
                        reportFileName = strRptLifeTimeFileName;

                    UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "CSV file i/o started for the period: " + fbiPeriodValue,
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                    // Verify whether the etl folder exist. If not, try to create it.
                    if (!UtilityHelper.VerifyFolderExists(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                                "InitiateDataGrabbing : Facebook insights etl folder path not found or not accessible. " +
                                                "The etl folder path is " + etlFolderPath);
                    }

                    // Modified by Sankar for FBI single column value data issue
                    UtilityHelper.WriteCSV(dsReport, rootFolderPath, reportFileName,
                                                            backupZipFileSubPath, etlFolderPath);

                    UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "Successfully created the data file for the period: " +
                                    fbiPeriodValue, string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(dsCode);

                jobSucceeded = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(dsCode);
                }
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            Tbl_DatasourceConfig datasourceConfig = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Retrieve the configuration information for the database (tbl_datasourceconfig table)
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(dsCode);

                if (datasourceConfig != null)
                {
                    // App id (clientIdentifier db column value)
                    appId = datasourceConfig.Clientidentifier;
                    if (string.IsNullOrEmpty(appId))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Client Identifier (facebook app id), config info not found in DB");
                    }

                    // App secret (clientSecret db column value)
                    appSecret = datasourceConfig.Clientsecret;
                    if (string.IsNullOrEmpty(appSecret))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Client Secret (facebook app secret), config info not found in DB");
                    }

                    // Api key
                    key = datasourceConfig.Apikey;
                    if (string.IsNullOrEmpty(key))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Key, config info not found in DB");
                    }

                    // The root folder path for facebook insights
                    rootFolderPath = datasourceConfig.Rootfolderpath;
                    if (string.IsNullOrEmpty(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                    }
                    rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);

                    // The report file name (partial), the datetime stamp will be concatenated with this report file name
                    // to make the full file name.
                    reportFileName = datasourceConfig.Reportfilename;
                    if (string.IsNullOrEmpty(reportFileName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");
                    }

                    // The back up folder name. The back up of the csv file taken in 7z format
                    backupZipFolderName = datasourceConfig.Backupzipfoldername;
                    if (string.IsNullOrEmpty(backupZipFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                    }

                    // The etl folder path - the folder path to which the final report file (7z extension) and the control file
                    // should be file i/o.
                    etlFolderPath = datasourceConfig.Etlfolderpath;
                    if (string.IsNullOrEmpty(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                    "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                    }
                    etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);

                    // The facebook insights report repeat frequency. Eg. Yesterday data (D), Past one week data (W),
                    // Past one month data (M), Past one year data (Y)
                    string repeatFrequency = datasourceConfig.Repeat;

                    // To set the unix start and end date
                    SetUnixStartAndEndDate(repeatFrequency, datasourceConfig.Startdate, datasourceConfig.Enddate);

                    //Dsconfig2: Starting Index to trim
                    if (datasourceConfig.Dsconfig2 != null)
                    {
                        int.TryParse(datasourceConfig.Dsconfig2, out lengthSTrim);
                    }

                    //Dsconfig3: Last Index to trim
                    if (datasourceConfig.Dsconfig3 != null)
                    {
                        int.TryParse(datasourceConfig.Dsconfig3, out lengthLTrim);
                    }
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Config info not found in DB");
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (datasourceConfig != null)
                {
                    datasourceConfig = null;
                }
            }
        }

        /// <summary>
        /// To set the unix start and end date
        /// </summary>
        /// <param name="repeatFrequency">the repeat frequency. The D, W, M, Y</param>
        /// <param name="dsConfigStartDate">the start date as configured in the data source config db</param>
        /// <param name="dsConfigEndDate">the end date as configured in the data source config db</param>
        private void SetUnixStartAndEndDate(string repeatFrequency, DateTime dsConfigStartDate, DateTime dsConfigEndDate)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetUnixStartAndEndDate() started...", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DateTime startDate = DateTime.Now;
                DateTime estTime = new DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 00);

                DateTime newDateTime = DateTime.Now;
                if (repeatFrequency.ToUpper() == RepeatFrequency.DAILY) //daily
                {
                    newDateTime = estTime.AddDays(1);
                }
                else if (repeatFrequency.ToUpper() == RepeatFrequency.WEEKLY) //weekly
                {
                    newDateTime = estTime.AddDays(-7);
                }
                else if (repeatFrequency.ToUpper() == RepeatFrequency.MONTHLY) //monthly
                {
                    newDateTime = estTime.AddMonths(-1);
                }
                else if (repeatFrequency.ToUpper() == RepeatFrequency.YEARLY) //yearly
                {
                    newDateTime = estTime.AddYears(-1);
                }
                else
                {
                    repeatFrequency = string.Empty;
                }

                if (!string.IsNullOrEmpty(repeatFrequency))
                {
                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
                    DateTime pstTime = TimeZoneInfo.ConvertTime(estTime, timeZoneInfo);
                    // Convert and set the unix end date
                    unixEndDate = Facebook.DateTimeConvertor.ToUnixTime(pstTime);

                    estTime = new DateTime(newDateTime.Year, newDateTime.Month, newDateTime.Day, 23, 59, 00);
                    // Convert and set the unix start date
                    unixStartDate = Facebook.DateTimeConvertor.ToUnixTime(estTime);
                }
                else
                {
                    if (dsConfigStartDate == null)
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "SetUnixStartAndEndDate : Fatal error - Start date not found in config ");
                    }
                    else
                    {

                        estTime = dsConfigStartDate.AddDays(1);
                        // Convert and set the unix start date
                        unixStartDate = Facebook.DateTimeConvertor.ToUnixTime(estTime);
                    }

                    if (dsConfigEndDate == null)
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "SetUnixStartAndEndDate : Fatal error - End date not found in config ");
                    }
                    else
                    {
                        estTime = dsConfigEndDate.AddDays(1); ;
                        // Convert and set the unix end date
                        unixEndDate = Facebook.DateTimeConvertor.ToUnixTime(estTime);
                    }
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetUnixStartAndEndDate() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// Get the access token
        /// </summary>
        /// <returns>the access token</returns>
        private string GetAccessToken()
        {
            string accountAccessToken = string.Empty;

            Facebook.FacebookClient fbClient = null;
            try
            {
                // AAAGLQtpNG3cBAPG1rRxBPEvGWUYYbCTw1ga2jEG7rZAj1Cq3cZBsxhRF1mpdFEvGxSem3AlrpPRWc41luSW01DWFiMOdpiolhkKkkCMAZDZD

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAccessToken() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                fbClient = new Facebook.FacebookClient();
                // Assign the app id
                fbClient.AppId = appId;
                // Assign the app secret
                fbClient.AppSecret = appSecret;

                // Get the access token info
                dynamic tokenInfo = fbClient.Get(String.Format("/oauth/access_token?grant_type=fb_exchange_token&client_id={0}&client_secret={1}&fb_exchange_token={2}",
                                   fbClient.AppId, fbClient.AppSecret, key));

                // Get the account access token
                accountAccessToken = tokenInfo.access_token;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAccessToken() completed.", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                        ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (fbClient != null)
                {
                    fbClient = null;
                }
            }
            return accountAccessToken;
        }

        /// <summary>
        /// Get the account details using the accountAccessToken
        /// </summary>
        /// <param name="accountAccessToken">account access token</param>
        /// <returns>the account details dataset</returns>
        private DataSet GetAccountDetails(string accountAccessToken)
        {
            DataSet dsAccount = new DataSet();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAccountDetails() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // The accounts url
                //string accountsUrl = "https://graph.facebook.com/100004064120157?fields=id,name&access_token=" + accountAccessToken;
                string accountsUrl = "https://graph.facebook.com/me?fields=id,name&access_token=" + accountAccessToken;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(accountsUrl);
                // Get the accounts detail
                var response = request.GetResponse();
                string jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                XNode xNodeResponse = JsonConvert.DeserializeXNode(jsonResponse, "Root");
                StringReader reader = new StringReader(xNodeResponse.ToString());
                // Read the accounts detail xml to the data set
                dsAccount.ReadXml(reader);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAccountDetails() completed.", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsAccount;
        }

        /// <summary>
        ///  Get the dataset from the face book by using the account Access token
        /// </summary>
        /// <param name="accountAccessToken">Account Access token</param>
        /// <returns>the pages dataset</returns>
        private DataSet GetPages(string accountAccessToken)
        {
            DataSet dsPages = new DataSet();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetPages() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // The page url
                string pageUrl = "https://graph.facebook.com/me/accounts?access_token=" + accountAccessToken;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pageUrl);
                // Get page details
                var response = request.GetResponse();
                string strResponse = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                XNode xnode = JsonConvert.DeserializeXNode(strResponse, "Root");
                StringReader reader = new StringReader(xnode.ToString());
                // Read the page details xml to the dataset
                dsPages.ReadXml(reader);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetPages() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsPages;
        }

        /// <summary>
        /// To retrieve the facebook insights data
        /// </summary>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">the page access token</param>
        /// <param name="period">Format such as Day,Weekly,Monthly or Lifetime</param>
        /// <returns>the page dataset</returns>
        private DataSet RetrieveInsightsData(string accountId, string pageId, string pageAccessToken, int period)
        {
            DataSet dsPage = new DataSet();

            List<string> pageUrls = null;
            FacebookInsightsMetrics fbiMetrics = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveInsightsData() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string periodQueryString = "&period=" + period.ToString();
                pageUrls = new List<string>();

                // Initiate the fbi object and variables
                fbiMetrics = new FacebookInsightsMetrics(dsCode, dsActivityLogNameIP,
                                                                            dsGeneralActivityCode, dsErrorActivityCode);

                // if the period is not a lifetime 
                if (period != PERIOD_LIFETIME_VALUE)
                {
                    pageUrls = fbiMetrics.GetPageUrlsList(pageUrls, accountId, pageId, pageAccessToken,
                                                                unixStartDate, unixEndDate, periodQueryString);
                }
                else // the period is lifetime
                {
                    // Append the page user urls
                    pageUrls = fbiMetrics.GetPageUserUrlsList(pageUrls, accountId, pageId, pageAccessToken,
                                                                    unixStartDate, unixEndDate, periodQueryString);
                }
                // Retrieve the page data for all the page urls in the list
                dsPage = RetrievePageData(pageUrls);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveInsightsData() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (fbiMetrics != null)
                {
                    fbiMetrics = null;
                }

                if (pageUrls != null)
                {
                    pageUrls = null;
                }
            }
            return dsPage;
        }

        /// <summary>
        /// Retrive the page data from the URLs
        /// </summary>
        /// <param name="pageUrls">the list of page urls</param>
        /// <returns>the page details dataset</returns>
        private DataSet RetrievePageData(List<string> pageUrls)
        {
            DataSet dsPageDetails = new DataSet();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrievePageData() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                foreach (string url in pageUrls)
                {
                    UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "RetrievePageData : the page url is " + url, string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    // Get the page data
                    var response = request.GetResponse();
                    try
                    {
                        string data = new StreamReader(response.GetResponseStream()).ReadToEnd();
                        if (!data.Contains("data\":[]"))
                        {
                            XDocument xnode = JsonConvert.DeserializeXNode(data, "Root");
                            StringReader reader = new StringReader(xnode.ToString());
                            // Read the page data xml to the dataset
                            dsPageDetails.ReadXml(reader);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrievePageData() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsPageDetails;
        }

        /// <summary>
        /// To format the json / dataset received data to the report format
        /// </summary>
        /// <param name="dsAccountPages">the account pages dataset</param>
        /// <param name="accountId">the account id</param>
        /// <param name="accountName">the account name</param>
        /// <param name="pageName">the page name</param>
        /// <returns>the formated report dataset</returns>
        private DataSet FormatToReport(DataSet dsAccountPages, string accountId, string accountName, string pageName)
        {
            DataSet dsReports = new DataSet();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "FormatToReport() started...", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataRelation drelDataValue = dsAccountPages.Relations["data_values"];

                // Create a new table with the columns
                DataTable dtReports = new DataTable();
                dtReports.Columns.Add("ColumnName");
                dtReports.Columns.Add("Date");
                dtReports.Columns.Add("AccountId");
                dtReports.Columns.Add("Account");
                dtReports.Columns.Add("PageId");
                dtReports.Columns.Add("Page");
                dtReports.Columns.Add("Value");

                if (dsAccountPages.Tables["Data"] != null)
                {
                    foreach (DataRow drData in dsAccountPages.Tables["data"].Rows)
                    {
                        foreach (DataRow drValues in drData.GetChildRows(drelDataValue))
                        {
                            DataRow drNewReport = dtReports.NewRow();

                            drNewReport["ColumnName"] = drData["name"].ToString();// +"_" + drData["period"].ToString(); // parent
                            //Fixing datetime issue -1 2013-04-18T07:00:00+0000
                            string dateVal = drValues["end_time"].ToString();
                            int year = Convert.ToInt32(dateVal.Substring(0, 4));
                            int month = Convert.ToInt32(dateVal.Substring(5, 2));
                            int day = Convert.ToInt32(dateVal.Substring(8, 2));
                            DateTime curTime = new DateTime(year, month, day);

                            curTime = curTime.AddDays(-1);

                            string monthStr = curTime.Month.ToString();
                            if (monthStr.Length == 1) monthStr = "0" + monthStr;
                            string yearStr = curTime.Day.ToString();
                            if (yearStr.Length == 1) yearStr = "0" + yearStr;

                            string fday = curTime.Year.ToString() + "-" + monthStr + "-" + yearStr;
                            //Modified by Karthic
                            //drNewReport["Date"] = drValues["end_time"]; // child

                            drNewReport["Date"] = fday;
                            drNewReport["AccountId"] = accountId; // account id
                            drNewReport["Account"] = accountName; // account name
                            drNewReport["PageId"] = drData["id"].ToString().Split('/').FirstOrDefault(); // page id
                            drNewReport["Page"] = pageName; // page name
                            drNewReport["Value"] = drValues["value"]; // the data

                            // Add the new records to the reports table
                            dtReports.Rows.Add(drNewReport);
                        }
                    }
                }

                // Create a data view for the reports data
                DataView dvReportsDate = new DataView(dtReports);
                // Select the distinct report data by the date column
                DataTable distinctDate = dvReportsDate.ToTable(true, "Date");

                // Create a data view for the reports data
                DataView dvReportsColumnName = new DataView(dtReports);
                // Select the distinct report data by the column name column
                DataTable distinctColumnName = dvReportsDate.ToTable(true, "ColumnName");

                foreach (DataRow drDate in distinctDate.Rows)
                {
                    // Select all the reports data rows by date column value
                    DataRow[] drReports = dtReports.Select("Date='" + drDate["Date"].ToString() + "'");
                    dsReports.Merge(PushReportDataToDataSet(distinctColumnName, drReports));
                }

                StringBuilder sbFbInfo = new StringBuilder();
                sbFbInfo.Append(dsActivityLogNameIP);
                sbFbInfo.Append("FormatToReport() for Page name : ");
                sbFbInfo.Append(pageName);
                sbFbInfo.Append(" completed");

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode, sbFbInfo.ToString(),
                               string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                if (sbFbInfo != null)
                {
                    sbFbInfo = null;
                }
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsReports;
        }

        /// <summary>
        /// Push Reports data to dataset
        /// </summary>
        /// <param name="drReports">The reports data row</param>
        /// <returns>The reports dataset</returns>
        private DataSet PushReportDataToDataSet(DataTable dtColumnName, DataRow[] drReports)
        {
            DataSet dsReportDetails = new DataSet();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "PushReportDataToDataSet() started...", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Create a new data table for the reports data
                DataTable dtReportDetails = new DataTable("Report");

                // Add date column
                dtReportDetails.Columns.Add("Date");
                // Add account id column
                dtReportDetails.Columns.Add("AccountId");
                // Add account column
                dtReportDetails.Columns.Add("Account");
                // Add page id column
                dtReportDetails.Columns.Add("PageId");
                // Add page column
                dtReportDetails.Columns.Add("Page");

                // Traverse through the columns collection and create the columns to
                // report details data table
                foreach (DataRow drColumnName in dtColumnName.Rows)
                {
                    dtReportDetails.Columns.Add(drColumnName["ColumnName"].ToString());
                }

                DataRow drReportDetail = dtReportDetails.NewRow();
                // Add date
                drReportDetail["Date"] = drReports[0]["Date"];
                // Add account id
                drReportDetail["AccountId"] = drReports[0]["AccountId"];
                // add account data
                drReportDetail["Account"] = drReports[0]["Account"];
                // add page id 
                drReportDetail["PageId"] = drReports[0]["PageId"];
                // add page data
                drReportDetail["Page"] = drReports[0]["Page"];

                // Push the data to the report details table
                foreach (DataRow drReport in drReports)
                {
                    drReportDetail[drReport["ColumnName"].ToString()] = drReport["Value"];
                }
                dtReportDetails.Rows.Add(drReportDetail);
                dsReportDetails.Tables.Add(dtReportDetails);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "PushReportDataToDataSet() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsReportDetails;
        }

        /// <summary>
        /// Create Report header
        /// </summary>
        /// <param name="reportcolumn">The report name</param>
        /// <returns>The Report column</returns>
        private List<string> ReportHeader(string reportcolumn)
        {
            List<string> reportHeader = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ReportHeader() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                switch (reportcolumn)
                {
                    case PERIOD_WEEKLY:
                        reportHeader = new List<string> { "Period", "Date", "AccountId", "Account", "PageId", "Page", 
                                                        "page_impressions", "page_impressions_unique", "page_impressions_paid", 
                                                        "page_impressions_paid_unique", "page_impressions_organic", 
                                                        "page_impressions_organic_unique", "page_impressions_viral", 
                                                        "page_views_unique", "page_views_login", "page_views_login_unique", 
                                                        "page_stories", "page_storytellers", "page_storytellers_by_country", 
                                                        "page_storytellers_by_locale", "page_engaged_users", "page_consumptions", 
                                                        "page_consumptions_unique", "page_places_checkin_total", 
                                                        "page_places_checkin_total_unique", "page_places_checkin_mobile", 
                                                        "page_places_checkin_mobile_unique", "page_negative_feedback", 
                                                        "page_negative_feedback_unique", "page_negative_feedback_by_type", 
                                                        "page_negative_feedback_by_type_unique", "page_posts_impressions", 
                                                        "page_posts_impressions_unique", "page_posts_impressions_paid", 
                                                        "page_posts_impressions_paid_unique", "page_posts_impressions_organic", 
                                                        "page_posts_impressions_organic_unique", "page_posts_impressions_viral", 
                                                        "page_posts_impressions_viral_unique", "page_tab_views_login_top_unique", 
                                                        "page_tab_views_login_top" };
                        break;
                    case PERIOD_MONTHLY:
                        reportHeader = new List<string> { "Period", "Date", "AccountId", "Account", "PageId", "Page" };
                        break;
                    case PERIOD_LIFETIME:
                        reportHeader = new List<string> { "Period", "Date", "AccountId", "Account", "PageId", "Page", 
                                                        "page_fans", "page_fans_locale", "page_fans_country" };
                        break;
                    case PERIOD_28DAYS:
                        reportHeader = new List<string> { "Period", "Date", "AccountId", "Account", "PageId", "Page", 
                                                        "page_impressions", "page_impressions_unique", "page_impressions_paid", 
                                                        "page_impressions_paid_unique", "page_impressions_organic", 
                                                        "page_impressions_organic_unique", "page_impressions_viral", 
                                                        "page_stories", "page_storytellers", "page_storytellers_by_country", 
                                                        "page_storytellers_by_locale", "page_engaged_users", "page_consumptions", 
                                                        "page_consumptions_unique", "page_places_checkin_total", 
                                                        "page_places_checkin_total_unique", "page_places_checkin_mobile", 
                                                        "page_places_checkin_mobile_unique", "page_negative_feedback", 
                                                        "page_negative_feedback_unique", "page_negative_feedback_by_type", 
                                                        "page_negative_feedback_by_type_unique", "page_posts_impressions", 
                                                        "page_posts_impressions_unique", "page_posts_impressions_paid", 
                                                        "page_posts_impressions_paid_unique", "page_posts_impressions_organic", 
                                                        "page_posts_impressions_organic_unique", "page_posts_impressions_viral", 
                                                        "page_posts_impressions_viral_unique" };
                        break;
                    case PERIOD_DAILY:
                        reportHeader = new List<string> { "Period", "Date", "AccountId", "Account", "PageId", "Page", 
                                                        "page_impressions", "page_impressions_unique", "page_impressions_paid", 
                                                        "page_impressions_paid_unique", "page_impressions_organic", 
                                                        "page_impressions_organic_unique", "page_impressions_viral", 
                                                        "page_views", "page_views_unique", "page_views_login", "page_views_login_unique", 
                                                        "page_views_logout", "page_fan_adds", "page_fan_adds_unique", 
                                                        "page_fans_by_like_source", "page_fans_by_like_source_unique", 
                                                        "page_fan_removes", "page_fan_removes_unique", "page_friends_of_fans", 
                                                        "page_stories", "page_storytellers", "page_storytellers_by_country", 
                                                        "page_storytellers_by_locale", "page_engaged_users", "page_consumptions", 
                                                        "page_consumptions_unique", "page_places_checkin_total", 
                                                        "page_places_checkin_total_unique", "page_places_checkin_mobile", 
                                                        "page_places_checkin_mobile_unique", "page_places_checkins_by_age_gender", 
                                                        "page_places_checkins_by_locale", "page_places_checkins_by_country", 
                                                        "page_negative_feedback", "page_negative_feedback_unique", 
                                                        "page_negative_feedback_by_type", "page_negative_feedback_by_type_unique", 
                                                        "page_posts_impressions", "page_posts_impressions_unique", 
                                                        "page_posts_impressions_paid", "page_posts_impressions_paid_unique", 
                                                        "page_posts_impressions_organic", "page_posts_impressions_organic_unique", 
                                                        "page_posts_impressions_viral", "page_posts_impressions_viral_unique", 
                                                        "page_tab_views_login_top_unique", "page_tab_views_login_top" };
                        break;
                    default:
                        reportHeader = new List<string> { };
                        break;
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "ReportHeader() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return reportHeader;
        }
    }
}

