﻿/******************************************************************************************
Filename        : FacebookInsights.cs
Purpose         : To connect to the Facebook Insights API to grab the data.
                    
Created by      : Raja B
Created date    : 09-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
09-Mar-2013     Raja B                              Initial created
30-Apr-2013     Karthic M                           Modified to get 5 types of report.  
  
*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using APIGrabberUtility;

namespace APIGrabberDataSources
{
    class FacebookInsightsMetrics
    {   
        #region Facebook Insights - Specific to individual FBI account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;
        
        // The datasource general activty code
        private int dsGeneralActivityCode;
        
        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        /// <summary>
        /// The facebook insights metrics constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourceActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public FacebookInsightsMetrics(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                                                        int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }
        
        #region Metrics

        /// <summary>
        /// To get the list of page urls
        /// </summary>
        /// <param name="urlsList">the page urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">the page access token</param>
        /// <param name="startDate">the start date range</param>
        /// <param name="endDate">the end date range</param>
        /// <param name="period">the period</param>
        /// <returns>the page urls list</returns>
        public List<string> GetPageUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                      dsActivityLogNameIP + "GetPageUrlsList() started..", string.Empty,
                                      SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Append the page impresions urls
                urlsList = GetPageImpressionsUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Apend the page view urls
                urlsList = GetPageViewUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Append the page user urls
                urlsList = GetPageUserUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Append the stories people urls
                urlsList = GetStoriesPeopleUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Append the page engagement urls
                urlsList = GetPageEngagementUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Append the page post urls
                urlsList = GetPagePostUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);
                // Append the page content urls
                urlsList = GetPageContentUrlsList(urlsList, accountId, pageId, pageAccessToken,
                                                            startDate, endDate, period);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                          dsActivityLogNameIP + "GetPageUrlsList() completed", string.Empty,
                                          SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        ///  To get the page impression metrics url lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page impressions urls</returns>
        private List<string> GetPageImpressionsUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000
                        
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                      dsActivityLogNameIP + "GetPageImpressionsUrlsList() started..", string.Empty,
                                      SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);

                const string page_impressions = "page_impressions";
                const string page_impressions_unique = "page_impressions_unique";
                const string page_impressions_paid = "page_impressions_paid";
                const string page_impressions_paid_unique = "page_impressions_paid_unique";
                const string page_impressions_organic = "page_impressions_organic";
                const string page_impressions_organic_unique = "page_impressions_organic_unique";
                const string page_impressions_viral = "page_impressions_viral";

                #region the commented metrics
                //const string page_impressions_viral_unique = "page_impressions_viral_unique";
                //const string page_impressions_by_story_type = "page_impressions_by_story_type";
                //const string page_impressions_by_story_type_unique = "page_impressions_by_story_type_unique";
                //const string page_impressions_by_city_unique = "page_impressions_by_city_unique";
                //const string page_impressions_by_age_gender_unique = "page_impressions_by_age_gender_unique";
                //const string page_impressions_frequency_distribution = "page_impressions_frequency_distribution";
                //const string page_impressions_viral_frequency_distribution = "page_impressions_viral_frequency_distribution";
                #endregion

                string dateRangeAndAccessToken = "?fields=id,name&since=" + startDate + "&until=" +
                                                 endDate + "&access_token=" + pageAccessToken + period;

                urlsList.Add(graphUrlWithPageId + page_impressions + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_paid + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_paid_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_organic + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_organic_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_impressions_viral + dateRangeAndAccessToken);

                #region the commented metrics
                //UrlsList.Add(graphUrlWithPageId + page_impressions_viral_unique + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_by_story_type + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_by_story_type_unique + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_by_city_unique + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_by_age_gender_unique + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_frequency_distribution + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_impressions_viral_frequency_distribution + dateRangeAndAccessToken);
                #endregion

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                      dsActivityLogNameIP + "GetPageImpressionsUrlsList() completed", string.Empty,
                                      SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }            
            return urlsList;
        }
        
        /// <summary>
        /// To get the page view metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page view urls</returns>
        private List<string> GetPageViewUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetPageViewUrlsList() started..", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);

                const string page_views = "page_views";
                const string page_views_unique = "page_views_unique";
                const string page_views_login = "page_views_login";
                const string page_views_login_unique = "page_views_login_unique";
                const string page_views_logout = "page_views_logout";

                #region the commented metrics
                //const string page_views_external_referrals = "page_views_external_referrals";
                #endregion
                
                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);
                
                urlsList.Add(graphUrlWithPageId + page_views + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_views_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_views_login + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_views_login_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_views_logout + dateRangeAndAccessToken);

                #region the commented metrics
                //UrlsList.Add(graphUrlWithPageId + page_views_external_referrals + dateRangeAndAccessToken);
                #endregion

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                      dsActivityLogNameIP + "GetPageViewUrlsList() completed", string.Empty,
                                      SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To get the page user metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page user urls</returns>
        public List<string> GetPageUserUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetPageUserUrlsList() started..", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);
                
                const string page_fans = "page_fans";
                const string page_fans_locale = "page_fans_locale";
                #region the commented metrics
                //const string page_fans_city = "page_fans_city";
                #endregion
                const string page_fans_country = "page_fans_country";
                #region the commented metrics
                //const string page_fans_gender_age = "page_fans_gender_age";
                #endregion
                const string page_fan_adds = "page_fan_adds";

                const string page_fan_adds_unique = "page_fan_adds_unique";
                const string page_fans_by_like_source = "page_fans_by_like_source";
                const string page_fans_by_like_source_unique = "page_fans_by_like_source_unique";
                const string page_fan_removes = "page_fan_removes";
                const string page_fan_removes_unique = "page_fan_removes_unique";
                const string page_friends_of_fans = "page_friends_of_fans";

                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);

                //Page	If period = 0 -> page_fans	page_fans_locale	page_fans_country
                urlsList.Add(graphUrlWithPageId + page_fans + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_fans_locale + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_fans_country + dateRangeAndAccessToken);

                if (period != "0")
                {
                    #region the commented metrics
                    //UrlsList.Add(graphUrlWithPageId + page_fans_city + dateRangeAndAccessToken);
                    //UrlsList.Add(graphUrlWithPageId + page_fans_gender_age + dateRangeAndAccessToken);        
                    #endregion
                    urlsList.Add(graphUrlWithPageId + page_fan_adds + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_fan_adds_unique + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_fans_by_like_source + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_fans_by_like_source_unique + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_fan_removes + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_fan_removes_unique + dateRangeAndAccessToken);
                    urlsList.Add(graphUrlWithPageId + page_friends_of_fans + dateRangeAndAccessToken);
                }
                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                         dsActivityLogNameIP + "GetPageUserUrlsList() completed", string.Empty,
                                         SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To get the stories people metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of stories people urls</returns>
        private List<string> GetStoriesPeopleUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetStoriesPeopleUrlsList() started..", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);

                const string page_stories = "page_stories";
                const string page_storytellers = "page_storytellers";
                #region the commented metrics
                //const string page_stories_by_story_type = "page_stories_by_story_type";
                //const string page_storytellers_by_story_type = "page_storytellers_by_story_type";
                //const string page_storytellers_by_age_gender = "page_storytellers_by_age_gender";
                #endregion
                const string page_storytellers_by_country = "page_storytellers_by_country";
                const string page_storytellers_by_locale = "page_storytellers_by_locale";

                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);

                urlsList.Add(graphUrlWithPageId + page_stories + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_storytellers + dateRangeAndAccessToken);
                #region the commented metrics
                //UrlsList.Add(graphUrlWithPageId + page_stories_by_story_type + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_storytellers_by_story_type + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_storytellers_by_age_gender + dateRangeAndAccessToken);
                #endregion
                urlsList.Add(graphUrlWithPageId + page_storytellers_by_country + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_storytellers_by_locale + dateRangeAndAccessToken);
                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                              dsActivityLogNameIP + "GetStoriesPeopleUrlsList() completed", string.Empty,
                                              SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To get the page engagement metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page engagement urls</returns>
        private List<string> GetPageEngagementUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetPageEngagementUrlsList() started..", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);

                const string page_engaged_users = "page_engaged_users";
                const string page_consumptions = "page_consumptions";
                const string page_consumptions_unique = "page_consumptions_unique";
                #region the commented metrics
                //const string page_consumptions_by_consumption_type = "page_consumptions_by_consumption_type";
                //const string page_consumptions_by_consumption_type_unique = "page_consumptions_by_consumption_type_unique";
                #endregion
                const string page_places_checkin_total = "page_places_checkin_total";
                const string page_places_checkin_total_unique = "page_places_checkin_total_unique";
                const string page_places_checkin_mobile = "page_places_checkin_mobile";
                const string page_places_checkin_mobile_unique = "page_places_checkin_mobile_unique";
                const string page_places_checkins_by_age_gender = "page_places_checkins_by_age_gender";
                const string page_places_checkins_by_locale = "page_places_checkins_by_locale";
                const string page_places_checkins_by_country = "page_places_checkins_by_country";
                const string page_negative_feedback = "page_negative_feedback";
                const string page_negative_feedback_unique = "page_negative_feedback_unique";
                const string page_negative_feedback_by_type = "page_negative_feedback_by_type";
                const string page_negative_feedback_by_type_unique = "page_negative_feedback_by_type_unique";

                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);
                
                urlsList.Add(graphUrlWithPageId + page_engaged_users + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_consumptions + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_consumptions_unique + dateRangeAndAccessToken);
                #region the commented metrics
                //UrlsList.Add(graphUrlWithPageId + page_consumptions_by_consumption_type + dateRangeAndAccessToken);
                //UrlsList.Add(graphUrlWithPageId + page_consumptions_by_consumption_type_unique + dateRangeAndAccessToken);
                #endregion
                urlsList.Add(graphUrlWithPageId + page_places_checkin_total + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkin_total_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkin_mobile + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkin_mobile_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkins_by_age_gender + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkins_by_locale + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_places_checkins_by_country + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_negative_feedback + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_negative_feedback_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_negative_feedback_by_type + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_negative_feedback_by_type_unique + dateRangeAndAccessToken);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                   dsActivityLogNameIP + "GetPageEngagementUrlsList() completed", string.Empty,
                                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To get the page post metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page post urls</returns>
        private List<string> GetPagePostUrlsList(List<string> urlsList, string accountId, string pageId,
                                                                string pageAccessToken, double startDate,
                                                                double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetPagePostUrlsList() started..", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);
                
                const string page_posts_impressions = "page_posts_impressions";
                const string page_posts_impressions_unique = "page_posts_impressions_unique";
                const string page_posts_impressions_paid = "page_posts_impressions_paid";
                const string page_posts_impressions_paid_unique = "page_posts_impressions_paid_unique";
                const string page_posts_impressions_organic = "page_posts_impressions_organic";
                const string page_posts_impressions_organic_unique = "page_posts_impressions_organic_unique";
                const string page_posts_impressions_viral = "page_posts_impressions_viral";
                const string page_posts_impressions_viral_unique = "page_posts_impressions_viral_unique";
                #region the commented metrics
                //const string page_posts_impressions_frequency_distribution = "page_posts_impressions_frequency_distribution";
                #endregion

                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);

                urlsList.Add(graphUrlWithPageId + page_posts_impressions + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_paid + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_paid_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_organic + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_organic_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_viral + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_posts_impressions_viral_unique + dateRangeAndAccessToken);
                #region the commented metrics
                //UrlsList.Add(graphUrlWithPageId + page_posts_impressions_frequency_distribution + dateRangeAndAccessToken);
                #endregion

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                   dsActivityLogNameIP + "GetPagePostUrlsList() completed", string.Empty,
                                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To get the page content metrics urls lists
        /// </summary>
        /// <param name="UrlsList">the urls list</param>
        /// <param name="accountId">the account id</param>
        /// <param name="pageId">the page id</param>
        /// <param name="pageAccessToken">page access token</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="period">the period</param>
        /// <returns>the list of page content urls</returns>
        private List<string> GetPageContentUrlsList(List<string> urlsList, string accountId, string pageId,
                                                              string pageAccessToken, double startDate,
                                                              double endDate, string period)
        {
            // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

            try
            {   
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "GetPageContentUrlsList() started..",
                                     string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string graphUrlWithPageId = GetGraphUrlWithPageId(pageId);

                const string page_tab_views_login_top_unique = "page_tab_views_login_top_unique";
                const string page_tab_views_login_top = "page_tab_views_login_top";

                string dateRangeAndAccessToken = GetDateRangeAndAccessToken(startDate, endDate, pageAccessToken, period);

                urlsList.Add(graphUrlWithPageId + page_tab_views_login_top_unique + dateRangeAndAccessToken);
                urlsList.Add(graphUrlWithPageId + page_tab_views_login_top + dateRangeAndAccessToken);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                        dsActivityLogNameIP + "GetPageContentUrlsList() completed",
                                        string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                               ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return urlsList;
        }

        /// <summary>
        /// To concatenate and form the graph url with pageid
        /// </summary>
        /// <param name="pageId">the page id</param>
        /// <returns>the graph url</returns>
        private string GetGraphUrlWithPageId(string pageId)
        {
            string graphUrlWithPageId = string.Empty;

            StringBuilder sbGraphUrlWithPageId = null;
            try
            {
                sbGraphUrlWithPageId = new StringBuilder();
                sbGraphUrlWithPageId.Append("https://graph.facebook.com/");
                sbGraphUrlWithPageId.Append(pageId);
                sbGraphUrlWithPageId.Append("/Insights/");

                graphUrlWithPageId = sbGraphUrlWithPageId.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (sbGraphUrlWithPageId != null)
                {
                    sbGraphUrlWithPageId = null;
                }
            }
            return graphUrlWithPageId;
        }

        /// <summary>
        /// To concatenate and form the url request query with date range and access token
        /// </summary>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="pageAccessToken">the page access token</param>
        /// <param name="period">the period</param>
        /// <returns>the request query string with date range and access token</returns>
        private string GetDateRangeAndAccessToken(double startDate, double endDate, string pageAccessToken, string period)
        {
            string dateRangeAndAccessToken = string.Empty;

            StringBuilder sbDateRangeAndAccessToken = null;
            try
            {
                sbDateRangeAndAccessToken = new StringBuilder();
                sbDateRangeAndAccessToken.Append("?since=");
                sbDateRangeAndAccessToken.Append(startDate);
                sbDateRangeAndAccessToken.Append("&until=");
                sbDateRangeAndAccessToken.Append(endDate);
                sbDateRangeAndAccessToken.Append("&access_token=");
                sbDateRangeAndAccessToken.Append(pageAccessToken);
                sbDateRangeAndAccessToken.Append(period);

                dateRangeAndAccessToken = sbDateRangeAndAccessToken.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (sbDateRangeAndAccessToken != null)
                {
                    sbDateRangeAndAccessToken = null;
                }
            }
            return dateRangeAndAccessToken;
        }

        #endregion

        #region All Metrics

        ///// <summary>
        ///// To get the domain content metrics urls lists
        ///// </summary>
        ///// <param name="UrlsList">the urls list</param>
        ///// <param name="accountId">the account id</param>
        ///// <param name="pageId">the page id</param>
        ///// <param name="pageAccessToken">page access token</param>
        ///// <param name="startDate">the start date</param>
        ///// <param name="endDate">the end date</param>
        ///// <returns>the list of domain content urls</returns>
        //public static List<string> GetDomainContentUrlsList(List<string> urlsList, string accountId, string pageId,
        //                                                        string pageAccessToken, double startDate,
        //                                                        double endDate)
        //{
        //    // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

        //    try
        //    {
        //        // To get the activity event log name and the local ip
        //        string dsActivityLogNameIP = UtilityHelper.GetdsActivityLogNameIP(FACEBOOK_INSIGHTS_ACTIVITY_NAME);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                             dsActivityLogNameIP + "GetDomainContentUrlsList() started..", string.Empty,
        //                             SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

        //        string graphUrlWithPageId = "https://graph.facebook.com/" + pageId + "/Insights/";
        //        string dateRangeAndAccessToken = "?since=" + startDate + "&until=" +
        //                                         endDate + "&access_token=" + pageAccessToken;


        //        const string domain_feed_clicks = "domain_feed_clicks";
        //        const string domain_feed_views = "domain_feed_views";
        //        const string domain_stories = "domain_stories";
        //        const string domain_widget_comments_adds = "domain_widget_comments_adds";
        //        const string domain_widget_comments_views = "domain_widget_comments_views";
        //        const string domain_widget_comments_feed_views = "domain_widget_comments_feed_views";
        //        const string domain_widget_comments_feed_clicks = "domain_widget_comments_feed_clicks";
        //        const string domain_widget_like_views = "domain_widget_like_views";
        //        const string domain_widget_likes = "domain_widget_likes";
        //        const string domain_widget_like_feed_views = "domain_widget_like_feed_views";
        //        const string domain_widget_like_feed_clicks = "domain_widget_like_feed_clicks";
        //        const string domain_widget_send_views = "domain_widget_send_views";
        //        const string domain_widget_send_clicks = "domain_widget_send_clicks";
        //        const string domain_widget_send_inbox_views = "domain_widget_send_inbox_views";
        //        const string domain_widget_send_inbox_clicks = "domain_widget_send_inbox_clicks";

        //        UrlsList.Add(graphUrlWithPageId + domain_feed_clicks + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_feed_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_stories + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_comments_adds + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_comments_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_comments_feed_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_comments_feed_clicks + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_like_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_likes + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_like_feed_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_like_feed_clicks + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_send_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_send_clicks + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_send_inbox_views + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + domain_widget_send_inbox_clicks + dateRangeAndAccessToken);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                                               dsActivityLogNameIP + "GetDomainContentUrlsList() completed", string.Empty,
        //                                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
        //                       ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

        //        throw ex;
        //    }
        //    return UrlsList;
        //}

        ///// <summary>
        ///// To get the page post engagement metrics urls lists
        ///// </summary>
        ///// <param name="UrlsList">the urls list</param>
        ///// <param name="accountId">the account id</param>
        ///// <param name="pageId">the page id</param>
        ///// <param name="pageAccessToken">page access token</param>
        ///// <param name="startDate">the start date</param>
        ///// <param name="endDate">the end date</param>
        ///// <returns>the list of page post engagement urls</returns>
        //public static List<string> GetPagePostEngagementUrlsList(List<string> urlsList, string accountId, string pageId,
        //                                                        string pageAccessToken, double startDate,
        //                                                        double endDate)
        //{
        //    // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

        //    try
        //    {
        //        // To get the activity event log name and the local ip
        //        string dsActivityLogNameIP = UtilityHelper.GetdsActivityLogNameIP(FACEBOOK_INSIGHTS_ACTIVITY_NAME);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                             dsActivityLogNameIP + "GetPagePostEngagementUrlsList() started..", string.Empty,
        //                             SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

        //        string graphUrlWithPageId = "https://graph.facebook.com/" + pageId + "/Insights/";
        //        string dateRangeAndAccessToken = "?since=" + startDate + "&until=" +
        //                                         endDate + "&access_token=" + pageAccessToken;

        //        const string post_consumptions = "post_consumptions";
        //        const string post_consumptions_unique = "post_consumptions_unique";
        //        const string post_consumptions_by_type = "post_consumptions_by_type";
        //        const string post_consumptions_by_type_unique = "post_consumptions_by_type_unique";
        //        const string post_engaged_users = "post_engaged_users";
        //        const string post_negative_feedback = "post_negative_feedback";
        //        const string post_negative_feedback_unique = "post_negative_feedback_unique";
        //        const string post_negative_feedback_by_type = "post_negative_feedback_by_type";
        //        const string post_negative_feedback_by_type_unique = "post_negative_feedback_by_type_unique";

        //        UrlsList.Add(graphUrlWithPageId + post_consumptions + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_consumptions_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_consumptions_by_type + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_consumptions_by_type_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_engaged_users + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_negative_feedback + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_negative_feedback_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_negative_feedback_by_type + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_negative_feedback_by_type_unique + dateRangeAndAccessToken);
                
        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                                                    dsActivityLogNameIP + "GetPagePostEngagementUrlsList() completed", 
        //                                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
        //                       ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

        //        throw ex;
        //    }
        //    return UrlsList;
        //}

        ///// <summary>
        ///// To get the page post impressions metrics urls lists
        ///// </summary>
        ///// <param name="UrlsList">the urls list</param>
        ///// <param name="accountId">the account id</param>
        ///// <param name="pageId">the page id</param>
        ///// <param name="pageAccessToken">page access token</param>
        ///// <param name="startDate">the start date</param>
        ///// <param name="endDate">the end date</param>
        ///// <returns>the list of page post impressions urls</returns>
        //public static List<string> GetPagePostImpressionsUrlsList(List<string> urlsList, string accountId, string pageId,
        //                                                        string pageAccessToken, double startDate,
        //                                                        double endDate)
        //{
        //    // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

        //    try
        //    {
        //        // To get the activity event log name and the local ip
        //        string dsActivityLogNameIP = UtilityHelper.GetdsActivityLogNameIP(FACEBOOK_INSIGHTS_ACTIVITY_NAME);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                             dsActivityLogNameIP + "GetPagePostImpressionsUrlsList() started..", string.Empty,
        //                             SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

        //        string graphUrlWithPageId = "https://graph.facebook.com/" + pageId + "/Insights/";
        //        string dateRangeAndAccessToken = "?since=" + startDate + "&until=" +
        //                                         endDate + "&access_token=" + pageAccessToken;

        //        const string post_impressions = "post_impressions";
        //        const string post_impressions_unique = "post_impressions_unique";
        //        const string post_impressions_paid = "post_impressions_paid";
        //        const string post_impressions_paid_unique = "post_impressions_paid_unique";
        //        const string post_impressions_fan = "post_impressions_fan";
        //        const string post_impressions_fan_unique = "post_impressions_fan_unique";
        //        const string post_impressions_fan_paid = "post_impressions_fan_paid";
        //        const string post_impressions_fan_paid_unique = "post_impressions_fan_paid_unique";
        //        const string post_impressions_organic = "post_impressions_organic";
        //        const string post_impressions_organic_unique = "post_impressions_organic_unique";
        //        const string post_impressions_viral = "post_impressions_viral";
        //        const string post_impressions_viral_unique = "post_impressions_viral_unique";
        //        const string post_impressions_by_story_type = "post_impressions_by_story_type";
        //        const string post_impressions_by_story_type_unique = "post_impressions_by_story_type_unique";

        //        UrlsList.Add(graphUrlWithPageId + post_impressions + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_paid + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_paid_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_fan + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_fan_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_fan_paid + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_fan_paid_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_organic + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_organic_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_viral + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_viral_unique + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_by_story_type + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_impressions_by_story_type_unique + dateRangeAndAccessToken);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                                                    dsActivityLogNameIP + "GetPagePostImpressionsUrlsList() completed",
        //                                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
        //                       ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

        //        throw ex;
        //    }
        //    return UrlsList;
        //}

        ///// <summary>
        ///// To get the page post stories and people talking about this metrics urls lists
        ///// </summary>
        ///// <param name="UrlsList">the urls list</param>
        ///// <param name="accountId">the account id</param>
        ///// <param name="pageId">the page id</param>
        ///// <param name="pageAccessToken">page access token</param>
        ///// <param name="startDate">the start date</param>
        ///// <param name="endDate">the end date</param>
        ///// <returns>the list of page post stories and people talking about this urls</returns>
        //public static List<string> GetPagePostStoriesandPeopletalkingaboutthisUrlsList(List<string> urlsList, string accountId, string pageId,
        //                                                      string pageAccessToken, double startDate,
        //                                                      double endDate)
        //{
        //    // Format : 166794433356457/insights/page_impressions?since=1329724800&until=1329984000

        //    try
        //    {
        //        // To get the activity event log name and the local ip
        //        string dsActivityLogNameIP = UtilityHelper.GetdsActivityLogNameIP(FACEBOOK_INSIGHTS_ACTIVITY_NAME);

        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                             dsActivityLogNameIP + "GetPagePostStoriesandPeopletalkingaboutthisUrlsList() started..", 
        //                             string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

        //        string graphUrlWithPageId = "https://graph.facebook.com/" + pageId + "/Insights/";
        //        string dateRangeAndAccessToken = "?since=" + startDate + "&until=" +
        //                                         endDate + "&access_token=" + pageAccessToken;

        //        const string post_stories = "post_stories";
        //        const string post_storytellers = "post_storytellers";
        //        const string post_stories_by_action_type = "post_stories_by_action_type";
        //        const string post_storytellers_by_action_type = "post_storytellers_by_action_type";

        //        UrlsList.Add(graphUrlWithPageId + post_stories + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_storytellers + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_stories_by_action_type + dateRangeAndAccessToken);
        //        UrlsList.Add(graphUrlWithPageId + post_storytellers_by_action_type + dateRangeAndAccessToken);
                
        //        UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
        //                            dsActivityLogNameIP + "GetPagePostStoriesandPeopletalkingaboutthisUrlsList() completed",
        //                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        //    }
        //    catch (Exception ex)
        //    {
        //        UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
        //                       ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

        //        throw ex;
        //    }
        //    return UrlsList;
        //}
                
        #endregion
    }
}
