﻿/******************************************************************************************
Filename        : GADimensionsMetrics
Purpose         : To grab the Google Analytics data by Dimensions and Metrics
                    
Created by      : Raja B
Created date    : 02-Apr-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
02-Apr-2013     Raja B                              Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;

using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class GADimensionsMetrics
    {
        // The # of records fetch per request
        private const int maxPageSize = 10000;

        // The initial starting index for the pagination of the data fetch
        private const int INITIAL_STARTINDEX = 1;

        #region Google Analytics - Specific to individual GA account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        /// <summary>
        /// The google analytics dimensions and metrics constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourceActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public GADimensionsMetrics(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }

        /// <summary>
        /// To get GA report data
        /// </summary>
        /// <param name="service">the google analytics service</param>
        /// <param name="accountId">account id</param>
        /// <param name="accountName">account name</param>
        /// <param name="profileId">profile id</param>
        /// <param name="profileName">profile name</param>
        /// <param name="startDate">start date</param>
        /// <param name="endDate">end date</param>
        /// <param name="dimensions">dimensions</param>
        /// <param name="metrics">metrics</param>
        /// <param name="outputFileName">the google analytics output file name</param>
        /// <param name="rootFolderPath">the google analytics root folder path</param>
        /// <param name="backupZipFolderName">the google analytics back up folder name</param>
        /// <param name="etlFolderPath">the google analytics etl folder path</param>
        /// <returns>true if successful in getting the data. Otherwise false</returns>
        public bool GetReportData(AnalyticsService service, string accountId, string accountName, 
                                                    string profileId, string profileName, string startDate, string endDate, 
                                                    string dimension, string metrics, string outputFileName, 
                                                    string rootFolderPath, string backupZipFolderName, string etlFolderPath)
        {
            bool dataRetrieved = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetReportData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get the google analytics data
                dataRetrieved = GetData(service, accountId, accountName, profileId, profileName, 
                                                            startDate, endDate, dimension, metrics,
                                                            outputFileName, rootFolderPath, backupZipFolderName,
                                                            etlFolderPath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetReportData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dataRetrieved;
        }

        /// <summary>
        /// To get GA data
        /// </summary>        
        /// <param name="service">the google analytics service</param>
        /// <param name="accountId">account id</param>
        /// <param name="accountName">account name</param>
        /// <param name="profileId">profile id</param>
        /// <param name="profileName">profile name</param>
        /// <param name="startDate">start date</param>
        /// <param name="endDate">end date</param>
        /// <param name="dimensions">dimensions</param>
        /// <param name="metrics">metrics</param>
        /// <param name="outputFileName">the google analytics output file name</param>
        /// <param name="rootFolderPath">the google analytics root folder path</param>
        /// <param name="backupZipFolderName">the google analytics back up folder name</param>
        /// <param name="etlFolderPath">the google analytics etl folder path</param>
        /// <returns>true if successful in getting the data. Otherwise false</returns>
        private bool GetData(AnalyticsService service, string accountId, string accountName, 
                                                string profileId, string profileName, string startDate, string endDate,
                                                string dimension, string metrics, string outputFileName,
                                                string rootFolderPath, string backupZipFolderName,
                                                string etlFolderPath)
        {
            bool dataRetrieved = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() started.", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get google analytisc data
                dataRetrieved = GetData(service, accountId, accountName, profileId, profileName,
                                        startDate, endDate, dimension, metrics,
                                        outputFileName, rootFolderPath, backupZipFolderName,
                                        INITIAL_STARTINDEX, string.Empty, etlFolderPath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() completed", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                            ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }            
            return dataRetrieved;
        }

        /// <summary>
        /// To get GA data
        /// </summary>
        /// <param name="service">the google analytics service</param>
        /// <param name="accountId">account id</param>
        /// <param name="accountName">account name</param>
        /// <param name="profileId">profile id</param>
        /// <param name="profileName">profile name</param>
        /// <param name="startDate">start date</param>
        /// <param name="endDate">end date</param>
        /// <param name="dimensions">dimensions</param>
        /// <param name="metrics">metrics</param>
        /// <param name="outputFileName">the google analytics output file name</param>
        /// <param name="rootFolderPath">the google analytics root folder path</param>
        /// <param name="backupZipFolderName">the google analytics back up folder name</param>
        /// <param name="startIndex">the loop start index for retrieving the data per request</param>
        /// <param name="existingFileName">the new (first time) / already existing file name</param>
        /// <param name="etlFolderPath">the google analytics etl folder path</param>
        /// <returns>true if successful in getting the data. Otherwise false</returns>
        private bool GetData(AnalyticsService service, string accountId, string accountName, 
                                                string profileId, string profileName, string startDate, string endDate,
                                                string dimensions, string metrics, string outputFileName,
                                                string rootFolderPath, string backupZipFolderName,
                                                int startIndex, string existingFileName, string etlFolderPath)
        {   
            bool dataRetrieved = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                GaData googleAnalyticsData = null;
                long totalResults = 0;
                string existingFileNameValue = string.Empty;

                DateTime dtStartDate = DateTime.Now;
                DateTime.TryParse(startDate, out dtStartDate);
                // format the start date to the google analytics api service acceptable format
                startDate = dtStartDate.ToString("yyyy-MM-dd");

                DateTime dtEndDate = DateTime.Now;
                DateTime.TryParse(endDate, out dtEndDate);
                // format the end date to the google analytics api service acceptable format
                endDate = dtEndDate.ToString("yyyy-MM-dd");

                // Get the google analytisc data by calling the google analytics api service
                var gaMetricsData = service.Data.Ga.Get("ga:" + profileId, // Table Id. ga: + profile id.
                                                startDate, // Start date.
                                                endDate, // End date.
                                                metrics); // Metrics.

                // Set the dimensions
                gaMetricsData.Dimensions = dimensions;
                // Set the start index
                gaMetricsData.StartIndex = startIndex;
                // Set the maximum result (max page size) should be retrieve per call
                gaMetricsData.MaxResults = maxPageSize;

                // Retrieve the google analytics records
                googleAnalyticsData = gaMetricsData.Fetch();

                // Get the total results ( the total # of records found for the call )
                totalResults = googleAnalyticsData.TotalResults == null ? 0 : (long) googleAnalyticsData.TotalResults;
                
                // For pagination.
                if (totalResults > (startIndex + maxPageSize - 1))
                {
                    #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                    //existingFileNameValue = GenerateCSVFile(googleAnalyticsData, accountId, accountName, profileId, profileName,
                    //                    rootFolderPath, outputFileName, backupZipFolderName, startIndex, existingFileName, etlFolderPath,
                    //                    totalResults,false);
                    #endregion

                    // Method to write first 10000 records to the csv file
                    existingFileNameValue = GenerateCSVFile(googleAnalyticsData, accountId, accountName, profileId, profileName,
                                        rootFolderPath, outputFileName, backupZipFolderName, startIndex, existingFileName, etlFolderPath,
                                        false);

                    
                    // Set the start index for the pagination
                    startIndex = startIndex + maxPageSize;

                    // Get the data
                    GetData(service, accountId, accountName, profileId, profileName, startDate, endDate, dimensions, metrics,
                                        outputFileName, rootFolderPath, backupZipFolderName, startIndex, existingFileNameValue, etlFolderPath);
                }
                else
                {
                    #region Commented the ctl file related code by Sankar on 5/20/13, since the ctl file not required any more to ETL
                    //existingFileNameValue = GenerateCSVFile(googleAnalyticsData, accountId, accountName, profileId, profileName,
                    //                   rootFolderPath, outputFileName, backupZipFolderName, startIndex, existingFileName, etlFolderPath,
                    //                   totalResults, true);
                    #endregion

                    // The file name is returned - since the records are appended for each 10,000 records
                    existingFileNameValue = GenerateCSVFile(googleAnalyticsData, accountId, accountName, profileId, profileName,
                                        rootFolderPath, outputFileName, backupZipFolderName, startIndex, existingFileName, etlFolderPath,
                                         true);
                }
                dataRetrieved = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetData() completed.", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dataRetrieved;
        }
                
        /// <summary>
        /// To generate CSV file based on GA data
        /// </summary>
        /// <param name="googleAnalyticsData">google analytics data object</param>
        /// <param name="accountId">account id</param>
        /// <param name="accountName">account name</param>
        /// <param name="profileId">profile id</param>
        /// <param name="profileName">profile name</param>
        /// <param name="rootFolderPath">google analytics root folder path (config)</param>
        /// <param name="outputFileName">google analytics output file name</param>
        /// <param name="backupZipFolderName">google analytics backup folder for zip files</param>
        /// <param name="startIndex">the starting index for report data retrieval</param>
        /// <param name="existingFileName">the existing file to which the report data should be appended</param>
        /// <param name="lastRecordsSet">whether it is the last set of records to be grabbed</param>
        /// <returns>the newly created (first time) / existing file name</returns>
        private string GenerateCSVFile(GaData googleAnalyticsData, string accountId, string accountName,
                                    string profileId, string profileName, string rootFolderPath, string outputFileName, 
                                    string backupZipFolderName, int startIndex, string existingFileName, string etlFolderPath,
                                     bool lastRecordsSet) //long recordCount
        {
            string fileName = string.Empty;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GenerateCSVFile() started..", string.Empty, 
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                                
                DataSet dsData = new DataSet();
                // Get the dataset created with the report schema
                dsData = GetReportSchemaCreated(googleAnalyticsData, outputFileName, dsData);
                // Get the dataset loaded with report data
                dsData = GetReportMerged(accountName, profileId, profileName, googleAnalyticsData, dsData);

                // Verify whether the root folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(rootFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "Generate CSV File : Google Analytics root folder path not found or not accessible. " +
                                            "The root folder path is " + rootFolderPath);
                }

                // Verify whether the backup folder exist. If not, try to create it.
                string backupZipFileSubPath = rootFolderPath + backupZipFolderName;
                if (!UtilityHelper.VerifyFolderExists(backupZipFileSubPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "Generate CSV File : Google Analytics backup folder path not found or not accessible. " +
                                            "The backup folder path is " + backupZipFileSubPath);
                }

                // Verify whether the etl folder exist. If not, try to create it.
                if (!UtilityHelper.VerifyFolderExists(etlFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "Generate CSV File : Google Analytics etl folder path not found or not accessible. " +
                                            "The etl folder path is " + etlFolderPath);
                }
                
                // returns the filename
                fileName = UtilityHelper.WriteCSV(dsData, rootFolderPath, outputFileName, backupZipFileSubPath, startIndex,
                                                    existingFileName, etlFolderPath, lastRecordsSet); //recordCount

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GenerateCSVFile() completed", string.Empty, 
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return fileName;
        }

        /// <summary>
        /// To merge the accountname, profileid and the profilename to the report data
        /// </summary>
        /// <param name="accountName">accountname</param>
        /// <param name="profileId">profileid</param>
        /// <param name="profileName">profilename</param>
        /// <param name="googleAnalyticsData">google analytics data object</param>
        /// <param name="dsReport">the report dataset</param>
        /// <returns>the report dataset filled with report data</returns>
        private DataSet GetReportMerged(string accountName, string profileId, string profileName,
                                                            GaData googleAnalyticsData, DataSet dsReport)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetReportMerged() started..", string.Empty, 
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                object[] itemArray = new object[3];
                itemArray[0] = accountName;     // account name
                itemArray[1] = profileId;       // profile id
                itemArray[2] = profileName;     // profile name

                if (googleAnalyticsData != null && googleAnalyticsData.Rows != null && googleAnalyticsData.Rows.Count > 0)
                {
                    foreach (var drReport in googleAnalyticsData.Rows)
                    {
                        var drNewReport = dsReport.Tables[0].NewRow();
                        // Concatenate the itemArray and the drReport row values
                        drNewReport.ItemArray = itemArray.Concat(drReport).ToArray();
                        dsReport.Tables[0].Rows.Add(drNewReport);
                    }
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetReportMerged() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsReport;
        }
        
        /// <summary>
        /// To create the report schema to the dataset
        /// </summary>
        /// <param name="googleAnalyticsData">google analytics data object</param>
        /// <param name="tableName">dataset tablename</param>
        /// <param name="dsReport">the report dataset</param>
        /// <returns>the dataset with report schema</returns>
        private DataSet GetReportSchemaCreated(GaData googleAnalyticsData, string tableName, DataSet dsReport)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode, 
                                    dsActivityLogNameIP + "GetReportSchemaCreated() started..", string.Empty, 
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable dtReport = new DataTable(tableName);
                // Get the ga data column hearders
                var totalColumns = googleAnalyticsData.ColumnHeaders;

                // Add account columnn 
                dtReport.Columns.Add("Account", System.Type.GetType("System.String"));
                // Add profile id column 
                dtReport.Columns.Add("ProfileId", System.Type.GetType("System.String"));
                // Add profile column
                dtReport.Columns.Add("Profile", System.Type.GetType("System.String"));

                foreach (var column in totalColumns)
                {
                    try
                    {
                        // Ge the ga data column headers data type
                        string dataType = column.DataType;
                        dtReport.Columns.Add(column.Name, System.Type.GetType("System.String"));
                    }
                    catch (Exception ex)
                    {                        
                        dtReport.Columns.Add(tableName + column.Name, System.Type.GetType("System.String"));
                        UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                    ex.StackTrace, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                    }
                }

                dsReport.Tables.Add(dtReport);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetReportSchemaCreated() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, 
                                        ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dsReport;
        }
    }
}
