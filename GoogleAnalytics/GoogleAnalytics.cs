﻿/******************************************************************************************
Filename        : GoogleAnalytics.cs
Purpose         : To connect to google analytics API to grab the data.
                    
Created by      : Raja B
Created date    : 29-Mar-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
29-Mar-2013     Raja B                              Initial created

*******************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Samples.Helper;
using Google.Apis.Util;

using Google.Apis.Samples.Helper.NativeAuthorizationFlows;

using System.Security.Authentication;
using System.Security.Cryptography;
using System.Configuration;
using System.Data;

using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class GoogleAnalytics
    {
        #region Google Analytics - Generic variables for GA all accounts

        // The client identifier
        private string clientIdentifier = string.Empty;

        // The client secret
        private string clientSecret = string.Empty;

        // The storage - unique to the application
        private string storage = string.Empty;

        // The api key
        private string key = string.Empty;

        // The google analytics api grabber root folder - 
        // dot net end google analytics file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The google analytics api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        // The report file name. This is the partial file name. 
        // The datetime stamp will be concatenated with the final file name.
        private string reportFileName = string.Empty;

        // The google analytics api grabber - etl folder path.
        // The path to which the dfa for dart 7z file should be 
        // dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;

        // The google analytics report repeat frequency. Eg. Yesterday data (D), Past one week data (W),
        // Past one month data (M), Past one year data (Y)
        private string repeatFrequency = string.Empty;

        // The api scope
        private const string DEVSTORAGESCOPE_READONLY = "https://www.googleapis.com/auth/devstorage.read_only";

        // The api additional scope
        private readonly string googleAnalyticsScope = AnalyticsService.Scopes.Analytics.GetStringValue();

        #endregion

        #region Google Analytics - Specific to individual GA account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        /// <summary>
        /// The google analytics constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourceActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public GoogleAnalytics(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {            
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }

        /// <summary>
        /// Entry point method for the Google Analytics run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool InitiateDataGrabbing()
        {
            bool jobSucceeded = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                    dsActivityLogNameIP + "InitiateDataGrabbing() started..", string.Empty,
                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get the configuration information from the DB (tbl_DatasourceConfig)
                GetConfigInfo();

                // Get the accounts details from the DB (tbl_GAAccounts)
                IEnumerable<Tbl_GAAccounts> gaAccounts = DataAccessUtility.GetGAAccounts(dsCode);
                
                // Register the authenticator.
                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
                provider.ClientIdentifier = clientIdentifier;
                provider.ClientSecret = clientSecret;

                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);                
                var service = new AnalyticsService(auth);
                
                // Account Ids - 11401921   12305179    32102700    33685389
                // Get the accounts               
                Accounts accounts = service.Management.Accounts.List().Fetch();

                #region to get the refresh token through the api grabber ui (APIGrabberTester) - for one time activity

                bool onlyToGetRefreshToken = false;
                bool.TryParse(ConfigurationManager.AppSettings["googleanalytics_onlytogetrefreshtoken"], out onlyToGetRefreshToken);
                if (onlyToGetRefreshToken)
                {
                    UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing : The API Grabber UI application running in " +
                                    "onlyToGetRefreshToken mode. The googleanalytics_onlytogetrefreshtoken configuration flag in " +
                                    "the api grabber ui app.config file is set to true", string.Empty, SeverityLevel.INFORMATION,
                                    EmailNotification.NOT_NEEDED);

                    jobSucceeded = true;
                    return jobSucceeded;
                }

                #endregion

                if (accounts == null)
                {
                    throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : Error in accounts retrieval");
                }
                else if (accounts.Items.Count == 0)
                {
                    throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : No account found");
                }

                string accountName = string.Empty;
                string accountId = string.Empty;
                foreach (var gaAccount in gaAccounts)
                {
                    // Get the account id
                    accountId = gaAccount.Accountid != null ? gaAccount.Accountid.ToString() : string.Empty;
                    if (string.IsNullOrEmpty(accountId))
                    {
                        throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : Account id is empty.");
                    }

                    // Get the account
                    Account account = accounts.Items.Where(x => x.Id == accountId).FirstOrDefault();
                    if (account == null)
                    {
                        accountName = string.Empty;
                    }
                    else
                    {
                        accountName = account.Name;
                    }

                    string startDate = string.Empty;
                    string endDate = string.Empty;

                    if (repeatFrequency.ToUpper() == RepeatFrequency.DAILY) //daily
                    {
                        startDate = DateTime.Now.AddDays(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.WEEKLY) //weekly
                    {
                        startDate = DateTime.Now.AddDays(-7).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.MONTHLY) //monthly
                    {
                        startDate = DateTime.Now.AddMonths(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else if (repeatFrequency.ToUpper() == RepeatFrequency.YEARLY) //yearly
                    {
                        startDate = DateTime.Now.AddYears(-1).ToString();
                        endDate = DateTime.Now.AddDays(-1).ToString();
                    }
                    else
                    {
                        // Get the start date range
                        startDate = gaAccount.Startdate.ToString();
                        // Get the end date range
                        endDate = gaAccount.Enddate.ToString();
                    }

                    // The comma separated list of dimensions
                    string dimension = gaAccount.Dimension != null ? gaAccount.Dimension.ToString() : string.Empty;
                    // The comma separated list of metrics
                    string metrics = gaAccount.Metrics != null ? gaAccount.Metrics.ToString() : string.Empty;
                    // The output file name
                    string outputFileName = gaAccount.Outputfilename != null ? gaAccount.Outputfilename.ToString() : string.Empty;

                    // Get the webproperies for the account
                    Webproperties webProperties = service.Management.Webproperties.List(accountId).Fetch();

                    if (webProperties == null)
                    {
                        throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : Error in web properties retrieval " +
                                                    "for account id : " + accountId);
                    }
                    else if (webProperties.Items.Count == 0)
                    {
                        throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : No web properties found for account id : " +
                                                        accountId);
                    }

                    string webPropertyId = string.Empty;
                    foreach (Webproperty webProperty in webProperties.Items)
                    {
                        webPropertyId = webProperty.Id;
                        // Get the profiles
                        Profiles profiles = service.Management.Profiles.List(accountId, webPropertyId).Fetch();

                        if (profiles == null)
                        {
                            throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : Error in profiles retrieval " +
                                                        "for account id : " + accountId + " and web property id : " +
                                                        webPropertyId);
                        }
                        else if (profiles.Items.Count == 0)
                        {
                            throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : No profiles found for account id : " +
                                                            accountId + " and web property id : " + webPropertyId);
                        }

                        string profileId = string.Empty;
                        foreach (Profile profile in profiles.Items)
                        {
                            // Get the profile id
                            profileId = profile.Id;

                            // Retrieve the report data
                            bool dataRetrievalSucceeded = RetrieveReportData(service, accountId, accountName,
                                                                                profileId, profile.Name,
                                                                                startDate, endDate,
                                                                                dimension, metrics, outputFileName);
                            if (!dataRetrievalSucceeded)
                            {
                                throw new Exception(dsActivityLogNameIP + "InitiateDataGrabbing : Error occurred during " +
                                                        "google analytics report generation");
                            }
                        }
                    }
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(dsCode);

                jobSucceeded = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)                
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(dsCode);
                }                  
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            Tbl_DatasourceConfig datasourceConfig = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get the GA datasource config information from tbl_DatasourceConfig 
                // DB table by dsCode
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(dsCode);

                if (datasourceConfig != null)
                {
                    // Get the client identifier
                    clientIdentifier = datasourceConfig.Clientidentifier;
                    if (string.IsNullOrEmpty(clientIdentifier))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Client Identifier, config info not found in DB");
                    }

                    // Get the client secret info
                    clientSecret = datasourceConfig.Clientsecret;
                    if (string.IsNullOrEmpty(clientSecret))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Client Secret, config info not found in DB");
                    }

                    // Get the storage info
                    storage = datasourceConfig.Storage;
                    if (string.IsNullOrEmpty(storage))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Storage, config info not found in DB");
                    }

                    // Get the key info
                    key = datasourceConfig.Apikey;
                    if (string.IsNullOrEmpty(key))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Key, config info not found in DB");
                    }

                    // Get the root folder path
                    rootFolderPath = datasourceConfig.Rootfolderpath;
                    if (string.IsNullOrEmpty(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                    }
                    rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);

                    // Get the backup zip (7z) folder name
                    backupZipFolderName = datasourceConfig.Backupzipfoldername;
                    if (string.IsNullOrEmpty(backupZipFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                    }

                    // Get the report file name
                    reportFileName = datasourceConfig.Reportfilename;
                    if (string.IsNullOrEmpty(reportFileName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");
                    }

                    // Get the etl folder path
                    etlFolderPath = datasourceConfig.Etlfolderpath;
                    if (string.IsNullOrEmpty(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                            "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                    }
                    etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);

                    // Get the frequency (the date range - daily, weekly, monthly, yearly) data, that should be fetched
                    repeatFrequency = datasourceConfig.Repeat;
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);               
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message, ex.StackTrace,
                            SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (datasourceConfig != null)
                {
                    datasourceConfig = null;
                }
            }
        }

        /// <summary>
        /// To get the authorization
        /// </summary>
        /// <param name="client">the native application client object</param>
        /// <returns>the object which implements iauthorization state - the authorization state object</returns>
        private IAuthorizationState GetAuthorization(NativeApplicationClient client)
        {
            IAuthorizationState authorizationState = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetAuthorization() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Check if there is a cached refresh token available in the db table,
                // tbl_DatasourceConfig
                authorizationState = GetCachedRefreshToken();

                if (authorizationState != null)
                {
                    try
                    {
                        // Refresh the token
                        client.RefreshToken(authorizationState);
                        return authorizationState;
                    }
                    catch (DotNetOpenAuth.Messaging.ProtocolException ex)
                    {
                        throw new Exception("Google Analytics : Get Authorization : Using existing refresh token failed: " +
                                                    ex.Message);
                    }
                }

                // Retrieve the authorization from the user.                
                authorizationState = AuthorizationMgr.RequestNativeAuthorization(client, googleAnalyticsScope,
                                                                                    DEVSTORAGESCOPE_READONLY);
                // Cache the refresh token to the db datasource config table
                SetCachedRefreshToken(authorizationState);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetAuthorization() completed", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return authorizationState;
        }

        /// <summary>         
        /// Returns a cached refresh token for this application, or null if unavailable.         
        /// </summary>         
        /// <returns>The authorization state containing a Refresh Token, or null if unavailable</returns>         
        private AuthorizationState GetCachedRefreshToken()         
        {
            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            // Retrieve the refresh token from db table, tbl_DatasourceConfig
            string contents = DataAccessUtility.GetScopeRefreshToken(dsCode);

            if (string.IsNullOrEmpty(contents))             
            {                 
                return null; // No cached token available.             
            }                          
            string[] content = contents.Split(new[] { "\r\n" }, StringSplitOptions.None);                       
            // Create the authorization state.             
            string[] scopes = content[0].Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);             
            string refreshToken = content[1];

            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            return new AuthorizationState(scopes) { RefreshToken = refreshToken };         
        }

        /// <summary>
        /// Saves a refresh token to the specified storage name to the DB table
        /// </summary>
        /// <param name="state">the authorization state object</param>       
        private void SetCachedRefreshToken(IAuthorizationState state)         
        {
            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetCachedRefreshToken() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            // Create the file content.             
            string scopes = state.Scope.Aggregate("", (left, append) => left + " " + append);             
            string content = scopes + "\r\n" + state.RefreshToken;

            // Cache the refresh token to the db table, tbl_DatasourceConfig
            DataAccessUtility.SetScopeRefreshToken(dsCode, content);

            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "SetCachedRefreshToken() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
        }         
       
        /// <summary>
        /// The retrieve the report data
        /// </summary>
        /// <param name="service">the google analytics api service</param>
        /// <param name="accountId">the account id</param>
        /// <param name="accountName">the account name</param>
        /// <param name="profileId">the user profile id</param>
        /// <param name="profileName">the profile name</param>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <param name="dimension">the dimension</param>
        /// <param name="metrics">the metrics</param>
        /// <param name="outputFileName">the output file name</param>
        /// <returns>True, if successful. Otherwise false</returns>
        private bool RetrieveReportData(AnalyticsService service, string accountId, string accountName,
                                                    string profileId, string profileName,
                                                    string startDate, string endDate,
                                                    string dimension, string metrics, string outputFileName)
        {
            UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "RetrieveReportData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            bool reportRetrievalSucceeded = false;
            GADimensionsMetrics gaDimMetrics = null;
            try
            {
                // Set / Pass on the data source related config info
                gaDimMetrics = new GADimensionsMetrics(dsCode,dsActivityLogNameIP,
                                                            dsGeneralActivityCode,
                                                            dsErrorActivityCode);

                // To retrieve the google analytics report data
                reportRetrievalSucceeded = gaDimMetrics.GetReportData(service, accountId, accountName,
                                                                    profileId, profileName, startDate, endDate,
                                                                    dimension, metrics, outputFileName,
                                                                    rootFolderPath, backupZipFolderName, etlFolderPath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "RetrieveReportData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (gaDimMetrics != null)
                {
                    gaDimMetrics = null;
                }
            }
            return reportRetrievalSucceeded;
        }
    }
}
