﻿/******************************************************************************************
Filename        : VizuDatasource.cs
Purpose         : Class file for Vizu and VizuConnector to grab the data.
                    
Created by      : Karthic
Created date    : 13-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
13-May-2013     Karthic                             Initial created
*******************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APIGrabberDataSources
{
    public class VizuDatasource
    {
        public string CampaignId;

        public string CampaignName;

        public string CampaignStatus;

        public string Objective;

        public string ResultWeightByVoteProportions;

        public string WeightByMediaPlan;

        public string CampaignStartDate;

        public string CampaignEndDate;

        public string MetricId;

        public string MetricText;

        public string MetricType;

        public string SingleExposure;

        public string AnswersFilters;

        public string ExposedN;

        public string ExposedVotes;

        public string ExposedResults;

        public string ExposedHarveyBall;

        public string OverAllPerformanceLift;

        public string ControlVotes;

        public string ControlN;

        public string ControlResults;

        public string ControlHarveyBall;

        public string ExposureNum;

        public string ExposureVotes;

        public string ExposureN;

        public string ExposureResults;

        public string ExposureLift;

        public string ExposureHarveyBall;
        
        public List<SurveyResult.answerRow> AnswersList;

        public RawData.responsesDataTable RawDataResponseSet;

        public CreativePerformance.creativesDataTable CreativeRow;

        public WildCardPerformance.wildCardsDataTable WildCardRows;

        public SitePerformance.siteDataTable SiteRows;

        public FrequencyPerformance.exposuresDataTable FrequencyRows;

        public CampaignTargetingPerformance.siteDataTable SiteCampaignRows;
    }

    public class MetricList
    {
        public string CampaignId;

        public List<string> MetricIds = new List<string>();
        
        public List<Metric> MetricsList = new List<Metric>();
    }

    public class Metric
    {
        public string MetricId;

        public string CampaignId;

        public string MetricTxt;

        public string MetricType;

        public string CampaignName;

        public string CampaignStatus;

        public string CampaignStartDate;

        public string CampaignEndDate;

        public string Objective;

        public string VoteProportionsWeightingEnabled;

        public string MediaPlanWeightingEnabled;

    }
}
