﻿/******************************************************************************************
Filename        : Vizu.cs
Purpose         : To connect to the Vizu API to grab the data.
                    
Created by      : Karthic
Created date    : 13-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
13-May-2013     Karthic                             Initial created
23-May-2013     Raja                                Modified the code to fit into the current
                                                    architecture
 
*******************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using System.Data;
using System.Configuration;

using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class Vizu
    {
        #region Vizu - Generic variables for vizu all accounts

        // The api key
        private string apiKey = string.Empty;

        // The campaign list url
        private string campaignListURL = string.Empty;

        // The metric list url
        private string metricListURL = string.Empty;

        // The vizu api grabber root folder - 
        // dot net end vizu file i/o root folder path
        private string rootFolderPath = string.Empty;

        // The vizu api grabber report folder name - 
        // dot net end vizu file i/o report (metric xml files) folder path
        private string reportFolderName = string.Empty;

        // The vizu api grabber - the back up folder name
        private string backupZipFolderName = string.Empty;

        // The report file name. This is the partial file name. 
        // The datetime stamp will be concatenated with the final file name.
        private string reportFileName = string.Empty;

        // The vizu api grabber - etl folder path.
        // The path to which the vizu 7z and ctl file should be 
        // dropped for the etl job to pick it up.
        private string etlFolderPath = string.Empty;


        //Answer column seperator for answer txt       
        private string answerColumnSeperator = string.Empty;

        //Answer row seperator for each answers row
        private string answerRowSeperator = string.Empty;

        private Tbl_DatasourceConfig datasourceConfig = null;

        #endregion

        #region Vizu - Specific to individual Vizu account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion

        /// <summary>
        /// The vizu api grabber constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourcedsdsActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public Vizu(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;
        }

        /// <summary>
        /// Entry point method for the vizu api run
        /// </summary>
        /// <returns>True, if successful. Otherwise false</returns>
        public bool InitiateDataGrabbing()
        {
            bool jobSucceeded = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Read the configuration information from the database
                GetConfigInfo();

                //Retrieve the report data 
                jobSucceeded = RetrieveReportData();
                if (!jobSucceeded)
                {
                    throw new Exception(dsActivityLogNameIP +
                                            "InitiateDataGrabbing() : Error occurred during vizu report generation");
                }

                // Update the schedule (tbl_schedule table) with next run
                DataAccessUtility.ScheduleSuccessfullyCompleted(dsCode);

                jobSucceeded = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "InitiateDataGrabbing() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {
                if (!jobSucceeded)
                {
                    // Update the schedule (tbl_schedule table) that the process is unsuccessfully completed
                    DataAccessUtility.ScheduleUnsuccessfullyCompleted(dsCode);
                }
            }
            return jobSucceeded;
        }

        /// <summary>
        /// To retrieve the configuration information
        /// </summary>
        private void GetConfigInfo()
        {
            datasourceConfig = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Retrieve the configuration information for the database (tbl_datasourceconfig table)
                datasourceConfig = DataAccessUtility.GetDatasourceConfig(dsCode);

                if (datasourceConfig != null)
                {
                    // Api key
                    apiKey = datasourceConfig.Apikey;
                    if (string.IsNullOrEmpty(apiKey))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Key, config info not found in DB");
                    }

                    // The root folder path for vizu
                    rootFolderPath = datasourceConfig.Rootfolderpath;
                    if (string.IsNullOrEmpty(rootFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Root Folder Path, config info not found in DB");
                    }
                    rootFolderPath = UtilityHelper.FormatFolderPath(rootFolderPath, true);

                    // The report file name (partial), the datetime stamp will be concatenated with this report file name
                    // to make the full file name.
                    reportFolderName = datasourceConfig.Reportfoldername;
                    if (string.IsNullOrEmpty(reportFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Report folder name, config info not found in DB");
                    }
                    reportFolderName = reportFolderName + UtilityHelper.FormatDateTimeForFileName();

                    // The report file name (partial), the datetime stamp will be concatenated with this report file name
                    // to make the full file name.
                    reportFileName = datasourceConfig.Reportfilename;
                    if (string.IsNullOrEmpty(reportFileName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Report Filename, config info not found in DB");

                    }

                    // The back up folder name. The back up of the csv file taken in 7z format
                    backupZipFolderName = datasourceConfig.Backupzipfoldername;
                    if (string.IsNullOrEmpty(backupZipFolderName))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Backup zip folder name, config info not found in DB");
                    }

                    // The etl folder path - the folder path to which the final report file (7z extension) and the control file
                    // should be file i/o.
                    etlFolderPath = datasourceConfig.Etlfolderpath;
                    if (string.IsNullOrEmpty(etlFolderPath))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - ETL Folder Path, config info not found in DB");
                    }
                    etlFolderPath = UtilityHelper.FormatFolderPath(etlFolderPath, false);

                    // Campaign list url
                    campaignListURL = datasourceConfig.Dsconfig1;
                    if (string.IsNullOrEmpty(campaignListURL))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Dsconfig1(campaign list url), config info not found in DB");
                    }

                    // Metric list url
                    metricListURL = datasourceConfig.Dsconfig2;
                    if (string.IsNullOrEmpty(metricListURL))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Dsconfig2(metric list url), config info not found in DB");
                    }


                    // Row Seperator for answers
                    answerRowSeperator = datasourceConfig.Dsconfig3;
                    if (string.IsNullOrEmpty(answerRowSeperator))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Dsconfig3(Answers row seperator), config info not found in DB");
                    }

                    // Column Seperator for answer text columnn
                    answerColumnSeperator = datasourceConfig.Dsconfig4;
                    if (string.IsNullOrEmpty(answerColumnSeperator))
                    {
                        throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Dsconfig4(Answers column seperator), config info not found in DB");
                    }
                }
                else
                {
                    throw new Exception(dsActivityLogNameIP +
                                        "GetConfigInfo : Fatal error - Config info not found in DB");
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetConfigInfo() completed.",
                                    string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode,
                            ex.Message, ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {//Comment by Karthic to pass to VizuConnector class
                //if (datasourceConfig != null)
                //{
                //    datasourceConfig = null;
                //}
            }
        }

        /// <summary>
        /// To retrieve and file i/o the metric, campaign xml files and to store 
        /// the metrics to the metrics list for further processing.
        /// </summary>        
        /// <returns>True, if successful. Otherwise false</returns>
        private bool RetrieveReportData()
        {
            bool dataRetrievedSuccessfully = false;

            DataSet dsCampaign = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveReportData() started..", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                // Get the query string formed with the apikey
                string queryString = "apiKey=" + apiKey;

                string campaignXmlFilenameWithPath = string.Empty;

                // Get the url formatted by appending the campaignlisturl and the querystring
                string formattedUrl = campaignListURL + "?" + queryString;

                dsCampaign = new DataSet();
                // Do a web request with the formattted url to get the campaign data dataset
                dsCampaign = UtilityHelper.GetWebRequest(formattedUrl);
                // The data view of the campaign data
                DataView dvCampaign = dsCampaign.Tables["campaign"].DefaultView;
                // The filtering condition to filter the campaign data further
                dvCampaign.RowFilter = "status <> 'Ready' AND component = 'AdCatalyst'";

                List<MetricList> metricsListCol = new List<MetricList>();

                DataSet dsMetric = null;
                foreach (DataRow drCampaign in dvCampaign.ToTable().Rows)
                {
                    string campaignId = drCampaign["ID"] != null ? drCampaign["ID"].ToString() : string.Empty;

                    StringBuilder sbCampaignQuery = new StringBuilder();
                    sbCampaignQuery.Append(metricListURL);
                    sbCampaignQuery.Append("?");
                    sbCampaignQuery.Append(queryString);
                    sbCampaignQuery.Append("&campaignId=");
                    sbCampaignQuery.Append(campaignId);
                    // Get the formattted url
                    formattedUrl = sbCampaignQuery.ToString();
                    sbCampaignQuery = null;

                    dsMetric = new DataSet();
                    dsMetric = UtilityHelper.GetWebRequest(formattedUrl);

                    campaignXmlFilenameWithPath = GetXMLFilePath(campaignId);
                    dsMetric.WriteXml(campaignXmlFilenameWithPath);

                    MetricList metricList = new MetricList();
                    metricList.CampaignId = campaignId;

                    if (dsMetric.Tables.Contains("metric"))
                    {
                        DataSet dsVisu = null;
                        foreach (DataRow drMetric in dsMetric.Tables["metric"].Rows)
                        {
                            string metricId = drMetric["ID"] != null ? drMetric["ID"].ToString() : string.Empty;

                            queryString = "apiKey=" + apiKey + "&metricId=" + metricId;

                            dsVisu = new DataSet();
                            dsVisu = UtilityHelper.GetWebRequest(formattedUrl);

                            string metricXmlFilenameWithPath = GetXMLFilePath(metricId);
                            dsVisu.WriteXml(metricXmlFilenameWithPath);

                            metricList.MetricIds.Add(metricId);


                            Metric metricObj = new Metric();
                            metricObj.CampaignId = campaignId;
                            ///
                            foreach (DataRow compaignRow in dsMetric.Tables["campaign"].Rows)
                            {
                                metricObj.CampaignStartDate = compaignRow["startdate"].ToString();
                                metricObj.CampaignEndDate = compaignRow["enddate"].ToString();

                                metricObj.CampaignName = compaignRow["name"].ToString();
                                metricObj.CampaignStatus = compaignRow["status"].ToString();
                                metricObj.Objective = compaignRow["objective"].ToString();

                                metricObj.VoteProportionsWeightingEnabled = compaignRow["voteProportionsWeightingEnabled"].ToString();
                                metricObj.MediaPlanWeightingEnabled = compaignRow["mediaPlanWeightingEnabled"].ToString();
                            }

                            metricObj.MetricTxt = drMetric["txt"].ToString();
                            metricObj.MetricType = drMetric["type"].ToString();
                            metricObj.MetricId = drMetric["ID"].ToString();

                            metricList.MetricsList.Add(metricObj);
                        }
                        metricsListCol.Add(metricList);
                    }
                }

                VizuConnector vizuConnector = new VizuConnector(dsCode, dsActivityLogNameIP,
                                                                    dsGeneralActivityCode, dsErrorActivityCode);

                vizuConnector.DataSourceConfig = datasourceConfig;
                vizuConnector.XmlFilePath = GetGeneratedVizuFolderPath(campaignXmlFilenameWithPath);
                dataRetrievedSuccessfully = vizuConnector.ProcessMetricListData(metricsListCol);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "RetrieveReportData() completed.", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (dsCampaign != null)
                {
                    dsCampaign = null;
                }
            }
            return dataRetrievedSuccessfully;
        }

        /// <summary>
        /// To get the xml file path
        /// </summary>
        /// <param name="Id">id value - campaign id / metric id</param>
        /// <returns>the xml file path</returns>
        private string GetGeneratedVizuFolderPath(string campaignGenPath)
        {
            string vizuFolderPath = string.Empty;

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "GetGeneratedVizuFolderPath() started..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                //c:\datasource\Vizu\20130601172000310370\Vizu\7436.xml
                int lastIndex = campaignGenPath.LastIndexOf("\\");

                if (lastIndex < 0)
                {
                    throw new Exception(dsActivityLogNameIP + "GetGeneratedVizuFolderPath() : Error in parsing the file:" + campaignGenPath);
                }

                //Getting the c:\datasource\Vizu\20130601172000310370\Vizu 
                vizuFolderPath = campaignGenPath.Substring(0, lastIndex);

                //Verify whether able to create the report folder.
                if (!UtilityHelper.VerifyFolderExists(vizuFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP + "GetGeneratedVizuFolderPath() :  Unable to create the vizu report folder." +
                                                    "Vizu folder path - " + vizuFolderPath);
                }

                //Now Getting the path like Getting the c:\datasource\Vizu\20130601172000310370\Vizu\
                vizuFolderPath = campaignGenPath.Substring(0, lastIndex + 1);


                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetGeneratedVizuFolderPath() completed.", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (campaignGenPath != null)
                {
                    campaignGenPath = null;
                }
            }
            return vizuFolderPath;
        }

        /// <summary>
        /// To get the xml file path
        /// </summary>
        /// <param name="Id">id value - campaign id / metric id</param>
        /// <returns>the xml file path</returns>
        private string GetXMLFilePath(string Id)
        {
            string xmlFilenameWithPath = string.Empty;

            StringBuilder sbXmlFolderPath = null;
            StringBuilder sbXmlFilenameWithPath = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "GetXMLFilePath() started..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                sbXmlFolderPath = new StringBuilder();
                sbXmlFolderPath.Append(rootFolderPath);
                sbXmlFolderPath.Append(reportFileName);
                sbXmlFolderPath.Append(@"\");
                // Get the xml file folder path
                string xmlFolderPath = sbXmlFolderPath.ToString();
                sbXmlFolderPath = null;

                // Get the xml metric file folder path formatted to a well formed folder path
                xmlFolderPath = UtilityHelper.FormatFolderPath(xmlFolderPath, false);

                // Verify whether able to create the report folder.
                if (!UtilityHelper.VerifyFolderExists(xmlFolderPath))
                {
                    throw new Exception(dsActivityLogNameIP + "GetXMLFilePath() : Unable to create the vizu report folder." +
                                                    "The report folder path - " + xmlFolderPath);
                }

                sbXmlFilenameWithPath = new StringBuilder();
                sbXmlFilenameWithPath.Append(xmlFolderPath);
                sbXmlFilenameWithPath.Append(Id);
                sbXmlFilenameWithPath.Append(UtilityHelper.XML_FILEEXTN);
                // Get the xml full path including the folder path and the file name
                xmlFilenameWithPath = sbXmlFilenameWithPath.ToString();
                sbXmlFilenameWithPath = null;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                dsActivityLogNameIP + "GetXMLFilePath() completed.", string.Empty,
                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            finally
            {
                if (sbXmlFolderPath != null)
                {
                    sbXmlFolderPath = null;
                }
                if (sbXmlFilenameWithPath != null)
                {
                    sbXmlFilenameWithPath = null;
                }
            }
            return xmlFilenameWithPath;
        }
    }
}

