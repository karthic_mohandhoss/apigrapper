﻿/******************************************************************************************
Filename        : VizuConnector.cs
Purpose         : To connect to the Vizu API to grab the data.
                    
Created by      : Karthic
Created date    : 13-May-2013

Revision history :
-------------------------------------------------------------------------------------------
Date            Modified by         Request Id      Change details
-------------------------------------------------------------------------------------------
13-May-2013     Karthic                             Initial created
23-May-2013     Raja                                Modified the code to fit into the current
                                                    architecture
 
*******************************************************************************************/

using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;

using System.Collections.Generic;

using System.Net;
using APIGrabberUtility;

namespace APIGrabberDataSources
{
    public class VizuConnector
    {
        #region Vizu - Generic variables and constants for vizu all accounts

        private Tbl_DatasourceConfig dataSourceConfig;
        
        //Store the different types of metrics file in the xml folder path
        private string xmlFilePath;
     
        public Tbl_DatasourceConfig DataSourceConfig
        {
            get
            {
                return dataSourceConfig;
            }
            set
            {
                dataSourceConfig = value;
            }
        }
        
        public string XmlFilePath
        {
            get
            {
                return xmlFilePath;
            }
            set
            {
                xmlFilePath = value;
            }
        }
        public DataSet vizuLifeTimeDs = new DataSet();
        
        #endregion
             
        #region Vizu - Specific to individual Vizu account

        // The datasource code
        private int dsCode;

        // The activity event log name and the local ip
        private string dsActivityLogNameIP = string.Empty;

        // The datasource general activty code
        private int dsGeneralActivityCode;

        // The datasource error activity code
        private int dsErrorActivityCode;

        #endregion
        
        /// <summary>
        /// The vizu connector constructor
        /// </summary>
        /// <param name="datasourceCode">the ds code</param>
        /// <param name="datasourcedsdsActivityLogNameIP">the ds activity log name and the ip</param>
        /// <param name="datasourceGeneralActivityCode">the error / event code that should be logged for 
        /// the ds general activity / event</param>
        /// <param name="datasourceErrorActivityCode">the error / event code that should be logged for
        /// the ds error activity / event</param>
        public VizuConnector(int datasourceCode, string datasourceActivityLogNameIP, int datasourceGeneralActivityCode,
                            int datasourceErrorActivityCode)
        {
            dsCode = datasourceCode;
            dsActivityLogNameIP = datasourceActivityLogNameIP;
            dsGeneralActivityCode = datasourceGeneralActivityCode;
            dsErrorActivityCode = datasourceErrorActivityCode;   
        }

        /// <summary>
        /// Process the metric list data with the metrics list column
        /// </summary>
        /// <param name="metricsListCol">Metrics columns</param>
        /// <returns>True if successfull else false</returns>
        public bool ProcessMetricListData(List<MetricList> metricsListCol)
        {
            bool processedSuccessfully = false;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessMetricListData() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                string queryString = string.Empty;

                string overAllFileName = string.Empty;
                string freqPerFileName = string.Empty;
                string creatPerFileName = string.Empty;
                string sitePerFileName = string.Empty;
                string surveyObjFileName = string.Empty;
                string lifeTimPerFileName = string.Empty;
                string wildCardPerFileName = string.Empty;
                string rawDataFileName = string.Empty;
                string campTargFileName = string.Empty;

                string answerRowSeperator = dataSourceConfig.Dsconfig3;
                string answerColumnSeperator = dataSourceConfig.Dsconfig4;

                List<VizuDatasource> overallPer = new List<VizuDatasource>();
                List<VizuDatasource> freqList = new List<VizuDatasource>();

                List<VizuDatasource> creativeList = new List<VizuDatasource>();
                List<VizuDatasource> campTargetList = new List<VizuDatasource>();
                List<VizuDatasource> rawDataList = new List<VizuDatasource>();
                List<VizuDatasource> wildCardList = new List<VizuDatasource>();
                List<VizuDatasource> surveyList = new List<VizuDatasource>();
                List<VizuDatasource> siteList = new List<VizuDatasource>();
                List<SurveyResult.answerRow> answersList = new List<SurveyResult.answerRow>();

                // Get the accounts details from the DB (tbl_VizuUrls)
                IEnumerable<Tbl_VizuUrls> vizuUrls = DataAccessUtility.GetVizuUrls(dsCode);

                string formattedUrl = string.Empty;
                DataSet visuDS = new DataSet();
                foreach (MetricList listObj in metricsListCol)
                {
                    foreach (string metricId in listObj.MetricIds)
                    {
                        foreach (Tbl_VizuUrls dsSou in vizuUrls)
                        {

                            Metric metricObj = listObj.MetricsList.Find(delegate(Metric metricdelObj) { return metricdelObj.MetricId == metricId; });

                            if (metricObj == null)
                            {
                                throw new Exception();

                            }
                            //Overall performance
                            if (dsSou.Metricname.ToUpper().Equals(DataList.OVERALL_PERFORMANCE))
                            {

                                overAllFileName = dsSou.Metricfilename;

                                VizuDatasource vizuDataSourceObj = new VizuDatasource();
                                vizuDataSourceObj.MetricId = metricId;

                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {
                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;

                                    OverAllPerformance overAllPerfDataSet = new OverAllPerformance();
                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);

                                    visuDS.WriteXml(xmlFilePath + DataList.OVERALL_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    overAllPerfDataSet.ReadXml(xmlFilePath + DataList.OVERALL_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);

                                    //Always there is one record even though it is foreach
                                    foreach (OverAllPerformance.controlRow rObj in overAllPerfDataSet.control)
                                    {
                                        vizuDataSourceObj.ControlN = rObj.n;
                                        vizuDataSourceObj.ControlResults = rObj.results;
                                        vizuDataSourceObj.ControlVotes = rObj.votes;
                                        vizuDataSourceObj.ControlHarveyBall = rObj.harveyBall;
                                    }

                                    foreach (OverAllPerformance.exposedRow rObj in overAllPerfDataSet.exposed)
                                    {
                                        vizuDataSourceObj.ExposedHarveyBall = rObj.harveyBall;
                                        vizuDataSourceObj.ExposedN = rObj.n;
                                        vizuDataSourceObj.ExposedResults = rObj.results;
                                        vizuDataSourceObj.ExposedVotes = rObj.votes;
                                    }

                                    foreach (OverAllPerformance.overallPerformanceRow rObj in overAllPerfDataSet.overallPerformance)
                                    {
                                        vizuDataSourceObj.OverAllPerformanceLift = rObj.lift;
                                    }

                                    string answersfilters = string.Empty;

                                    foreach (OverAllPerformance.answerRow answersRowObj in overAllPerfDataSet.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + answersRowObj["id"].ToString() +  answerColumnSeperator 
                                                                                                                + answersRowObj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;

                                    overallPer.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }//Frequency performance
                            else if (dsSou.Metricname.Trim().ToUpper().Equals(DataList.FREQUENCY_PERFORMANCE))
                            {
                                FrequencyPerformance freqPerfObj = new FrequencyPerformance();

                                freqPerFileName = dsSou.Metricfilename;


                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {

                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();
                                    vizuDataSourceObj.MetricId = metricId;

                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;


                                    freqPerfObj = new FrequencyPerformance();
                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);

                                    visuDS.WriteXml(xmlFilePath + DataList.FREQUENCY_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    freqPerfObj.ReadXml(xmlFilePath + DataList.FREQUENCY_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);


                                    foreach (FrequencyPerformance.controlRow rObj in freqPerfObj.control)
                                    {
                                        vizuDataSourceObj.ControlN = rObj.n;
                                        vizuDataSourceObj.ControlResults = rObj.results;
                                        vizuDataSourceObj.ControlVotes = rObj.votes;
                                        vizuDataSourceObj.ControlHarveyBall = rObj.harveyBall;
                                    }

                                    string answersfilters = string.Empty;

                                    foreach (FrequencyPerformance.answerRow answersRowObj in freqPerfObj.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;
                                    vizuDataSourceObj.FrequencyRows = freqPerfObj.exposures;

                                    freqList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }//Creative performance
                            else if (dsSou.Metricname.Trim().ToUpper().Equals(DataList.CREATIVE_PERFORMANCE))
                            {
                                CreativePerformance creativePerfObj = new CreativePerformance();

                                creatPerFileName = dsSou.Metricfilename;

                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {

                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                    vizuDataSourceObj.MetricId = metricId;

                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;

                                    creativePerfObj = new CreativePerformance();
                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.CREATIVE_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    creativePerfObj.ReadXml(xmlFilePath + DataList.CREATIVE_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);

                                    string answersfilters = string.Empty;

                                    foreach (CreativePerformance.answerRow answersRowObj in creativePerfObj.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;
                                    vizuDataSourceObj.CreativeRow = creativePerfObj.creatives;

                                    creativeList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }//site performance
                            else if (dsSou.Metricname.Trim().ToUpper().Equals(DataList.SITE_PERFORMANCE))
                            {
                                SitePerformance sitePerfObj = new SitePerformance();

                                sitePerFileName = dsSou.Metricfilename;

                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {
                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                    vizuDataSourceObj.MetricId = metricId;


                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;


                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.SITE_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    sitePerfObj.ReadXml(xmlFilePath + DataList.SITE_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);

                                    string answersfilters = string.Empty;

                                    foreach (SitePerformance.answerRow answersRowObj in sitePerfObj.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + answersRowObj["id"].ToString() + answerColumnSeperator + answersRowObj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;
                                    vizuDataSourceObj.SiteRows = sitePerfObj.site;

                                    siteList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }//survery performance
                            else if (dsSou.Metricname.Trim().ToUpper().Equals(DataList.SURVEY))
                            {
                                SurveyResult surveyResultObj = new SurveyResult();

                                surveyObjFileName = dsSou.Metricfilename;

                                VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                vizuDataSourceObj.MetricId = metricId;

                                vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                vizuDataSourceObj.MetricType = metricObj.MetricType;

                                vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                vizuDataSourceObj.Objective = metricObj.Objective;

                                vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;

                                try
                                {

                                    queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                    formattedUrl = dsSou.Weburls + "?" + queryString;

                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.SURVEY + metricId + UtilityHelper.XML_FILEEXTN);
                                    surveyResultObj.ReadXml(xmlFilePath + DataList.SURVEY + metricId + UtilityHelper.XML_FILEEXTN);

                                    foreach (SurveyResult.answersRow answRowsObj in surveyResultObj.answers)
                                    {
                                        foreach (SurveyResult.answerRow ansRowObj in answRowsObj.GetanswerRows())
                                        {
                                            answersList.Add(ansRowObj);
                                        }
                                    }
                                    vizuDataSourceObj.AnswersList = answersList;
                                    surveyList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }

                            }
                            else if (dsSou.Metricname.ToUpper().Equals(DataList.LIFETIME_PERFORMANCE))
                            {
                                lifeTimPerFileName = dsSou.Metricfilename;
                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                             
                                try
                                {
                                    formattedUrl = dsSou.Weburls + "?" + queryString;
                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.LIFETIME_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    
                                    if (visuDS.Tables.Contains("date"))
                                    {
                                        DataView cumulativeView;
                                        cumulativeView = visuDS.Tables["date"].DefaultView;

                                        DataRow[] foundRows = visuDS.Tables["dates"].Select("type='cumulative'");
                                        if (foundRows.Count() > 0)
                                        {
                                            //Filter data with cumulative dates
                                            cumulativeView.RowFilter = "dates_Id=" + foundRows[0].Field<int>("dates_Id");
                                            if (cumulativeView.ToTable().Rows.Count > 0)
                                            {
                                                MergeLifeTimeDataSet(visuDS, ReportPeriod.CUMULATIVE);                                                                                        
                                            }
                                        }

                                        foundRows = visuDS.Tables["dates"].Select("type='week'");
                                        if (foundRows.Count() > 0)
                                        {
                                            //Filter data with cumulative dates
                                            cumulativeView.RowFilter = "dates_Id=" + foundRows[0].Field<int>("dates_Id");
                                            if (cumulativeView.ToTable().Rows.Count > 0)
                                            {
                                                MergeLifeTimeDataSet(visuDS, ReportPeriod.CUMULATIVE);
                                            }
                                        }
                                        //dates type=day,week,month
                                        foundRows = visuDS.Tables["dates"].Select("type='month'");
                                        if (foundRows.Count() > 0)
                                        {
                                            //Filter data with cumulative dates
                                            cumulativeView.RowFilter = "dates_Id=" + foundRows[0].Field<int>("dates_Id");
                                            if (cumulativeView.ToTable().Rows.Count > 0)
                                            {
                                                MergeLifeTimeDataSet(visuDS, ReportPeriod.CUMULATIVE);
                                            }
                                        }
                                        foundRows = visuDS.Tables["dates"].Select("type='day'");
                                        if (foundRows.Count() > 0)
                                        {
                                            //Filter data with cumulative dates
                                            cumulativeView.RowFilter = "dates_Id=" + foundRows[0].Field<int>("dates_Id");
                                            if (cumulativeView.ToTable().Rows.Count > 0)
                                            {
                                                MergeLifeTimeDataSet(visuDS, ReportPeriod.CUMULATIVE);
                                            }
                                        }
                                    }//if
                                }
                                catch (Exception eobj)
                                {
                                }
                            }
                            else if (dsSou.Metricname.ToUpper().Equals(DataList.WILDCARD_PERFORMANCE))
                            {
                                WildCardPerformance wildCardPerfDataSet = new WildCardPerformance();

                                wildCardPerFileName = dsSou.Metricfilename;

                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {

                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                    vizuDataSourceObj.MetricId = metricId;

                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;

                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.WILDCARD_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);
                                    wildCardPerfDataSet.ReadXml(xmlFilePath + DataList.WILDCARD_PERFORMANCE + metricId + UtilityHelper.XML_FILEEXTN);

                                    string answersfilters = string.Empty;

                                    foreach (WildCardPerformance.answerRow cobj in wildCardPerfDataSet.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = cobj["id"].ToString() + answerColumnSeperator + cobj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + cobj["id"].ToString() + answerColumnSeperator + cobj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;
                                    vizuDataSourceObj.WildCardRows = wildCardPerfDataSet.wildCards;

                                    wildCardList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }
                            else if (dsSou.Metricname.ToUpper().Equals(DataList.RAWDATA))
                            {
                                RawData rawDataSetObj = new RawData();

                                rawDataFileName = dsSou.Metricfilename;

                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {

                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                    vizuDataSourceObj.MetricId = metricId;

                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;


                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.RAWDATA + metricId + UtilityHelper.XML_FILEEXTN);
                                    rawDataSetObj.ReadXml(xmlFilePath + DataList.RAWDATA + metricId + UtilityHelper.XML_FILEEXTN);

                                    vizuDataSourceObj.RawDataResponseSet = rawDataSetObj.responses;
                                    rawDataList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }
                            else if (dsSou.Metricname.ToUpper().Equals(DataList.CAMPAIGN_TARGETING))
                            {
                                CampaignTargetingPerformance CampaignTargetPerfDataSet = new CampaignTargetingPerformance();

                                campTargFileName = dsSou.Metricfilename;


                                queryString = "apiKey=" + dataSourceConfig.Apikey + "&metricId=" + metricId;
                                formattedUrl = dsSou.Weburls + "?" + queryString;

                                try
                                {

                                    VizuDatasource vizuDataSourceObj = new VizuDatasource();

                                    vizuDataSourceObj.MetricId = metricId;

                                    vizuDataSourceObj.MetricText = metricObj.MetricTxt;
                                    vizuDataSourceObj.MetricType = metricObj.MetricType;

                                    vizuDataSourceObj.CampaignId = metricObj.CampaignId;
                                    vizuDataSourceObj.CampaignName = metricObj.CampaignName;
                                    vizuDataSourceObj.CampaignStatus = metricObj.CampaignStatus;
                                    vizuDataSourceObj.CampaignStartDate = metricObj.CampaignStartDate;
                                    vizuDataSourceObj.CampaignEndDate = metricObj.CampaignEndDate;
                                    vizuDataSourceObj.Objective = metricObj.Objective;

                                    vizuDataSourceObj.ResultWeightByVoteProportions = metricObj.VoteProportionsWeightingEnabled;
                                    vizuDataSourceObj.WeightByMediaPlan = metricObj.MediaPlanWeightingEnabled;


                                    visuDS = UtilityHelper.GetWebRequest(formattedUrl);
                                    visuDS.WriteXml(xmlFilePath + DataList.CAMPAIGN_TARGETING + metricId + UtilityHelper.XML_FILEEXTN);
                                    CampaignTargetPerfDataSet.ReadXml(xmlFilePath + DataList.CAMPAIGN_TARGETING + metricId + UtilityHelper.XML_FILEEXTN);

                                    string answersfilters = string.Empty;

                                    foreach (CampaignTargetingPerformance.answerRow answerRowObj in CampaignTargetPerfDataSet.answer)
                                    {
                                        if (answersfilters.Equals(string.Empty))
                                            answersfilters = answerRowObj["id"].ToString() + answerColumnSeperator + answerRowObj["txt"].ToString();
                                        else
                                            answersfilters = answersfilters + answerRowSeperator + answerRowObj["id"].ToString() + answerColumnSeperator + answerRowObj["txt"].ToString();
                                    }

                                    vizuDataSourceObj.AnswersFilters = answersfilters;
                                    vizuDataSourceObj.SiteCampaignRows = CampaignTargetPerfDataSet.site;
                                    campTargetList.Add(vizuDataSourceObj);
                                }
                                catch (Exception eobj)
                                {

                                }
                            }
                        }
                    }
                }

                ProcessOverAllAndLifeTimeData(overallPer, overAllFileName, lifeTimPerFileName);
             
                ProcessFrequencyData(freqList, freqPerFileName);
                ProcessCreativeData(creativeList, creatPerFileName);
                ProcessCampaignTargetingData(campTargetList, campTargFileName);
                ProcessRawData(rawDataList, rawDataFileName);
                ProcessSiteData(siteList, sitePerFileName);
                ProcessWildCardData(wildCardList, wildCardPerFileName);
                ProcessSurveyData(surveyList, surveyObjFileName);

                processedSuccessfully = true;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessMetricListData() completed", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
            }
            finally
            {

            }

            return processedSuccessfully;
        }

        /// <summary>
        /// To merge the life time data set.
        /// </summary>
        /// <param name="visuDS">vizu data</param>
        /// <param name="dataType">the datatype</param>
        private void MergeLifeTimeDataSet(DataSet vizuDS, string dataType)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessLifeTimeData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuCampaign = new DataTable();
                DataView cumulativeView;
                DataTable vizuData = new DataTable();

                string campaignID = string.Empty;
                string campaignName = string.Empty;
                string metricText = string.Empty;
                string metricID = string.Empty;
                string campaignStatus = string.Empty;
                string metricType = string.Empty;
                string campaignObj = string.Empty;

                string weightByVoteProportions = string.Empty;
                string weightByMediaPlan = string.Empty;
                string answersfilters = string.Empty;

                string startDate = string.Empty;
                string endDate = string.Empty;

                string answerRowSeperator = dataSourceConfig.Dsconfig3;
                string answerColumnSeperator = dataSourceConfig.Dsconfig4;

                if (vizuDS != null && vizuDS.Tables.Count >0)
                {
                    vizuCampaign = vizuDS.Tables["campaign"];

                    if (vizuCampaign.Rows.Count > 0)
                    {
                        DataTable vizuResults = vizuDS.Tables["results"];
                        DataTable vizuFilters = vizuDS.Tables["answer"];

                        if (vizuResults.Rows.Count > 0)
                        {
                            foreach (DataRow vizuResultRow in vizuResults.Rows)
                            {
                                weightByMediaPlan = vizuResultRow["weightByMediaPlan"].ToString();
                                weightByVoteProportions = vizuResultRow["weightByVoteProportions"].ToString();
                            }
                        }                        

                        if (vizuFilters.Rows.Count > 0)
                        {
                            foreach (DataRow vizuAnswerRow in vizuFilters.Rows)
                            {
                                if (answersfilters.Equals(string.Empty))
                                    answersfilters = vizuAnswerRow["id"].ToString() + answerColumnSeperator + vizuAnswerRow["txt"].ToString();
                                else
                                    answersfilters = answersfilters + answerRowSeperator + vizuAnswerRow["id"].ToString() + answerColumnSeperator + vizuAnswerRow["txt"].ToString();
                            }
                        }
                       

                        foreach (DataRow vizuCampaignRow in vizuCampaign.Rows)
                        {
                            campaignID = vizuCampaignRow["id"].ToString();
                            campaignName = vizuCampaignRow["name"].ToString();
                            campaignStatus = vizuCampaignRow["status"].ToString();
                            startDate = vizuCampaignRow["startDate"].ToString();
                            endDate = vizuCampaignRow["endDate"].ToString();
                            campaignObj = vizuCampaignRow["objective"].ToString();
                        }

                        DataTable vizuMetric = new DataTable();
                        vizuMetric = vizuDS.Tables["metric"];

                        if (vizuMetric.Rows.Count > 0)
                        {
                            foreach (DataRow metricDataRow in vizuMetric.Rows)
                            {
                                metricID = metricDataRow["id"].ToString();
                                metricText = metricDataRow["txt"].ToString();
                                metricType = metricDataRow["type"].ToString();
                            }
                        }

                        if (vizuDS.Tables["date"].Rows.Count > 0)
                        {
                            cumulativeView = vizuDS.Tables["date"].DefaultView;
                            if (dataType != ReportPeriod.WEEK)
                                cumulativeView.RowFilter = "dates_Id = '1'";
                            else
                                cumulativeView.RowFilter = "dates_Id = '0'";

                            vizuData = cumulativeView.ToTable();

                            string curDate = DateTime.Now.ToShortDateString();

                            DataColumn dtExtractDate = new DataColumn("ExtractDate", typeof(string));
                            dtExtractDate.DefaultValue = curDate;
                            vizuData.Columns.Add(dtExtractDate);

                            DataColumn dtCol = new DataColumn("Campaign", typeof(string));
                            dtCol.DefaultValue = campaignName;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("Campaign_Id", typeof(string));
                            dtCol.DefaultValue = campaignID;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("Campaign_Status", typeof(string));
                            dtCol.DefaultValue = campaignStatus;
                            vizuData.Columns.Add(dtCol);


                            dtCol = new DataColumn("CampaignObjective", typeof(string));
                            dtCol.DefaultValue = campaignObj;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("CampaignStartDate", typeof(string));
                            dtCol.DefaultValue = startDate;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("CampaignEndDate", typeof(string));
                            dtCol.DefaultValue = endDate;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("MetricTxt", typeof(string));
                            dtCol.DefaultValue = metricText;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("MetricId", typeof(string));
                            dtCol.DefaultValue = metricID;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("MetricType", typeof(string));
                            dtCol.DefaultValue = metricType;
                            vizuData.Columns.Add(dtCol);


                            //Adding three columns

                            dtCol = new DataColumn("ResultWeightByMediaPlan", typeof(string));
                            dtCol.DefaultValue = weightByMediaPlan;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("ResultweightByVoteProportions", typeof(string));
                            dtCol.DefaultValue = weightByVoteProportions;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("Answers", typeof(string));
                            dtCol.DefaultValue = answersfilters;
                            vizuData.Columns.Add(dtCol);

                            dtCol = new DataColumn("DatesType", typeof(string));
                            dtCol.DefaultValue = dataType;
                            vizuData.Columns.Add(dtCol);



                            //Rearrange the columns before exporting

                            vizuData.Columns["Campaign"].SetOrdinal(0);
                            vizuData.Columns["Campaign_Id"].SetOrdinal(1);
                            vizuData.Columns["Campaign_Status"].SetOrdinal(2);
                            vizuData.Columns["CampaignObjective"].SetOrdinal(3);

                            vizuData.Columns["CampaignStartDate"].SetOrdinal(4);
                            vizuData.Columns["CampaignEndDate"].SetOrdinal(5);
                            vizuData.Columns["MetricId"].SetOrdinal(6);
                            vizuData.Columns["MetricTxt"].SetOrdinal(7);
                            vizuData.Columns["MetricType"].SetOrdinal(8);

                            vizuData.Columns["ResultweightByVoteProportions"].SetOrdinal(9);
                            vizuData.Columns["ResultWeightByMediaPlan"].SetOrdinal(10);
                            vizuData.Columns["Answers"].SetOrdinal(11);
                            vizuData.Columns["DatesType"].SetOrdinal(12);


                            vizuData.Columns["startDate"].SetOrdinal(13);
                            vizuData.Columns["endDate"].SetOrdinal(14);
                            vizuData.Columns["Answers"].SetOrdinal(15);
                            vizuData.Columns["controlVotes"].SetOrdinal(16);
                            vizuData.Columns["exposedVotes"].SetOrdinal(17);

                            vizuData.Columns["controlN"].SetOrdinal(18);
                            vizuData.Columns["testN"].SetOrdinal(19);
                            vizuData.Columns["controlResults"].SetOrdinal(20);
                            vizuData.Columns["exposedResults"].SetOrdinal(21);
                            vizuData.Columns["lift"].SetOrdinal(22);
                            vizuData.Columns["harveyBall"].SetOrdinal(23);
                            vizuData.Columns["ExtractDate"].SetOrdinal(24);


                            vizuData.Columns.Remove("dates_Id");
                            vizuData.Columns["Answers"].SetOrdinal(11);

                            if (vizuData.Rows.Count > 0)
                            {
                                DataTable dtSortedTable = vizuData.AsEnumerable()
                                .OrderBy(row => row.Field<string>("Campaign"))
                                .CopyToDataTable();


                                DataSet dsReport = new DataSet();

                                dsReport.Tables.Add(dtSortedTable);

                                vizuLifeTimeDs.Merge(dsReport);
                            }                            
                        }                 
                    }
                    
                }
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessLifeTimeData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        ///  Process Overall performance and life time data
        /// </summary>
        /// <param name="vizuDataListObj"></param>
        /// <param name="OverAllPerfFileName"></param>
        /// <param name="lifeTimeFileName"></param>
        private void ProcessOverAllAndLifeTimeData(List<VizuDatasource> vizuDataListObj, string OverAllPerfFileName, string lifeTimeFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessOverAllAndLifeTimeData() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);


                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "Going to write lifetime data..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                //Going to write the consolidated lifetime data
                UtilityHelper.WriteCSV(vizuLifeTimeDs, dataSourceConfig.Rootfolderpath, lifeTimeFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);


                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "Succesfully generated csv file for Lifetime..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetOverAllDataTableSchema();

                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource vizuDataSourceObj in vizuDataListObj)
                {
                    DataRow rObj = vizuData.NewRow();

                    rObj["Campaign"] = vizuDataSourceObj.CampaignName;
                    rObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                    rObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                    rObj["CampaignObjective"] = vizuDataSourceObj.Objective;

                    rObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                    rObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                    rObj["MetricId"] = vizuDataSourceObj.MetricId;
                    rObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                    rObj["MetricType"] = vizuDataSourceObj.MetricType;
                    rObj["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                    rObj["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                    rObj["Answers"] = vizuDataSourceObj.AnswersFilters;

                    rObj["controlvotes"] = vizuDataSourceObj.ControlVotes;
                    rObj["controlN"] = vizuDataSourceObj.ControlN;
                    rObj["controlResults"] = vizuDataSourceObj.ControlResults;
                    rObj["controlharveyBall"] = vizuDataSourceObj.ControlHarveyBall;

                    rObj["exposedN"] = vizuDataSourceObj.ExposedN;
                    rObj["exposedVotes"] = vizuDataSourceObj.ExposedVotes;
                    rObj["exposedResults"] = vizuDataSourceObj.ExposedResults;
                    rObj["exposedharveyBall"] = vizuDataSourceObj.ExposedHarveyBall;

                    rObj["overallPerformanceLift"] = vizuDataSourceObj.OverAllPerformanceLift;

                    rObj["ExtractDate"] = curDate;
                    vizuData.Rows.Add(rObj);

                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                     .OrderBy(row => row.Field<string>("Campaign"))
                     .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);


                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "Going to write Overall performance data..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, OverAllPerfFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                               dsActivityLogNameIP + "Succesfully generated csv file forOverall performance data..", string.Empty,
                               SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
           
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessOverAllData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with over all data schema 
        /// </summary>
        /// <returns>Empty table of over all data schema</returns>
        private DataTable GetOverAllDataTableSchema()
        {
            DataTable dtOverAllData = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetOverAllDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Add("Answers");
                columnsList.Add("controlVotes");
                columnsList.Add("controlN");
                columnsList.Add("controlResults");
                columnsList.Add("controlharveyBall");
                columnsList.Add("exposedN");
                columnsList.Add("exposedVotes");
                columnsList.Add("exposedResults");
                columnsList.Add("exposedharveyBall");
                columnsList.Add("overallPerformanceLift");
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtOverAllData = new DataTable();
                DataColumn dcOverAllData = null;
                foreach (string column in columnsList)
                {
                    dcOverAllData = new DataColumn(column, stringType);
                    dtOverAllData.Columns.Add(dcOverAllData);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetOverAllDataTableSchema() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtOverAllData;
        }
        
        /// <summary>
        /// Process the frequency list data with the given output file name
        /// </summary>
        /// <param name="vizuDataListObj"></param>
        /// <param name="frequencyDataFileName"></param>
        private void ProcessFrequencyData(List<VizuDatasource> vizuDataListObj, string frequencyDataFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessFrequencyData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
                
                DataTable vizuData = GetFrequencyDataTableSchema();
                
                string curDate = DateTime.Now.ToShortDateString();
                bool firstRow = true;
                foreach (VizuDatasource vizuDataSourceObj in vizuDataListObj)
                {
                    foreach (FrequencyPerformance.exposuresRow exposuresRowObj in vizuDataSourceObj.FrequencyRows)
                    {
                        foreach (FrequencyPerformance.exposureRow exposureRowObj in exposuresRowObj.GetexposureRows())
                        {
                            DataRow freqResultRow = vizuData.NewRow();

                            if (firstRow)
                            {
                                freqResultRow["Campaign"] = vizuDataSourceObj.CampaignName;
                                freqResultRow["Campaign_id"] = vizuDataSourceObj.CampaignId;
                                freqResultRow["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                                freqResultRow["CampaignObjective"] = vizuDataSourceObj.Objective;

                                freqResultRow["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                                freqResultRow["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                                freqResultRow["MetricId"] = vizuDataSourceObj.MetricId;
                                freqResultRow["MetricTxt"] = vizuDataSourceObj.MetricText;

                                freqResultRow["MetricType"] = vizuDataSourceObj.MetricType;
                                freqResultRow["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                                freqResultRow["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                                freqResultRow["Answers"] = vizuDataSourceObj.AnswersFilters;

                                freqResultRow["controlVotes"] =  vizuDataSourceObj.ControlVotes;
                                freqResultRow["controlN"] = vizuDataSourceObj.ControlN;
                                freqResultRow["controlResults"] = vizuDataSourceObj.ControlResults;
                                freqResultRow["controlharveyBall"] = vizuDataSourceObj.ControlHarveyBall;
                                //<control votes="117.0" n="291.0" results="40.2%" harveyBall="Highly Stable" />
                                freqResultRow["exposurenum"] = "";
                                freqResultRow["exposedVotes"] = "";
                                freqResultRow["exposureN"] = "";
                                freqResultRow["exposureResults"] = "";
                                freqResultRow["exposureharveyBall"] = "";
                                freqResultRow["exposurelift"] = "";
                                freqResultRow["ExtractDate"] = curDate;
                                firstRow = false;

                                vizuData.Rows.Add(freqResultRow);
                            }

                            freqResultRow = vizuData.NewRow();

                            freqResultRow["Campaign"] = vizuDataSourceObj.CampaignName;
                            freqResultRow["Campaign_id"] = vizuDataSourceObj.CampaignId;
                            freqResultRow["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                            freqResultRow["CampaignObjective"] = vizuDataSourceObj.Objective;

                            freqResultRow["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                            freqResultRow["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                            freqResultRow["MetricId"] = vizuDataSourceObj.MetricId;
                            freqResultRow["MetricTxt"] = vizuDataSourceObj.MetricText;

                            freqResultRow["MetricType"] = vizuDataSourceObj.MetricType;
                            freqResultRow["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                            freqResultRow["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                            freqResultRow["Answers"] = vizuDataSourceObj.AnswersFilters;

                            freqResultRow["controlVotes"] = "";
                            freqResultRow["controlN"] = "";
                            freqResultRow["controlResults"] = "";
                            freqResultRow["controlharveyBall"] = "";

                            freqResultRow["exposurenum"] = exposureRowObj.num;
                            freqResultRow["exposedVotes"] = exposureRowObj.votes;
                            freqResultRow["exposureN"] = exposureRowObj.n;
                            freqResultRow["exposureResults"] = exposureRowObj.results;
                            freqResultRow["exposureharveyBall"] = exposureRowObj.harveyBall;
                            freqResultRow["exposurelift"] = exposureRowObj.lift;
                            freqResultRow["ExtractDate"] = curDate;
                            vizuData.Rows.Add(freqResultRow);
                        }
                    }
                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                    .OrderBy(row => row.Field<string>("Campaign"))
                    .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                //DataTableToCSVF(dtSortedTable, fileName);
                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, frequencyDataFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                dsActivityLogNameIP + "ProcessFrequencyData() completed", string.Empty,
                                                SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with frequency schema 
        /// </summary>
        /// <returns>Empty table of frequency schema</returns>
        private DataTable GetFrequencyDataTableSchema()
        {
            DataTable dtFrequency = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetFrequencyDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Add("Answers");
                columnsList.Add("controlVotes");
                columnsList.Add("controlN");
                columnsList.Add("controlResults");
                columnsList.Add("controlharveyBall");
                columnsList.Add("exposurenum");
                columnsList.Add("exposedVotes");
                columnsList.Add("exposureN");
                columnsList.Add("exposureResults");
                columnsList.Add("exposureharveyBall");
                columnsList.Add("exposurelift");
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtFrequency = new DataTable();
                DataColumn dcFrequency = null;
                foreach (string column in columnsList)
                {
                    dcFrequency = new DataColumn(column, stringType);
                    dtFrequency.Columns.Add(dcFrequency);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetFrequencyDataTableSchema() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtFrequency;
        }

       /// <summary>
       /// Process the creative data with the passed filename
       /// </summary>
       /// <param name="vizuDataListObj"></param>
       /// <param name="creativeDataFileName"></param>
        private void ProcessCreativeData(List<VizuDatasource> vizuDataListObj, string creativeDataFileName)
        {
            string sinExposure = string.Empty;
            string controlVotes = string.Empty;
            string controlN = string.Empty;
            string controlResults = string.Empty;
            string controlHarveyBall = string.Empty;

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessCreativeData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetCreativeDataTableSchema();

                string curDate = DateTime.Now.ToShortDateString();
                string creativeId = string.Empty;

                foreach (VizuDatasource vizuDataSourceObj in vizuDataListObj)
                {

                    foreach (CreativePerformance.creativesRow cobj in vizuDataSourceObj.CreativeRow)
                    {
                        sinExposure = cobj.singleExposure;
                        foreach (CreativePerformance.controlRow crowobj in cobj.GetcontrolRows())
                        {
                            if (!crowobj.Iscreatives_IdNull())
                                creativeId = crowobj.creatives_Id.ToString();
                            controlVotes = crowobj.votes;
                            controlN = crowobj.n;
                            controlResults = crowobj.results;
                            controlHarveyBall = crowobj.harveyBall;
                        }

                        bool firstRow = true;
                        foreach (CreativePerformance.creativeRow robj in cobj.GetcreativeRows())
                        {
                            DataRow rObj = vizuData.NewRow(); //Add first time the control row
                            if (firstRow)
                            {
                                rObj["ExtractDate"] = curDate;
                                rObj["Campaign"] = vizuDataSourceObj.CampaignName;
                                rObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                                rObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                                rObj["CampaignObjective"] = vizuDataSourceObj.Objective;


                                rObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                                rObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                                rObj["MetricId"] = vizuDataSourceObj.MetricId;
                                rObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                                rObj["MetricType"] = vizuDataSourceObj.MetricType;
                                rObj["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                                rObj["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                                rObj["Answers"] = vizuDataSourceObj.AnswersFilters;
                                rObj["SingleExposure"] = cobj.singleExposure;

                                rObj["controlVotes"] = controlVotes;
                                rObj["controlN"] = controlN;
                                rObj["controlResults"] = controlResults;
                                rObj["controlharveyBall"] = controlHarveyBall;

                                rObj["CreativeId"] = creativeId;
                                rObj["CreativeVotes"] = "";
                                rObj["CreativeN"] = "";
                                rObj["CreativResults"] = "";
                                rObj["CreativeLift"] = "";
                                rObj["CreativeHarveyBall"] = "";

                                firstRow = false;

                                vizuData.Rows.Add(rObj);
                            }

                            rObj = vizuData.NewRow();
                            rObj["ExtractDate"] = curDate;
                            rObj["Campaign"] = vizuDataSourceObj.CampaignName;
                            rObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                            rObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                            rObj["CampaignObjective"] = vizuDataSourceObj.Objective;


                            rObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                            rObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                            rObj["MetricId"] = vizuDataSourceObj.MetricId;
                            rObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                            rObj["MetricType"] = vizuDataSourceObj.MetricType;
                            rObj["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                            rObj["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                            rObj["Answers"] = vizuDataSourceObj.AnswersFilters;
                            rObj["SingleExposure"] = cobj.singleExposure;
                            rObj["CreativeId"] = robj.id;
                            rObj["CreativeVotes"] = robj.votes;
                            rObj["CreativeN"] = robj.n;
                            rObj["CreativResults"] = robj.results;
                            rObj["CreativeLift"] = robj.lift;
                            rObj["CreativeHarveyBall"] = robj.harveyBall;

                            rObj["controlVotes"] = "";
                            rObj["controlN"] = "";
                            rObj["controlResults"] = "";
                            rObj["controlharveyBall"] = "";


                            vizuData.Rows.Add(rObj);
                        }
                    }
                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                   .OrderBy(row => row.Field<string>("Campaign"))
                   .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, creativeDataFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);
                      

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessCreativeData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with creative schema 
        /// </summary>
        /// <returns>Empty table of creative schema</returns>
        private DataTable GetCreativeDataTableSchema()
        {
            DataTable dtCreative = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCreativeDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Add("Answers");
                columnsList.Add("controlVotes");
                columnsList.Add("controlN");
                columnsList.Add("controlResults");
                columnsList.Add("controlharveyBall");
                columnsList.Add("SingleExposure");
                columnsList.Add("CreativeId");
                columnsList.Add("CreativeVotes");
                columnsList.Add("CreativeN");
                columnsList.Add("CreativResults");
                columnsList.Add("CreativeLift");
                columnsList.Add("CreativeHarveyBall");
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtCreative = new DataTable();
                DataColumn dcCreative = null;
                foreach (string column in columnsList)
                {
                    dcCreative = new DataColumn(column, stringType);
                    dtCreative.Columns.Add(dcCreative);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                       dsActivityLogNameIP + "GetCreativeDataTableSchema() completed", string.Empty,
                                       SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtCreative;
        }
        
        /// <summary>
        /// Process the campaign targeting data 
        /// </summary>
        /// <param name="vizuDataListObj"></param>
        /// <param name="campaignTargetingFileName"></param>
        private void ProcessCampaignTargetingData(List<VizuDatasource> vizuDataListObj, string campaignTargetingFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessCampaignTargetingData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetCampaignTargetingDataTableSchema();

                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource VizuDataSourceObj in vizuDataListObj)
                {
                    foreach (CampaignTargetingPerformance.siteRow siteRowObj in VizuDataSourceObj.SiteCampaignRows)
                    {
                        DataRow rObj = vizuData.NewRow();


                        rObj["Campaign"] = VizuDataSourceObj.CampaignName;
                        rObj["Campaign_id"] = VizuDataSourceObj.CampaignId;
                        rObj["Campaign_Status"] = VizuDataSourceObj.CampaignStatus;
                        rObj["CampaignObjective"] = VizuDataSourceObj.Objective;

                        rObj["CampaignStartDate"] = VizuDataSourceObj.CampaignStartDate;
                        rObj["CampaignEndDate"] = VizuDataSourceObj.CampaignEndDate;
                        rObj["MetricId"] = VizuDataSourceObj.MetricId;
                        rObj["MetricTxt"] = VizuDataSourceObj.MetricText;

                        rObj["MetricType"] = VizuDataSourceObj.MetricType;
                        rObj["ResultweightByVoteProportions"] = VizuDataSourceObj.ResultWeightByVoteProportions;
                        rObj["ResultWeightByMediaPlan"] = VizuDataSourceObj.WeightByMediaPlan;
                        rObj["Answers"] = VizuDataSourceObj.AnswersFilters;

                        rObj["SiteName"] = siteRowObj.name;
                        rObj["SiteVotes"] = siteRowObj.votes;
                        rObj["SiteN"] = siteRowObj.n;
                        rObj["SiteResults"] = siteRowObj.results;
                        rObj["harveyBall"] = siteRowObj.harveyBall;
                        rObj["ExtractDate"] = curDate;
                        vizuData.Rows.Add(rObj);

                    }
                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                    .OrderBy(row => row.Field<string>("Campaign"))
                    .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, campaignTargetingFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);
             
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessCampaignTargetingData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// Get Campaign Targeting table schema         
        /// </summary>
        /// <returns></returns>
        private DataTable GetCampaignTargetingDataTableSchema()
        {
            DataTable dtCampaignTargeting = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetCampaignTargetingDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Add("Answers");
                columnsList.Add("SiteName");
                columnsList.Add("SiteVotes");
                columnsList.Add("SiteN");
                columnsList.Add("SiteResults");                
                columnsList.Add("harveyBall");
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtCampaignTargeting = new DataTable();
                DataColumn dcCampaignTargeting = null;
                foreach (string column in columnsList)
                {
                    dcCampaignTargeting = new DataColumn(column, stringType);
                    dtCampaignTargeting.Columns.Add(dcCampaignTargeting);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                           dsActivityLogNameIP + "GetCampaignTargetingDataTableSchema() completed", 
                                           string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtCampaignTargeting;
        }

        /// <summary>
        /// Process the raw data list
        /// </summary>
        /// <param name="vizuDataListObj"></param>
        /// <param name="rawDataFileName"></param>
        private void ProcessRawData(List<VizuDatasource> vizuDataListObj, string rawDataFileName)
        {
            string customId = string.Empty;
            string customTxt = string.Empty;
            string adsData = string.Empty;

            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessRawData() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetRawDataTableSchema();
                
                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource vizuDataSourceObj in vizuDataListObj)
                {
                    foreach (RawData.responsesRow responsesRowObj in vizuDataSourceObj.RawDataResponseSet)
                    {
                        foreach (RawData.responseRow responseRowObj in responsesRowObj.GetresponseRows())
                        {
                            customId = string.Empty;
                            customTxt = string.Empty;
                            adsData = string.Empty;
                            foreach (RawData.customsRow customsRowObj in responseRowObj.GetcustomsRows())
                            {

                                foreach (RawData.customRow customRowObj in customsRowObj.GetcustomRows())
                                {
                                    if (customId.Equals(string.Empty))
                                        customId = customRowObj.id;
                                    else
                                        customId = customId + "-" + customRowObj.id;

                                    if (customTxt.Equals(string.Empty))
                                        customTxt = customRowObj.txt;
                                    else
                                        customTxt = customTxt + "-" + customRowObj.txt;
                                }
                            }
                            string ansText = string.Empty;
                            foreach (RawData.answersRow ansRowObj in responseRowObj.GetanswersRows())
                            {
                                foreach (RawData.answerRow ansRObj in ansRowObj.GetanswerRows())
                                {
                                    if (ansText.Equals(string.Empty))
                                        ansText = ansRObj.txt;
                                    else
                                        ansText = ansText + "-" + ansRObj.txt;
                                }
                            }
                            
                            foreach (RawData.adsRow adRowsObj in responseRowObj.GetadsRows())
                            {
                                foreach (RawData.adRow adRowObj in adRowsObj.GetadRows())
                                {

                                    DataRow vizuDataRowObj = vizuData.NewRow();

                                    vizuDataRowObj["Campaign"] = vizuDataSourceObj.CampaignName;
                                    vizuDataRowObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                                    vizuDataRowObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                                    vizuDataRowObj["CampaignObjective"] = vizuDataSourceObj.Objective;

                                    vizuDataRowObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                                    vizuDataRowObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                                    vizuDataRowObj["MetricId"] = vizuDataSourceObj.MetricId;
                                    vizuDataRowObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                                    vizuDataRowObj["MetricType"] = vizuDataSourceObj.MetricType;

                                    vizuDataRowObj["ResponseUserId"] = responseRowObj.userid;
                                    vizuDataRowObj["ResponseDate"] = responseRowObj.date;
                                    vizuDataRowObj["ResponseSiteUrl"] = responseRowObj.siteurl;
                                    vizuDataRowObj["ResponseWildCard"] = responseRowObj.wildCard;
                                    vizuDataRowObj["Answers"] = ansText;

                                    vizuDataRowObj["CustomId"] = customId;
                                    vizuDataRowObj["CustomTxt"] = customTxt;

                                    vizuDataRowObj["AdId"] = adRowObj.id;
                                    vizuDataRowObj ["Imp"] = adRowObj.imp;
                                    vizuDataRowObj["ExtractDate"] = curDate;
                                    vizuData.Rows.Add(vizuDataRowObj);

                                }
                            }
                        }
                    }
                }


                DataTable dtSortedTable = vizuData.AsEnumerable()
                    .OrderBy(row => row.Field<string>("Campaign"))
                    .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, rawDataFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessRawData() completed", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with raw schema 
        /// </summary>
        /// <returns>Empty table of raw schema</returns>
        private DataTable GetRawDataTableSchema()
        {
            DataTable dtRaw = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetRawDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Remove("ResultweightByVoteProportions");
                columnsList.Remove("ResultWeightByMediaPlan");

                columnsList.Add("ResponseUserId");
                columnsList.Add("ResponseDate");
                columnsList.Add("ResponseSiteUrl");
                columnsList.Add("ResponseWildCard");
                columnsList.Add("Answers");
                columnsList.Add("CustomId");
                columnsList.Add("CustomTxt");
                columnsList.Add("AdId");
                columnsList.Add("Imp");
                columnsList.Add("ExtractDate");
                
                Type stringType = typeof(string);

                dtRaw = new DataTable();
                DataColumn dcRaw = null;
                foreach (string column in columnsList)
                {
                    dcRaw = new DataColumn(column, stringType);
                    dtRaw.Columns.Add(dcRaw);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                              dsActivityLogNameIP + "GetRawDataTableSchema() completed",
                                              string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtRaw;
        }

        /// <summary>
        /// Process the site data
        /// </summary>
        /// <param name="vizuDataListSourceObj"></param>
        /// <param name="siteDataFileName"></param>
        private void ProcessSiteData(List<VizuDatasource> vizuDataListSourceObj, string siteDataFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessSiteData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetSiteDataTableSchema();

                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource vizuDataSourceObj in vizuDataListSourceObj)
                {
                    foreach (SitePerformance.siteRow siteRowObj in vizuDataSourceObj.SiteRows)
                    {
                        DataRow rObj = vizuData.NewRow();

                        rObj["ExtractDate"] = curDate;

                        rObj["Campaign"] = vizuDataSourceObj.CampaignName;
                        rObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                        rObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                        rObj["CampaignObjective"] = vizuDataSourceObj.Objective;

                        rObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                        rObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                        rObj["MetricId"] = vizuDataSourceObj.MetricId;
                        rObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                        rObj["MetricType"] = vizuDataSourceObj.MetricType;
                        rObj["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                        rObj["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                        rObj["Answers"] = vizuDataSourceObj.AnswersFilters;

                        rObj["SiteName"] = siteRowObj.name;
                        rObj["controlVotes"] = siteRowObj.controlVotes;
                        rObj["exposedVotes"] = siteRowObj.exposedVotes;
                        rObj["controlN"] = siteRowObj.controlN;
                        rObj["testN"] = siteRowObj.testN;
                        rObj["controlResults"] = siteRowObj.controlResults;
                        rObj["exposedResults"] = siteRowObj.exposedResults;
                        rObj["harveyBall"] = siteRowObj.harveyBall;
                        rObj["lift"] = siteRowObj.lift;

                        vizuData.Rows.Add(rObj);
                    }
                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                    .OrderBy(row => row.Field<string>("Campaign"))
                    .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, siteDataFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

               UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessSiteData() completed", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with site schema 
        /// </summary>
        /// <returns>Empty table of site schema</returns>
        private DataTable GetSiteDataTableSchema()
        {
            DataTable dtSite = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetSiteDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns());

                columnsList.Add("Answers");
                columnsList.Add("SiteName");
                columnsList.Add("controlVotes");
                columnsList.Add("ExposedVotes");
                columnsList.Add("controlN");
                columnsList.Add("testN");
                columnsList.Add("controlResults");
                columnsList.Add("ExposedResults");
                columnsList.Add("harveyBall");
                columnsList.Add("ExtractDate");                
                columnsList.Add("lift");

                Type stringType = typeof(string);

                dtSite = new DataTable();
                DataColumn dcSite = null;
                foreach (string column in columnsList)
                {
                    dcSite = new DataColumn(column, stringType);
                    dtSite.Columns.Add(dcSite);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                  dsActivityLogNameIP + "GetSiteDataTableSchema() completed",
                                                  string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtSite;
        }

        /// <summary>
        /// Process the wild card data
        /// </summary>
        /// <param name="vizuDataListObj"></param>
        /// <param name="wildCardFileName"></param>      
        private void ProcessWildCardData(List<VizuDatasource> vizuDataListObj, string wildCardFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "ProcessWildCardData() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable vizuData = GetWildCardDataTableSchema();

                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource vizuDataSourceObj in vizuDataListObj)
                {
                    foreach (WildCardPerformance.wildCardsRow cobj in vizuDataSourceObj.WildCardRows)
                    {
                        foreach (WildCardPerformance.wildCardRow wObj in cobj.GetwildCardRows())
                        {
                            DataRow rObj = vizuData.NewRow();


                            rObj["Campaign"] = vizuDataSourceObj.CampaignName;
                            rObj["Campaign_id"] = vizuDataSourceObj.CampaignId;
                            rObj["Campaign_Status"] = vizuDataSourceObj.CampaignStatus;
                            rObj["CampaignObjective"] = vizuDataSourceObj.Objective;

                            rObj["CampaignStartDate"] = vizuDataSourceObj.CampaignStartDate;
                            rObj["CampaignEndDate"] = vizuDataSourceObj.CampaignEndDate;
                            rObj["MetricId"] = vizuDataSourceObj.MetricId;
                            rObj["MetricTxt"] = vizuDataSourceObj.MetricText;

                            rObj["MetricType"] = vizuDataSourceObj.MetricType;
                            rObj["ResultweightByVoteProportions"] = vizuDataSourceObj.ResultWeightByVoteProportions;
                            rObj["ResultWeightByMediaPlan"] = vizuDataSourceObj.WeightByMediaPlan;
                            rObj["Answers"] = vizuDataSourceObj.AnswersFilters;

                            rObj["wildCardName"] = wObj.name;
                            rObj["controlVotes"] = wObj.controlVotes;
                            rObj["exposedVotes"] = wObj.exposedVotes;
                            rObj["controlN"] = wObj.controlN;
                            rObj["testN"] = wObj.testN;
                            rObj["controlResults"] = wObj.controlResults;
                            rObj["exposedResults"] = wObj.exposedResults;
                            rObj["lift"] = wObj.lift;
                            rObj["harveyBall"] = wObj.harveyBall;
                            rObj["ExtractDate"] = curDate;
                            vizuData.Rows.Add(rObj);
                        }
                    }
                }

                DataTable dtSortedTable = vizuData.AsEnumerable()
                   .OrderBy(row => row.Field<string>("Campaign"))
                   .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;

                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, wildCardFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                     dsActivityLogNameIP + "ProcessWildCardData() completed", string.Empty,
                                     SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with wild card schema 
        /// </summary>
        /// <returns>Empty table of wild card schema</returns>
        private DataTable GetWildCardDataTableSchema()
        {
            DataTable dtWildCard = null;
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetWildCardDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();
                columnsList.AddRange(GetCommonDataColumns()); 

                columnsList.Add("Answers");
                columnsList.Add("wildCardName");                
                columnsList.Add("controlVotes");
                columnsList.Add("ExposedVotes");
                columnsList.Add("controlN");
                columnsList.Add("testN");
                columnsList.Add("controlResults");
                columnsList.Add("ExposedResults");
                columnsList.Add("harveyBall");
                columnsList.Add("lift");
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtWildCard = new DataTable();
                DataColumn dcWildCard = null;
                foreach (string column in columnsList)
                {
                    dcWildCard = new DataColumn(column, stringType);
                    dtWildCard.Columns.Add(dcWildCard);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                      dsActivityLogNameIP + "GetWildCardDataTableSchema() completed",
                                                      string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtWildCard;
        }

        /// <summary>
        /// Process the survey data list
        /// </summary>
        /// <param name="vizuDatasourceList"></param>
        /// <param name="surveyDataFileName"></param>
        private void ProcessSurveyData(List<VizuDatasource> vizuDatasourceList, string surveyDataFileName)
        {
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessSurveyData() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                DataTable dtSurvey = GetSurveyDataTableSchema();
                
                string curDate = DateTime.Now.ToShortDateString();

                foreach (VizuDatasource surObj in vizuDatasourceList)
                {
                    if (surObj.AnswersList != null)
                    {
                        foreach (SurveyResult.answerRow rowObj in surObj.AnswersList)
                        {
                            DataRow rObj = dtSurvey.NewRow();

                            rObj["Campaign"] = surObj.CampaignName;
                            rObj["Campaign_id"] = surObj.CampaignId;
                            rObj["campaign_Status"] = surObj.CampaignStatus;
                            rObj["CampaignObjective"] = surObj.Objective;

                            rObj["CampaignStartDate"] = surObj.CampaignStartDate;
                            rObj["CampaignEndDate"] = surObj.CampaignEndDate;
                            rObj["MetricId"] = surObj.MetricId;
                            rObj["MetricTxt"] = surObj.MetricText;

                            rObj["MetricType"] = surObj.MetricType;
                            rObj["ResultweightByVoteProportions"] = surObj.ResultWeightByVoteProportions;
                            rObj["ResultWeightByMediaPlan"] = surObj.WeightByMediaPlan;

                            rObj["Answers"] = rowObj.answers_Id + "-" + rowObj.txt;

                            rObj["controlVotes"] = rowObj.controlVotes;
                            rObj["ExposedVotes"] = rowObj.exposedVotes;
                            rObj["controlN"] = rowObj.controlN;
                            rObj["testN"] = rowObj.testN;
                            rObj["controlResults"] = rowObj.controlResults;
                            rObj["ExposedResults"] = rowObj.exposedResults;
                            rObj["harveyBall"] = rowObj.exposedResults;
                            rObj["ExtractDate"] = curDate;
                            dtSurvey.Rows.Add(rObj);
                        }
                    }
                }

                DataTable dtSortedTable = dtSurvey.AsEnumerable()
                    .OrderBy(row => row.Field<string>("Campaign"))
                    .CopyToDataTable();

                DataSet dsReport = new DataSet();

                dsReport.Tables.Add(dtSortedTable);

                string backupZipFileSubPath = dataSourceConfig.Rootfolderpath + dataSourceConfig.Backupzipfoldername;
                UtilityHelper.WriteCSV(dsReport, dataSourceConfig.Rootfolderpath, surveyDataFileName, backupZipFileSubPath, dataSourceConfig.Etlfolderpath);

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "ProcessSurveyData() completed", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
        }

        /// <summary>
        /// To get the data table with survey schema 
        /// </summary>
        /// <returns>Empty table of survey schema</returns>
        private DataTable GetSurveyDataTableSchema()
        {
            DataTable dtSurvey = null;            
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                    dsActivityLogNameIP + "GetSurveyDataTableSchema() started..", string.Empty,
                                    SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                List<string> columnsList = new List<string>();

                //columnsList.AddRange(GetCommonDataColumns());
                //Modified by Karthic.M
                //Not called common Data columns because column is different from others
                columnsList.Add("Campaign");
                columnsList.Add("Campaign_id"); 
                columnsList.Add("CampaignObjective");
                columnsList.Add("CampaignStartDate");
                columnsList.Add("CampaignEndDate");
                columnsList.Add("Campaign_Status");             
                columnsList.Add("MetricId");
                columnsList.Add("MetricTxt");  
                columnsList.Add("MetricType");
                columnsList.Add("ResultweightByVoteProportions");   
                columnsList.Add("ResultWeightByMediaPlan");
                columnsList.Add("Answers");
                columnsList.Add("ControlVotes");   
                columnsList.Add("ExposedVotes");
                columnsList.Add("ControlN");        
                columnsList.Add("TestN");          
                columnsList.Add("ControlResults");  
                columnsList.Add("ExposedResults");
                columnsList.Add("HarveyBall");      
                columnsList.Add("ExtractDate");

                Type stringType = typeof(string);

                dtSurvey = new DataTable();
                DataColumn dcSurvey = null;
                foreach(string column in columnsList)
                {
                    dcSurvey = new DataColumn(column, stringType);
                    dtSurvey.Columns.Add(dcSurvey);
                }

                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                                dsActivityLogNameIP + "GetSurveyDataTableSchema() completed",
                                                string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);

                throw ex;
            }
            return dtSurvey;
        }

        /// <summary>
        /// Get all the common columns for the datatable
        /// </summary>
        /// <returns>the columns list</returns>
        private List<string> GetCommonDataColumns()
        {
            List<string> columnsList = new List<string>();
            try
            {
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                   dsActivityLogNameIP + "GetCommonDataColumns() started..", string.Empty,
                                   SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);

                columnsList.Add("Campaign");
                columnsList.Add("Campaign_id"); 
                columnsList.Add("Campaign_Status"); 
                columnsList.Add("CampaignObjective");
                columnsList.Add("CampaignStartDate");
                columnsList.Add("CampaignEndDate");
                columnsList.Add("MetricId");
                columnsList.Add("MetricTxt");  
                columnsList.Add("MetricType");
                columnsList.Add("ResultweightByVoteProportions");   
                columnsList.Add("ResultWeightByMediaPlan");
                
                UtilityHelper.ActivityLog(dsCode, dsGeneralActivityCode,
                                            dsActivityLogNameIP + "GetCommonDataColumns() completed",
                                            string.Empty, SeverityLevel.INFORMATION, EmailNotification.NOT_NEEDED);
            }
            catch (Exception ex)
            {
                UtilityHelper.ActivityLog(dsCode, dsErrorActivityCode, ex.Message,
                                    ex.StackTrace, SeverityLevel.ERROR_LEVEL1, EmailNotification.SEND);
                throw ex;
            }
            return columnsList;
        }
              
    }

    /// <summary>
    /// The data list
    /// </summary>
    public struct DataList
    {
        public const string OVERALL_PERFORMANCE = "OVERALL PERFORMANCE";
        public const string FREQUENCY_PERFORMANCE = "FREQUENCY PERFORMANCE";
        public const string CREATIVE_PERFORMANCE = "CREATIVE PERFORMANCE";
        public const string SITE_PERFORMANCE = "SITE PERFORMANCE";
        public const string SURVEY = "SURVEY";
        public const string LIFETIME_PERFORMANCE = "LIFETIME PERFORMANCE";
        public const string WILDCARD_PERFORMANCE = "WILDCARD PERFORMANCE";
        public const string RAWDATA = "RAWDATA";
        public const string CAMPAIGN_TARGETING = "CAMPAIGN TARGETING";
    }

    /// <summary>
    /// The report period
    /// </summary>
    public struct ReportPeriod
    {
        public const string DAY = "day";
        public const string WEEK = "week";
        public const string MONTH = "month";
        public const string CUMULATIVE = "Cumulative";
    }
}


